﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
//Imports System.Runtime.InteropServices
//Imports System.Drawing.Drawing2D
using System.Windows.Forms;
namespace ProGammaX
{
    public partial class SplashScreen
    {
 
        
        //BackgroundWorker backgroundWorker1 = new BackgroundWorker();



        private static SplashScreen SplashScreenRef;
        public static SplashScreen splashScreenRef
            {
            get
                {
                return SplashScreenRef;
                }
            }
        //private System.Windows.Forms.Timer timer;
        public SplashScreen()
        {
        //timer = new System.Windows.Forms.Timer();

            InitializeComponent();

            //timer.Interval = 1000;
            //timer.Start();
            
            //timer.Tick += timer_Tick;
            //this.FormClosing += SplashScreen_FormClosing;
            //this.Load += SplashScreen_Load;

            SplashScreenRef = this;
            //Hide our form from user
            //this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //this.TransparencyKey = Color.FromKnownColor(KnownColor.Control);
            //this.Update();

            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(asm.Location);
            //System.String.Format("{0}.{1}", fvi.ProductMajorPart, fvi.ProductMinorPart);

            //Label_Version.Text = Module_Translation.stringToBeTranslated[170];
            // "Version {0}.{1:00}.{2}"
            //Label_Version.Text = "Version {0}.{1:00}"
            Label_Version.Text = System.String.Format("Version {0}.{1:0}.{2:0}", fvi.ProductMajorPart, fvi.ProductMinorPart, fvi.ProductBuildPart);

            //Copyright info
            //copyright.Text = My.Application.Info.Copyright
            copyright.Text = MainForm.StringToBeTranslated[174];
            //this.Load += new System.EventHandler(SplashScreen1_Load);

                }


        //private void SplashScreen_Load(object sender, System.EventArgs e)
        //    {
        //    this.Refresh();
        //    }


        //bool refreshSplash = true;
        //protected override void OnPaint(PaintEventArgs e)
        //    {
        //    //  Do nothing here!
        //    if (refreshSplash)
        //        {
        //        this.Refresh();
        //        this.Update();
        //        refreshSplash = false;
        //        }
        //    }

        //protected override void OnPaintBackground(PaintEventArgs e)
        //    {
        //    this.DoubleBuffered = true;
        //    System.Drawing.Graphics gfx = e.Graphics;
        //    //gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

        //    gfx.DrawIcon(global::My.Resources.Resources.Profile11_a, new Rectangle(0, 0, this.Width, this.Height));
        //    //this.Update();
            
        //    }

        //private void SplashScreen_FormClosing(object sender, FormClosingEventArgs e)
        //    {
        //    timer.Stop();
        //    timer.Dispose();
        //    //this.Close();
        //    }

        //private void noPushMeButton_Click(object sender, EventArgs e)
        //    {
        //    this.Close();
        //    }
        //private void SplashScreen_VisibleChanged()
        //    {
        //    this.Refresh();
        //    }

         //void timer_Tick(object sender, EventArgs e)
         //   {
         //   refreshSplash = true;
         //   }
        //public static Image SplashScreen = Image.FromStream(MainForm.GetResource("Splash Screen.png"));

        //private static System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
        //private static string assemblyPath = Program.assembly.GetName().Name.Replace(' ', '_');



        //public static System.IO.Stream GetResource(string fileName)
        //    {
        //    System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
        //    string resourceName = asm.GetName().Name + ".Properties.Resources";
        //    var rm = new System.Resources.ResourceManager(resourceName, asm);
        //    //return (Bitmap)rm.GetObject(imageName);

        //    //string imageName1 = "dateBackGround";
        //    fileName = fileName.Replace(" ", "_");
        //    Icon myIcon = (Icon)rm.GetObject(fileName);
        //    //return SplashScreen.assembly.GetManifestResourceStream(Program.assemblyPath + '.' + fileName);
        //    }




        // send window to the top function
        //    <DllImport("user32.dll", SetLastError:=True)> _
        //Private Shared Function SetWindowPos(ByVal hWnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As UInt32) As Boolean
        //    End Function
        //    ReadOnly HWND_BOTTOM As New IntPtr(1)
        //    ReadOnly HWND_TOP As New IntPtr(0)
        //    Private Const SWP_NOSIZE As UInt32 = &H1
        //    Private Const SWP_NOMOVE As UInt32 = &H2
        //    'Private Sub Form1_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        //    'End Sub
        //    Private Sub SplashScreen1_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        //        REM send window to the top
        //        Dim flags As UInt32 = SWP_NOMOVE Or SWP_NOSIZE
        //        SetWindowPos(Form1.Handle, HWND_TOP, 0, 0, 0, 0, flags)
        //    End Sub
        //TODO: This form can easily be set as the splash screen for the application by going to the "Application" tab
        //  of the Project Designer ("Properties" under the "Project" menu).
        //Private Sub SplashScreen1_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        //    REM send window to the top
        //    Dim flags As UInt32 = SWP_NOMOVE Or SWP_NOSIZE
        //    SetWindowPos(Me.Handle, HWND_TOP, 0, 0, 0, 0, flags)
        //End Sub
        // ERROR: Handles clauses are not supported in C#
        //private void SplashScreen1_Load(object sender, System.EventArgs e)
        //    {
        //    //Dim m_Bitmap As New Bitmap(Me.Width, Me.Height)
        //    //Using g As Graphics = Graphics.FromImage(m_Bitmap)
        //    //    g.CopyFromScreen(Me.Location, Point.Empty, Me.Size)
        //    //    g.DrawImage(My.Resources.Profile11, 0, 0, Me.Width, Me.Height)
        //    //End Using
        //    //Me.BackgroundImage = m_Bitmap

        //    //Set up the dialog text at runtime according to the application's assembly information.

        //    //TODO: Customize the application's assembly information in the "Application" pane of the project
        //    //  properties dialog (under the "Project" menu).

        //    //Application title

        //    if (ProGammaX.MainForm.mainFormRef.Text != "")
        //        {
        //        ApplicationTitle.Text = ProGammaX.MainForm.mainFormRef.Text;//My.Application.Info.Title;
        //        }
        //    else
        //        {
        //        //If the application title is missing, use the application name, without the extension
        //        ApplicationTitle.Text = "ProGammaX";//System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName);
        //        }
        //    this.WindowState = FormWindowState.Minimized;

        //    this.ShowInTaskbar = false;
        //    //Format the version information using the text set into the Version control at design time as the
        //    //  formatting string.  This allows for effective localization if desired.
        //    //  Build and revision information could be included by using the following code and changing the
        //    //  Version control's designtime text to "Version {0}.{1:00}.{2}.{3}" or something similar.  See
        //    //  String.Format() in Help for more information.
        //    //
        //    //    Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)
        //    //Module_Translation Module_Translation = new Module_Translation();



        //    // " For DOSBox v0.74"



        //    //}
        //    }



        ///// <summary>

        ///// Contains information for creating a splash screen.

        ///// </summary>

        //private struct SplashScreenInfo

        //{

        //    /// <summary>

        //    /// The main form for the application.

        //    /// </summary>

        //    public Form StartupForm;

        //    /// <summary>

        //    /// The minimum number of milliseconds for which to display the splash screen.

        //    /// </summary>

        //    public int MinimumDisplayTime;

        //    /// <summary>

        //    /// Signals the main thread when a handler has been added to the main form's Load event.

        //    /// </summary>

        //    public ManualResetEvent Handle;

        //}

 

        ///// <summary>

        ///// Tells the splash screen when it has been open for the minimum amount of time.

        ///// </summary>

        //private System.Timers.Timer timer = new System.Timers.Timer();

        ///// <summary>

        ///// The minimum number of milliseconds for which to display the splash screen.

        ///// </summary>

        //private bool minimumDisplayTimeExpired = false;

        ///// <summary>

        ///// Used to synchronise multiple threads.

        ///// </summary>

        //private object syncRoot = new object();

        ///// <summary>

        ///// Signals the main thread when the splash screen is ready to close.

        ///// </summary>

        //private ManualResetEvent closeHandle;

 

        ///// <summary>

        ///// Creates a new instance of the SplashScreen class.

        ///// </summary>

        ///// <param name="startupForm">

        ///// The main form for the application.

        ///// </param>

        ///// <param name="minimumDisplayTime">

        ///// The minimum number of milliseconds for which to display the splash screen.

        ///// </param>

        //private SplashScreen(Form startupForm, int minimumDisplayTime)

        //{

        //    InitializeComponent();

 

        //    // We need to know when the main form is ready to be displayed.

        //    startupForm.Load += startupForm_Load;

 

        //    // We need to know when the splash screen is ready to be dismissed.

        //    timer.Elapsed += timer_Elapsed;

        //    timer.Interval = minimumDisplayTime;

        //    timer.Start();

        //}

 

        ///// <summary>

        ///// Displays a splash screen.

        ///// </summary>

        ///// <param name="startupForm">

        ///// The main form for the application.

        ///// </param>

        ///// <param name="minimumDisplayTime">

        ///// The minimum number of milliseconds for which to display the splash screen.

        ///// </param>

        //public static void DisplaySplashScreen(Form startupForm, int minimumDisplayTime)

        //{

        //    var continueHandle = new ManualResetEvent(false);

 

        //    // Create and display the splash screen on a secondary thread.

        //    new Thread(DisplaySplashScreen).Start(new SplashScreenInfo

        //                                              {

        //                                                  StartupForm = startupForm,

        //                                                  MinimumDisplayTime = minimumDisplayTime,

        //                                                  Handle = continueHandle

        //                                              });

 

        //    // The handler must be added to the main form's Load event before the main thread can safely continue.

        //    continueHandle.WaitOne();

        //}

 

        ///// <summary>

        ///// Displays a splash screen.

        ///// </summary>

        ///// <param name="info">

        ///// Contains information for creating a splash screen.

        ///// </param>

        //private static void DisplaySplashScreen(object info)

        //{

        //    var ssi = (SplashScreenInfo) info;

        //    var splashScreen = new SplashScreen(ssi.StartupForm, ssi.MinimumDisplayTime);

 

        //    // The main form's Load event has been handled so the main thread can safely continue.

        //    ssi.Handle.Set();

 

        //    // Display the splash screen.

        //    Application.Run(splashScreen);

        //}

 

        //private void startupForm_Load(object sender, EventArgs e)

        //{

        //    lock (syncRoot)

        //    {

        //        if (!minimumDisplayTimeExpired)

        //        {

        //            // The splash screen is not ready to be dismissed so we must make the main form wait.

        //            closeHandle = new ManualResetEvent(false);

        //        }

        //    }

 

        //    if (closeHandle != null)

        //    {

        //        // The splash screen is not ready to be dismissed so we must make the main form wait.

        //        closeHandle.WaitOne();

        //    }

 

        //    // Close the splash screen and allow the main form to be displayed.

        //    this.Invoke(new MethodInvoker(this.Close));

        //}

 

        //private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)

        //{

        //    lock (syncRoot)

        //    {

        //        if (closeHandle == null)

        //        {

        //            // The splash screen is ready to be dismissed before the main form is ready to be displayed.

        //            // Allow the main form to be displayed as soon as it is ready.

        //            minimumDisplayTimeExpired = true;
                    
        //        }

        //        else

        //        {

        //            // The main form is already ready to be displayed and is waiting for the splash screen.

        //            closeHandle.Set();
                    
                    
        //        }

        //    }

        //}

    }
}