﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProGammaX
{
    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    public class cPanel : IDisposable
    {
        #region Fields
        private IntPtr _hPanelWnd = IntPtr.Zero;
        private cInternalScrollBar _cInternalScroll;
        #endregion

        #region Constructor
        public cPanel(IntPtr handle, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb, Bitmap fader)
        {
            if (handle == IntPtr.Zero)
                throw new Exception("The Panel handle is invalid.");
            _hPanelWnd = handle;
            if (hztrack != null && hzarrow != null && hzthumb != null && vttrack != null && vtarrow != null && vtthumb != null)
                _cInternalScroll = new cInternalScrollBar(_hPanelWnd, hztrack, hzarrow, hzthumb, vttrack, vtarrow, vtthumb, fader);
            else
                throw new Exception("The Panel image(s) are invalid");
        }

        public void Dispose()
        {
            try
            {
                if (_cInternalScroll != null) _cInternalScroll.Dispose();
            }
            catch { }
        }
        #endregion
    }




}
