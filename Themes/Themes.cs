﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using ProGammaX.Properties;
//using scrollbar;

namespace ProGammaX
{
    public partial class MainForm
    {



        public void getThemes()
        {
            string[] filesPath;
            filesPath = Directory.GetFiles(themesDirectory + "\\", "*.ini", SearchOption.TopDirectoryOnly);

            listBox_OPTIONS_Themes.Items.Clear();

            foreach (string TempFile in filesPath)
            {
                // get filename without extension
                listBox_OPTIONS_Themes.Items.Add(Strings.Left(GetFileNameFromPath(TempFile), Strings.Len(GetFileNameFromPath(TempFile)) - 4));

            }

            listBox_OPTIONS_Themes.Sorted = true;
        }

        private string[] getStringArray(string stringToBeParsed, string delimiter = ",")
        {
            int tempCount = 0;
            string[] tempParameters = new string[] { string.Empty };


            while (Strings.InStr(stringToBeParsed, delimiter) > 0)
            {
                if (Strings.Left(stringToBeParsed, (Strings.InStr(stringToBeParsed, delimiter) - 1)) != string.Empty)
                {
                    tempParameters[tempCount] = Strings.Replace(Strings.Left(stringToBeParsed, (Strings.InStr(stringToBeParsed, delimiter) - 1)), delimiter, null);
                    tempCount += 1;
                    Array.Resize(ref tempParameters, tempCount + 2);
                }
                stringToBeParsed = Strings.LTrim(Strings.Right(stringToBeParsed, (Strings.Len(stringToBeParsed) - Strings.InStr(stringToBeParsed, delimiter))));
            }
            if (Strings.Trim(stringToBeParsed) != string.Empty)
            {
                tempParameters[tempCount] = Strings.Replace(Strings.Trim(stringToBeParsed), delimiter, null);
            }

            return tempParameters;
        }


        public void applyTheme(string themeNameWithPath)
        {




            if (!File.Exists(themeNameWithPath))
                return;

            // Deklarovanie poľa pre databázu obsahu súborov
            //System.IO.StreamReader fileReader = null;
            string Line = string.Empty;
            string TrimLine = string.Empty;


            System.Text.Encoding TempFileEncoding = System.Text.Encoding.UTF8;
            // detect current file encoding
            switch (FileIsEncodedInUTF8(themeNameWithPath))
            {

                case true:
                    TempFileEncoding = System.Text.Encoding.UTF8;
                    break;
                default:

                    TempFileEncoding = System.Text.Encoding.Default;
                    break;
            }

            // CONTENT OF FILES:
            using (StreamReader fileReader = new StreamReader(themeNameWithPath, TempFileEncoding))
            {
                while (!fileReader.EndOfStream)
                {
                    NEXT_LINE:
                    Line = fileReader.ReadLine();
                    TrimLine = Strings.Trim(Line);











                    //backColor
                    if (Strings.Left(TrimLine, Strings.Len("backColor=")) == "backColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("backColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.FromArgb(250, 247, 240);
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("backColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {


                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {



                                    string[] TempParameters = getStringArray(TempString);


                                    //while (Strings.InStr(TempString, ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ", ") - 1)), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Trim(TempString), ", ", null);
                                    //}

                                    ////ComboBox_PROFILE_Drives_ViewListboxDrivesToBeMounted.DataSource = TempParameters
                                    //ComboBox_PROFILE_Drives_ViewListboxDrivesToBeMounted.Items.Clear();
                                    //foreach (string TempItem in TempParameters)
                                    //{
                                    //    if (TempItem != null)
                                    //    {
                                    //        ComboBox_PROFILE_Drives_ViewListboxDrivesToBeMounted.Items.Add(TempItem);
                                    //    }
                                    //}
                                    //this.Text = TempParameters[0] + ":" + TempParameters[1] + ":" + TempParameters[2];

                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }
                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.BackColor = tempColor;
                            }


                        }
                        //}




                        goto NEXT_LINE;
                    }


                    //foreColor
                    if (Strings.Left(TrimLine, Strings.Len("foreColor=")) == "foreColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("foreColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.Black;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("foreColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {



                                    string[] TempParameters = getStringArray(TempString);

                                    //while (Strings.InStr(TempString, ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ", ") - 1)), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Trim(TempString), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.ForeColor = tempColor;
                            }


                        }
                        //}




                        goto NEXT_LINE;
                    }



                    //selectionColor
                    if (Strings.Left(TrimLine, Strings.Len("selectionColor=")) == "selectionColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("selectionColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.DimGray;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("selectionColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {



                                    string[] TempParameters = getStringArray(TempString);

                                    //while (Strings.InStr(TempString, ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ", ") - 1)), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Trim(TempString), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.SelectionColor = Color.FromArgb(255, tempColor.R, tempColor.G, tempColor.B);
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }



                    //menuColor
                    if (Strings.Left(TrimLine, Strings.Len("menuColor=")) == "menuColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("menuColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.Beige;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("menuColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {



                                    string[] TempParameters = getStringArray(TempString);

                                    //while (Strings.InStr(TempString, ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ", ") - 1)), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Trim(TempString), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.MenuColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }

                    //menuTextColor
                    if (Strings.Left(TrimLine, Strings.Len("menuTextColor=")) == "menuTextColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("menuTextColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.DarkSlateGray;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("menuTextColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {

                                    string[] TempParameters = getStringArray(TempString);



                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.MenuTextColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }


                    //disabledMenuTextColor
                    if (Strings.Left(TrimLine, Strings.Len("disabledMenuTextColor=")) == "disabledMenuTextColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("disabledMenuTextColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.DarkGray;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("disabledMenuTextColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {



                                    string[] TempParameters = getStringArray(TempString);

                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.DisabledMenuTextColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }



                    //windowColor
                    if (Strings.Left(TrimLine, Strings.Len("windowColor=")) == "windowColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("windowColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.LightGoldenrodYellow;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("windowColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {



                                    string[] TempParameters = getStringArray(TempString);

                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.WindowColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }


                    //selectionForeColor
                    if (Strings.Left(TrimLine, Strings.Len("selectionForeColor=")) == "selectionForeColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("selectionForeColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.LightGoldenrodYellow;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("selectionForeColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {



                                    string[] TempParameters = getStringArray(TempString);

                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.SelectionForeColor = Color.FromArgb(255, tempColor.R, tempColor.G, tempColor.B);
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }




                    //windowTextColor
                    if (Strings.Left(TrimLine, Strings.Len("windowTextColor=")) == "windowTextColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("windowTextColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.FromArgb(22, 22, 44);
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("windowTextColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {



                                    string[] TempParameters = getStringArray(TempString);

                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.WindowTextColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }



                    //labelColor
                    if (Strings.Left(TrimLine, Strings.Len("labelColor=")) == "labelColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("labelColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.FromArgb(22, 22, 44);
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("labelColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {


                                    string[] TempParameters = getStringArray(TempString);


                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.LabelColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }

                    //tabPageBorderColor
                    if (Strings.Left(TrimLine, Strings.Len("tabPageBorderColor=")) == "tabPageBorderColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("tabPageBorderColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.LightGray;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("tabPageBorderColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {


                                    string[] TempParameters = getStringArray(TempString);


                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.TabPageBorderColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }



                    //gridColor
                    if (Strings.Left(TrimLine, Strings.Len("gridColor=")) == "gridColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("gridColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.LightGray;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("gridColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {


                                    string[] TempParameters = getStringArray(TempString);


                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.GridColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }



                    //controlsColor
                    if (Strings.Left(TrimLine, Strings.Len("controlsColor=")) == "controlsColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("controlsColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.LightGray;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("controlsColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {


                                    string[] TempParameters = getStringArray(TempString);


                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.ControlsColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }



                    //windowFrameColor
                    if (Strings.Left(TrimLine, Strings.Len("windowFrameColor=")) == "windowFrameColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("windowFrameColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.Gray;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("windowFrameColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {


                                    string[] TempParameters = getStringArray(TempString);


                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.WindowFrameColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }



                    //sliderColor
                    if (Strings.Left(TrimLine, Strings.Len("sliderColor=")) == "sliderColor=" & (Strings.Len(Strings.Replace(TrimLine, ((char)(34)).ToString(), null)) - Strings.Len("sliderColor=")) > 0)
                    {
                        // Parameters
                        //byte TempCount = 0;
                        Color tempColor = Color.Gray;
                        string TempString = Strings.Right(TrimLine, (Strings.Len(TrimLine) - Strings.Len("sliderColor=")));


                        if (!string.IsNullOrEmpty(TempString))
                        {

                            try
                            {
                                if (!TempString.Contains(","))
                                {

                                    if (Color.FromName(TempString) != Color.FromArgb(0, 0, 0, 0))
                                    {
                                        tempColor = Color.FromName(TempString);
                                    }

                                }
                                else
                                {


                                    string[] TempParameters = getStringArray(TempString);


                                    //while (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") > 0)
                                    //{
                                    //    if (Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)) != string.Empty)
                                    //    {
                                    //        TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Left(TempString, (Strings.InStr(TempString, ((char)(34)).ToString() + ", ") - 1)), ((char)(34)).ToString(), null), ", ", null);
                                    //        TempCount += 1;
                                    //        Array.Resize(ref TempParameters, TempCount + 2);
                                    //    }
                                    //    TempString = Strings.LTrim(Strings.Right(TempString, (Strings.Len(TempString) - Strings.InStr(TempString, ((char)(34)).ToString() + ", "))));
                                    //}
                                    //if (Strings.Trim(TempString) != string.Empty)
                                    //{
                                    //    TempParameters[TempCount] = Strings.Replace(Strings.Replace(Strings.Trim(TempString), ((char)(34)).ToString(), null), ", ", null);
                                    //}



                                    tempColor = Color.FromArgb((int)Strings.Val(TempParameters[0]), (int)Strings.Val(TempParameters[1]), (int)Strings.Val(TempParameters[2]));


                                }
                            }

                            catch
                            {

                            }
                            finally
                            {
                                ThemedColors.SliderColor = tempColor;
                            }



                        }
                        //}




                        goto NEXT_LINE;
                    }












                }


            }




            //_cRcm = new ProGammaX.cRCM(this.Handle);


            //loadFrameImages(0);
            //if (this.Visible)
            //{
            //    loadControlImages(0);
            //}

            loadControlColors();
            applySelectedTheme();


        }


        //[DllImport("user32.dll")]

        //public static extern bool SetSysColors(int cElements, int[] lpaElements, int[] lpaRgbValues);



        //private const int COLOR_SCROLLBAR = 0;

        //private int[] elements = { COLOR_SCROLLBAR };

        //private int[] colors = { ColorTranslator.ToWin32(ProGammaX.ThemedColors.BackColor) };



        //private void button1_Click(object sender, EventArgs e)
        //{

        //    MessageBox.Show(SetSysColors(elements.Length, elements, colors).ToString());

        //}




        //private const int WSBPROPVSTYLE = 0x100;
        //private const int FSBENCARTAMODE = 0x1;
        //private const int WSBPROPVBKGCOLOR = 0x40;


        //[DllImport("comctl32", ExactSpelling = true, CharSet = CharSet.Auto, SetLastError = true)]
        //private static extern bool InitializeFlatSB(IntPtr hWnd);
        //[DllImport("comctl32", ExactSpelling = true, CharSet = CharSet.Auto, SetLastError = true)]
        //private static extern bool FlatSB_SetScrollProp(IntPtr hWnd, int index, int newValue, bool fRedraw);


        //void foo()
        //{
        //    var g = new DataGridView();
        //    ToString();
        //    g.Controls[0].BackColor = Color.Red;
        //}

        //static public class Util
        //{
        //    static public T Find<T>(Control container) where T : Control
        //    {
        //        foreach (Control child in container.Controls)
        //            return (child is T ? (T)child : Find<T>(child));
        //        // Not found.
        //        return null;
        //    }
        //}

        [DllImport("user32.dll")]
        static extern bool SetSysColors(int cElements, int[] lpaElements,
        uint[] lpaRgbValues);

        [DllImport("user32.dll")]
        static extern uint GetSysColor(int nIndex);

        [StructLayout(LayoutKind.Sequential)]
        public struct Rgb
        {
            byte byRed, byGreen, byBlue, RESERVED;

            public Rgb(Color colorIn)
            {
                byRed = colorIn.R;
                byGreen = colorIn.G;
                byBlue = colorIn.B;
                RESERVED = 0;
            }
            public Int32 ToInt32()
            {
                byte[] RGBCOLORS = new byte[4];
                RGBCOLORS[0] = byRed;
                RGBCOLORS[1] = byGreen;
                RGBCOLORS[2] = byBlue;
                RGBCOLORS[3] = RESERVED;
                return BitConverter.ToInt32(RGBCOLORS, 0);
            }
        }
        private const int WM_ACTIVATEAPP = 0x001C;
        private const int COLOR_HIGHLIGHT = 13;
        private const int COLOR_HIGHLIGHTTEXT = 14;
        //get system color
        private static uint startUpSystemColor_COLOR_HIGHLIGHT = GetSysColor(COLOR_HIGHLIGHT);
        private static uint startUpSystemColor_COLOR_HIGHLIGHTTEXT = GetSysColor(COLOR_HIGHLIGHTTEXT);
        private static uint myCOLOR_HIGHLIGHT = (uint)(new Rgb(Color.FromArgb(255, ThemedColors.SelectionColor.R, ThemedColors.SelectionColor.G, ThemedColors.SelectionColor.B)).ToInt32());
        private static uint myCOLOR_HIGHLIGHTTEXT = (uint)(new Rgb(Color.FromArgb(255, ThemedColors.SelectionForeColor.R, ThemedColors.SelectionForeColor.G, ThemedColors.SelectionForeColor.B)).ToInt32());
        private bool appActive = true;
        //private int[] lpaElements = { COLOR_HIGHLIGHT, COLOR_HIGHLIGHTTEXT };
        //private uint[] lpaRgbValues = { (uint)(new Rgb(ThemedColors.SelectionColor).ToInt32()), (uint)(new Rgb(ThemedColors.SelectionForeColor).ToInt32()) };
        //private uint[] lpaRgbOldValues = { startUpSystemColor_COLOR_HIGHLIGHT, startUpSystemColor_COLOR_HIGHLIGHTTEXT };


        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("user32")]
        static extern bool EnableMenuItem(IntPtr hMenu, uint uIDEnableItem, uint uEnable);

        private const int MF_BYCOMMAND = 0;
        private const int MF_DISABLED = 2;
        private const int SC_CLOSE = 0xF060;
        private const int WM_KEYDOWN = 0x100;
        private const int WM_KEYUP = 0x101;
        private const int WM_SYSKEYDOWN = 0x104;
        private const int WM_MOUSEACTIVATE = 0x21;
        private const int MA_NOACTIVATE = 3;
        private const int WA_INACTIVE = 0;
        private const int CHILDACTIVATE = 0x0022;
        private const uint MA_NOACTIVATEANDEAT = 4;
        public bool prolongedOperationIsRunning = false;

        //disable keyboard while saving
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            try
            {


                //when profile is saved
                if (backgroundWorkerIsWorking == true || prolongedOperationIsRunning)
                {
                    if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
                    {
                        //lastKeyPressed = keyData;
                        return true;
                    }
                }
            }
            catch
            {


            }




            return base.ProcessCmdKey(ref msg, keyData);
        }



        //private const int WM_CTLCOLORSCROLLBAR = 0x0137;
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        protected override void WndProc(ref Message m)
        {
            // Listen for operating system messages.

            switch (m.Msg)
            {
                // The WM_ACTIVATEAPP message occurs when the application
                // becomes the active application or becomes inactive.
                case WM_ACTIVATEAPP:

                    // The WParam value identifies what is occurring.
                    appActive = (((int)m.WParam != 0));


                    // application is active.
                    if (appActive)
                    {
                        //set the color
                        //SetSysColors(1, new[] { 13 }, new uint[] { startUpSystemColor_COLOR_HIGHLIGHT });
                        //SetSysColors(1, new[] { 14 }, new uint[] { startUpSystemColor_COLOR_HIGHLIGHTTEXT });

                        myCOLOR_HIGHLIGHT = (uint)(new Rgb(Color.FromArgb(255, ThemedColors.SelectionColor.R, ThemedColors.SelectionColor.G, ThemedColors.SelectionColor.B)).ToInt32());
                        myCOLOR_HIGHLIGHTTEXT = (uint)(new Rgb(Color.FromArgb(255, ThemedColors.SelectionForeColor.R, ThemedColors.SelectionForeColor.G, ThemedColors.SelectionForeColor.B)).ToInt32());

                        SetSysColors(1, new[] { 13 }, new uint[] { myCOLOR_HIGHLIGHT });
                        SetSysColors(1, new[] { 14 }, new uint[] { myCOLOR_HIGHLIGHTTEXT });

                        //SetSysColors(1, new[] { 13 }, new uint[] { (uint)(new Rgb(Color.Red)).ToInt32() });
                        //SetSysColors(1, new[] { 14 }, new uint[] { (uint)(new Rgb(Color.RoyalBlue)).ToInt32() });
                        //if (SetSysColors(1, new int[] { lpaElements[0], lpaElements[1] }, new uint[] { lpaRgbValues[0], lpaRgbValues[1] }))

                    }
                    else
                    {

                        //reset the color back
                        SetSysColors(1, new[] { 13 }, new uint[] { startUpSystemColor_COLOR_HIGHLIGHT });
                        SetSysColors(1, new[] { 14 }, new uint[] { startUpSystemColor_COLOR_HIGHLIGHTTEXT });
                        //SetSysColors(1, lpaElements, new uint[] { lpaRgbOldValues[0], lpaRgbOldValues[1] });




                    }


                    break;
            }


            //when profile is saved
            try
            {


                if (backgroundWorkerIsWorking == true || prolongedOperationIsRunning)
                {

                    // do not move window
                    const int WM_SYSCOMMAND = 0x0112;
                    const int SC_MOVE = 0xF010;

                    switch (m.Msg)
                    {
                        case WM_SYSCOMMAND:
                            int command = m.WParam.ToInt32() & 0xfff0;
                            if (command == SC_MOVE)
                                return;
                            break;
                    }

                    //deactivate mouse clicks
                    if (m.Msg == WM_MOUSEACTIVATE)
                    {
                        m.Result = (IntPtr)MA_NOACTIVATEANDEAT;
                        return;
                    }



                }
            }
            catch
            {

            }



            //if(disableSystemColorSet == false)
            base.WndProc(ref m);



        }


        ////declaring
        //[DllImport("user32.dll", SetLastError = true)]
        //private static extern bool SetSysColors(int cElements, Int32[] lpaElements, Int32[] lpaRgbValues);

        //[DllImport("user32.dll", SetLastError = true)]
        //private static extern uint GetSysColor(int nIndex);

        //private const int SYS_COLOR_INDEX_FLAG = 0x40000000;
        //public const int COLOR_HIGHLIGHT = (13 | SYS_COLOR_INDEX_FLAG);



        //public IEnumerable<Control> GetSelfAndChildrenRecursive(Control parent)
        //{
        //    List<Control> controls = new List<Control>();

        //    foreach (Control child in parent.Controls)
        //    {
        //        controls.AddRange(GetSelfAndChildrenRecursive(child));
        //    }

        //    controls.Add(parent);

        //    return controls;
        //}

        public IEnumerable<T> GetAllControls<T>(T control, Type type)
        {
            var controls = (control as Control).Controls.Cast<T>();

            return controls.SelectMany(ctrl => GetAllControls(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        //public IEnumerable<Control> getAllControls(Control control, Type type)
        //{
        //    var controls = control.Controls.Cast<Control>();

        //    return controls.SelectMany(ctrl => getAllControls(ctrl, type))
        //                              .Concat(controls)
        //                              .Where(c => c.GetType() == type);
        //}



        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(int hwnd);

        [DllImport("user32.dll")]
        private static extern int GetDesktopWindow();


        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);














        public static Color TargetColor = Color.FromArgb(255, targetColorRed, targetColorGreen, targetColorBlue); //colorBalanceClass.fromAhsb(255, 15, (float)0.74, (float)0.5);


        public Bitmap vista_ScrollHorzShaft = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollHorzShaft, TargetColor);
        public Bitmap vista_ScrollHorzArrow = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollHorzArrow, TargetColor);
        public Bitmap vista_ScrollHorzThumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollHorzThumb, TargetColor);
        public Bitmap vista_ScrollVertShaft = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollVertShaft, TargetColor);
        public Bitmap vista_ScrollVertArrow = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollVertArrow, TargetColor);
        public Bitmap vista_ScrollVertThumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollVertThumb, TargetColor);
        public Bitmap vista_combo = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_combo, TargetColor);
        public Bitmap vista_command = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_command, TargetColor);
        public Bitmap vista_checkbox = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_checkbox, TargetColor);
        public Bitmap vista_header = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_header, TargetColor);
        public Bitmap vista_spin = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_spin, TargetColor);
        public Bitmap vista_radiobutton = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_radiobutton, TargetColor);
        public Bitmap vista_Tab = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_Tab, TargetColor);
        public Bitmap vista_thumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_thumb, TargetColor);
        public Bitmap vista_Track = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_Track, TargetColor);

        public Bitmap vienna_ScrollHorzShaft = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollHorzShaft, TargetColor);
        public Bitmap vienna_ScrollHorzArrow = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollHorzArrow, TargetColor);
        public Bitmap vienna_ScrollHorzThumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollHorzThumb, TargetColor);
        public Bitmap vienna_ScrollVertShaft = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollVertShaft, TargetColor);
        public Bitmap vienna_ScrollVertArrow = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollVertArrow, TargetColor);
        public Bitmap vienna_ScrollVertThumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollVertThumb, TargetColor);
        public Bitmap vienna_combo = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_combo, TargetColor);
        public Bitmap vienna_command = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_command, TargetColor);
        public Bitmap vienna_checkbox = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_checkbox, TargetColor);
        public Bitmap vienna_header = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_header, TargetColor);
        public Bitmap vienna_Progress = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_Progress, TargetColor);
        public Bitmap vienna_spin = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_spin, TargetColor);
        public Bitmap vienna_radiobutton = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_radiobutton, TargetColor);
        public Bitmap vienna_Tab = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_Tab, TargetColor);
        public Bitmap vienna_thumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_thumb, TargetColor);
        public Bitmap vienna_Track = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_Track, TargetColor);

        public Bitmap fader = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.fader, TargetColor);

        public Bitmap WindowFrame = cColorBalance.colorBalance((Bitmap)Properties.Resources.Border, TargetColor);

        public Bitmap vista_FrameLeft = cColorBalance.colorBalance((Bitmap)Properties.Resources.vista_FrameLeft, TargetColor);
        public Bitmap vista_FrameRight = cColorBalance.colorBalance((Bitmap)Properties.Resources.vista_FrameRight, TargetColor);
        public Bitmap vista_FrameTop = cColorBalance.colorBalance((Bitmap)Properties.Resources.vista_FrameTop, TargetColor);
        public Bitmap vista_FrameBottom = cColorBalance.colorBalance((Bitmap)Properties.Resources.vista_FrameBottom, TargetColor);

        public Bitmap vienna_FrameLeft = cColorBalance.colorBalance((Bitmap)Properties.Resources.vienna_FrameLeft, TargetColor);
        public Bitmap vienna_FrameRight = cColorBalance.colorBalance((Bitmap)Properties.Resources.vienna_FrameRight, TargetColor);
        public Bitmap vienna_FrameTop = cColorBalance.colorBalance((Bitmap)Properties.Resources.vienna_FrameTop, TargetColor);
        public Bitmap vienna_FrameBottom = cColorBalance.colorBalance((Bitmap)Properties.Resources.vienna_FrameBottom, TargetColor);

        public Color backColor = Color.FromArgb(255, ThemedColors.BackColor.R, ThemedColors.BackColor.G, ThemedColors.BackColor.B);
        public Color foreColor = Color.FromArgb(255, ThemedColors.ForeColor.R, ThemedColors.ForeColor.G, ThemedColors.ForeColor.B);
        public Color selectionColor = Color.FromArgb(255, ThemedColors.SelectionColor.R, ThemedColors.SelectionColor.G, ThemedColors.SelectionColor.B);
        public Color menuColor = Color.FromArgb(255, ThemedColors.MenuColor.R, ThemedColors.MenuColor.G, ThemedColors.MenuColor.B);
        public Color windowColor = Color.FromArgb(255, ThemedColors.WindowColor.R, ThemedColors.WindowColor.G, ThemedColors.WindowColor.B);
        public Color selectionForeColor = Color.FromArgb(255, ThemedColors.SelectionForeColor.R, ThemedColors.SelectionForeColor.G, ThemedColors.SelectionForeColor.B);
        public Color windowTextColor = Color.FromArgb(255, ThemedColors.WindowTextColor.R, ThemedColors.WindowTextColor.G, ThemedColors.WindowTextColor.B);
        public Color menuTextColor = Color.FromArgb(255, ThemedColors.MenuTextColor.R, ThemedColors.MenuTextColor.G, ThemedColors.MenuTextColor.B);
        public Color labelColor = Color.FromArgb(255, ThemedColors.LabelColor.R, ThemedColors.LabelColor.G, ThemedColors.LabelColor.B);
        public Color tabPageBorderColor = Color.FromArgb(255, ThemedColors.TabPageBorderColor.R, ThemedColors.TabPageBorderColor.G, ThemedColors.TabPageBorderColor.B);
        public Color gridColor = Color.FromArgb(255, ThemedColors.GridColor.R, ThemedColors.GridColor.G, ThemedColors.GridColor.B);
        public Color controlsColor = Color.FromArgb(255, ThemedColors.ControlsColor.R, ThemedColors.ControlsColor.G, ThemedColors.ControlsColor.B);
        public Color windowFrameColor = Color.FromArgb(255, ThemedColors.WindowFrameColor.R, ThemedColors.WindowFrameColor.G, ThemedColors.WindowFrameColor.B);
        public Color sliderColor = Color.FromArgb(255, ThemedColors.SliderColor.R, ThemedColors.SliderColor.G, ThemedColors.SliderColor.B);



        public void loadControlColors()
        {

            backColor = Color.FromArgb(255, ThemedColors.BackColor.R, ThemedColors.BackColor.G, ThemedColors.BackColor.B);
            foreColor = Color.FromArgb(255, ThemedColors.ForeColor.R, ThemedColors.ForeColor.G, ThemedColors.ForeColor.B);
            selectionColor = Color.FromArgb(255, ThemedColors.SelectionColor.R, ThemedColors.SelectionColor.G, ThemedColors.SelectionColor.B);
            menuColor = Color.FromArgb(255, ThemedColors.MenuColor.R, ThemedColors.MenuColor.G, ThemedColors.MenuColor.B);
            windowColor = Color.FromArgb(255, ThemedColors.WindowColor.R, ThemedColors.WindowColor.G, ThemedColors.WindowColor.B);
            selectionForeColor = Color.FromArgb(255, ThemedColors.SelectionForeColor.R, ThemedColors.SelectionForeColor.G, ThemedColors.SelectionForeColor.B);
            windowTextColor = Color.FromArgb(255, ThemedColors.WindowTextColor.R, ThemedColors.WindowTextColor.G, ThemedColors.WindowTextColor.B);
            menuTextColor = Color.FromArgb(255, ThemedColors.MenuTextColor.R, ThemedColors.MenuTextColor.G, ThemedColors.MenuTextColor.B);
            labelColor = Color.FromArgb(255, ThemedColors.LabelColor.R, ThemedColors.LabelColor.G, ThemedColors.LabelColor.B);
            tabPageBorderColor = Color.FromArgb(255, ThemedColors.TabPageBorderColor.R, ThemedColors.TabPageBorderColor.G, ThemedColors.TabPageBorderColor.B);
            gridColor = Color.FromArgb(255, ThemedColors.GridColor.R, ThemedColors.GridColor.G, ThemedColors.GridColor.B);
            controlsColor = Color.FromArgb(255, ThemedColors.ControlsColor.R, ThemedColors.ControlsColor.G, ThemedColors.ControlsColor.B);
            windowFrameColor = Color.FromArgb(255, ThemedColors.WindowFrameColor.R, ThemedColors.WindowFrameColor.G, ThemedColors.WindowFrameColor.B);
            sliderColor = Color.FromArgb(255, ThemedColors.SliderColor.R, ThemedColors.SliderColor.G, ThemedColors.SliderColor.B);

            //vista_ScrollHorzShaft = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollHorzShaft, backColor);
            //vista_ScrollHorzArrow = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollHorzArrow, backColor);
            //vista_ScrollHorzThumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollHorzThumb, backColor);
            //vista_ScrollVertShaft = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollVertShaft, backColor);
            //vista_ScrollVertArrow = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollVertArrow, backColor);
            //vista_ScrollVertThumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_ScrollVertThumb, backColor);
            //vista_combo = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_combo, windowColor);
            //vista_command = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_command, backColor);
            //vista_checkbox = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_checkbox, backColor);
            //vista_header = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_header, menuColor);
            //vista_Progress = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_Progress, foreColor);
            //vista_spin = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_spin, backColor);
            //vista_radiobutton = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_radiobutton, backColor);
            //vista_Tab = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_Tab, tabPageBorderColor);
            //vista_thumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_thumb, backColor);
            //vista_Track = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vista_Track, backColor);

            vienna_ScrollHorzShaft = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollHorzShaft, sliderColor);
            vienna_ScrollHorzArrow = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollHorzArrow, sliderColor);
            vienna_ScrollHorzThumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollHorzThumb, sliderColor);
            vienna_ScrollVertShaft = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollVertShaft, sliderColor);
            vienna_ScrollVertArrow = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollVertArrow, sliderColor);
            vienna_ScrollVertThumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_ScrollVertThumb, sliderColor);
            vienna_combo = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_combo, controlsColor);
            vienna_command = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_command, windowColor);
            vienna_checkbox = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_checkbox, controlsColor);
            vienna_header = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_header, windowColor);
            vienna_Progress = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_Progress, foreColor);
            vienna_spin = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_spin, controlsColor);
            vienna_radiobutton = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_radiobutton, controlsColor);
            vienna_Tab = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_Tab, tabPageBorderColor);
            vienna_thumb = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_thumb, sliderColor);
            vienna_Track = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.vienna_Track, controlsColor);


            fader = cColorBalance.colorBalance((Bitmap)ProGammaX.Properties.Resources.fader, foreColor);

        }

        //for enabled skin
        public static int targetColorRed = 99;
        public static int targetColorGreen = 99;
        public static int targetColorBlue = 99;
        //public void loadFrameImages()
        //{

        //    Color targetColor = windowFrameColor;// Color.FromArgb(255, targetColorRed, targetColorGreen, targetColorBlue);
        //    WindowFrame = cColorBalance.colorBalance((Bitmap)Properties.Resources.Border, targetColor);

        //    vista_FrameLeft = cColorBalance.colorBalance((Bitmap)Properties.Resources.vista_FrameLeft, targetColor);
        //    vista_FrameRight = cColorBalance.colorBalance((Bitmap)Properties.Resources.vista_FrameRight, targetColor);
        //    vista_FrameTop = cColorBalance.colorBalance((Bitmap)Properties.Resources.vista_FrameTop, targetColor);
        //    vista_FrameBottom = cColorBalance.colorBalance((Bitmap)Properties.Resources.vista_FrameBottom, targetColor);

        //    vienna_FrameLeft = cColorBalance.colorBalance((Bitmap)Properties.Resources.vienna_FrameLeft, targetColor);
        //    vienna_FrameRight = cColorBalance.colorBalance((Bitmap)Properties.Resources.vienna_FrameRight, targetColor);
        //    vienna_FrameTop = cColorBalance.colorBalance((Bitmap)Properties.Resources.vienna_FrameTop, targetColor);
        //    vienna_FrameBottom = cColorBalance.colorBalance((Bitmap)Properties.Resources.vienna_FrameBottom, targetColor);






        //    //this.Text = targetColorRed.ToString() + ", " + targetColorGreen.ToString() + ", " + targetColorBlue.ToString();


        //}






        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams cp = base.CreateParams;
        //        cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
        //        return cp;
        //    }
        //} 



        public void CustomizeButton(ref myButton button)
        {
            button.EnableCustomAppearance = MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked;

            button.EnableCustomAppearance = checkBox_OPTIONS_Themes_UseSkinOnControls.Checked;
            button.FlatStyle = FlatStyle.Standard;
            button.UseVisualStyleBackColor = true;
            button.BackColor = backColor;
            button.ForeColor = foreColor;

            if (MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
            {
                var customAppearance = new CustomButtonUIAppearance(button);
                customAppearance.EnableAntiAlias = true;
                customAppearance.BorderThicness = 2;
                customAppearance.BackColor = MainForm.mainFormRef.menuColor;
                customAppearance.CheckedForeColor = MainForm.mainFormRef.backColor;
                customAppearance.BorderColor = MainForm.mainFormRef.windowFrameColor;
                customAppearance.CheckedBackColor = MainForm.mainFormRef.selectionColor;
                customAppearance.CheckedBorderColor = MainForm.mainFormRef.windowFrameColor;
                customAppearance.MouseDownBackColor = MainForm.mainFormRef.windowColor;
                customAppearance.MouseDownBorderColor = MainForm.mainFormRef.windowFrameColor;
                customAppearance.MouseDownForeColor = MainForm.mainFormRef.foreColor;
                customAppearance.MouseHoverBackColor = MainForm.mainFormRef.selectionColor;
                customAppearance.MouseHoverBorderColor = MainForm.mainFormRef.windowFrameColor;
                customAppearance.MouseHoverForeColor = MainForm.mainFormRef.backColor;

                button.CustomAppearance = customAppearance;
            }
        }




        public void applySelectedTheme(Control controlToBeApplied = null)
        {


            if (controlToBeApplied == null)
            {
                controlToBeApplied = this;
            }















            //disableSystemColorSet = true;

            //reset the system colors back (for case when the system colors are used in themes)
            //SetSysColors(1, new[] { 13 }, new uint[] { startUpSystemColor_COLOR_HIGHLIGHT });
            //SetSysColors(1, new[] { 14 }, new uint[] { startUpSystemColor_COLOR_HIGHLIGHTTEXT });

            //myCOLOR_HIGHLIGHT = (uint)(new Rgb(ThemedColors.SelectionColor).ToInt32());
            //myCOLOR_HIGHLIGHTTEXT = (uint)(new Rgb(ThemedColors.SelectionForeColor).ToInt32());

            //SetSysColors(1, new[] { 13 }, new uint[] { myCOLOR_HIGHLIGHT });
            //SetSysColors(1, new[] { 14 }, new uint[] { myCOLOR_HIGHLIGHTTEXT });
            //disableSystemColorSet = false;

            //const int SW_HIDE = 0;
            //const int SW_SHOWNORMAL = 1;
            //const int SW_NORMAL = 1;
            //const int SW_SHOWMINIMIZED = 2;
            //const int SW_SHOWMAXIMIZED = 3;
            //const int SW_MAXIMIZE = 3;
            //const int SW_SHOWNOACTIVATE = 4;
            //const int SW_SHOW = 5;
            //const int SW_MINIMIZE = 6;
            //const int SW_SHOWMINNOACTIVE = 7;
            //const int SW_SHOWNA = 8;
            //const int SW_RESTORE = 9;
            //const int SW_SHOWDEFAULT = 10;
            //const int SW_MAX = 10;

            //reset the color back
            //// force window to have focus
            //uint foreThread = GetWindowThreadProcessId(GetForegroundWindow(), IntPtr.Zero);
            //uint appThread = GetCurrentThreadId();
            //if (this.Visible)
            //{
            //    if (foreThread != appThread)
            //    {
            //        AttachThreadInput(foreThread, appThread, true);
            //        //BringWindowToTop(this.Handle);
            //        ShowWindow(this.Handle, SW_SHOWNA);
            //        AttachThreadInput(foreThread, appThread, false);
            //    }
            //    else
            //    {
            //        BringWindowToBottom(this.Handle);
            //        ShowWindow(this.Handle, SW_SHOWNA);
            //    }
            //    //form.Activate();
            //}





            //fixes a bug with colors - system colors will only applied when mainform losts focus
            SetForegroundWindow(GetDesktopWindow());
            if (this.Visible)
            {
                this.Show();
                //this.Activate();
                //SetForegroundWindow((int)this.Handle);
            }




            //SetParent((IntPtr)GetDesktopWindow(), this.Handle);

            //SetSysColors(1, new[] { 0x0019 }, new uint[] { startUpSystemColor_COLOR_HIGHLIGHTTEXT });

            ////reset the system colors back (for case when the system colors are used in themes)
            //SetSysColors(1, new[] { 13 }, new uint[] { startUpSystemColor_COLOR_HIGHLIGHT });
            //SetSysColors(1, new[] { 14 }, new uint[] { startUpSystemColor_COLOR_HIGHLIGHTTEXT });


            //Win32.SetWindowPos((int)this.Handle, 0, 0, 0, 0, 0, Win32.SWP_NOMOVE | Win32.SWP_NOSIZE | Win32.SWP_SHOWWINDOW | 0x0010 | 0x0080);

            //myCOLOR_HIGHLIGHT = (uint)(new Rgb(ThemedColors.SelectionColor).ToInt32());
            //myCOLOR_HIGHLIGHTTEXT = (uint)(new Rgb(ThemedColors.SelectionForeColor).ToInt32());

            //SetSysColors(1, new[] { 13 }, new uint[] { myCOLOR_HIGHLIGHT });
            //SetSysColors(1, new[] { 14 }, new uint[] { myCOLOR_HIGHLIGHTTEXT });









            //Color backColor = ThemedColors.BackColor;
            //Color foreColor = ThemedColors.ForeColor;
            //Color selectionColor = ThemedColors.SelectionColor;
            //Color menuColor = ThemedColors.MenuColor;
            //Color windowColor = ThemedColors.WindowColor;
            //Color selectionForeColor = ThemedColors.SelectionForeColor;
            //Color windowTextColor = ThemedColors.WindowTextColor;
            //Color menuTextColor = ThemedColors.MenuTextColor;
            //Color labelColor = ThemedColors.LabelColor;
            //Color tabPageBorderColor = ThemedColors.TabPageBorderColor;
            //Color gridColor = ThemedColors.GridColor;

            //if (SetSysColors(1, new[] { 13 }, new uint[] { (uint)(new Rgb(SystemColors.Highlight)).ToInt32() }))
            //{
            //    //MessageBox.Show("s");
            //}
            ////set system color
            //uint old_color = GetSysColor(COLOR_HIGHLIGHT);
            //int[] lpaElements = { COLOR_HIGHLIGHT };
            ////I found that the r value and the blue value is contrary in mobile
            //int[] lpaRgbValues = { Color.Red.ToArgb() & 0x00ffffff };
            //bool res = SetSysColors(1, lpaElements, lpaRgbValues);
            //if (!res)
            //{
            //    int error = Marshal.GetLastWin32Error();
            //    throw new Exception(string.Format("SetSysColors() returned {0}!", error));
            //}

            //MessageBox.Show("OK");

            ////reset the color back
            //lpaRgbValues[0] = (int)old_color;
            //SetSysColors(1, lpaElements, lpaRgbValues);




            //DataGridView_MAIN_LIST.ScrollBars = ScrollBars.Vertical;
            //ScrollBarEx vScrollBar1 = new ScrollBarEx();
            //this.Panel_MAIN_List.Controls.Add(vScrollBar1);
            //vScrollBar1.BackColor = Color.Red;
            //vScrollBar1.Orientation = ScrollBarOrientation.Horizontal;
            //vScrollBar1.Dock = DockStyle.Bottom;
            //vScrollBar1.Scroll += (sender, e) =>
            //{

            // //this.DataGridView_MAIN_LIST.FirstDisplayedScrollingIndex += 10;

            //};

            // set skin

            //targetColorRed = 99;
            //targetColorGreen = 99;
            //targetColorBlue = 99;




            //_cRcm = new ProGammaX.cRCM(this.Handle);
            //loadControlColors();
            ////loadFrameImages(0);
            //loadControlImages(1);
            //_cRcm.Start();


            //Util.Find<HScrollBar>(this.DataGridView_MAIN_LIST).BackColor = Color.Red;



            //this.Font = new Font("Tahoma", 25, FontStyle.Regular, GraphicsUnit.Pixel);


            //SetSysColors(elements.Length, elements, colors);

            if (controlToBeApplied is myForm)
            {
                base.ApplyTheme((myForm)controlToBeApplied);
            }




            var panels = GetAllControls(controlToBeApplied, typeof(myPanel));
            var textBoxes = GetAllControls(controlToBeApplied, typeof(textBox));
            var tabPages = GetAllControls(controlToBeApplied, typeof(TabPage));
            var comboBoxes = GetAllControls(controlToBeApplied, typeof(myComboBox));
            var statusStrips = GetAllControls(controlToBeApplied, typeof(StatusStrip));
            var treeViews = GetAllControls(controlToBeApplied, typeof(treeView));
            var labels = GetAllControls(controlToBeApplied, typeof(Label));
            var buttons = GetAllControls(controlToBeApplied, typeof(myButton)).Where(c => c is myButton).GetEnumerator();
            var pictureBoxes = GetAllControls(controlToBeApplied, typeof(PictureBox));
            var menuStrips = GetAllControls(controlToBeApplied, typeof(MenuStrip));
            var toolStrips = GetAllControls(controlToBeApplied, typeof(ToolStrip));
            //var toolStripMenuItems = GetSelfAndChildrenRecursive(this).Where(c => c is ToolStripMenuItem);
            var numericUpDowns = GetAllControls(controlToBeApplied, typeof(NumericUpDown));
            var listBoxes = GetAllControls(controlToBeApplied, typeof(listBox));
            var checkBoxes = GetAllControls(controlToBeApplied, typeof(myCheckBox));
            var radioButtons = GetAllControls(controlToBeApplied, typeof(myRadioButton));
            var myListViews = GetAllControls(controlToBeApplied, typeof(listView));
            var progressBars = GetAllControls(controlToBeApplied, typeof(myProgressBar));



            controlToBeApplied.BackColor = backColor;
            foreach (TabPage control in tabPages)
            {

                control.BackColor = backColor;
                control.ForeColor = foreColor;
                control.ToolTipText = control.Text;

            }
            foreach (Control control in panels)
            {

                control.BackColor = backColor;
                control.ForeColor = foreColor;

            }

            foreach (myProgressBar control in progressBars)
            {
                if (checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    XPProgressBarRenderer customRenderer = new XPProgressBarRenderer(foreColor);
                    customRenderer.MarqueeStyle = MarqueeStyle.LeftRight;
                    control.Renderer = customRenderer;
                }
                else
                {
                    VistaProgressBarRenderer customRenderer = new VistaProgressBarRenderer(foreColor, foreColor);
                    control.Renderer = customRenderer;
                }

                control.BackColor = backColor;
                control.ForeColor = foreColor;
            }

            foreach (Control control in textBoxes)
            {

                control.BackColor = windowColor;
                control.ForeColor = windowTextColor;

            }

            foreach (myComboBox control in comboBoxes)
            {

                if (checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    control.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
                    //control.FlatStyle = FlatStyle.Flat;
                }
                else
                {
                    control.DrawMode = System.Windows.Forms.DrawMode.Normal;
                    //control.FlatStyle = FlatStyle.Standard;
                }


                control.BackColor = windowColor;
                control.ForeColor = windowTextColor;
            }
            //foreach (myComboBox control in comboBoxes2)
            //{
            //    control.BackColor = windowColor;
            //    control.ForeColor = foreColor;
            //}
            foreach (Control control in treeViews)
            {

                control.BackColor = windowColor;
                control.ForeColor = windowTextColor;

            }
            foreach (Control control in labels)
            {

                control.BackColor = Color.Transparent;
                control.ForeColor = labelColor;

            }

            foreach (Control control in statusStrips)
            {

                control.BackColor = Color.Transparent;
                control.ForeColor = labelColor;

            }
            
            if (buttons != null)
            {
                while (buttons.MoveNext())
                {
                    var button = (myButton)buttons.Current;

                    CustomizeButton(ref button);
                }
            }

            foreach (Control control in pictureBoxes)
            {

                control.BackColor = windowColor;
                control.ForeColor = windowTextColor;

            }
            foreach (Control control in menuStrips)
            {

                control.BackColor = menuColor;
                control.ForeColor = menuTextColor;


            }
            foreach (Control control in toolStrips)
            {

                control.BackColor = menuColor;
                control.ForeColor = menuTextColor;

            }
            //foreach (Control control in toolStripMenuItems)
            //{

            //    control.BackColor = menuColor;
            //    control.ForeColor = menuTextColor;

            //}
            foreach (Control control in numericUpDowns)
            {

                control.BackColor = backColor;
                control.ForeColor = foreColor;

            }

            foreach (myCheckBox control in checkBoxes)
            {
                control.EnableCustomUIAppearance = checkBox_OPTIONS_Themes_UseSkinOnControls.Checked;

                if (control.Appearance == Appearance.Button)
                {
                    control.FlatStyle = FlatStyle.Flat;
                    control.BackColor = backColor;
                    control.ForeColor = foreColor;
                }
                //control.UseVisualStyleBackColor = true;
                if (control.FlatStyle == FlatStyle.Standard && !checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    control.BackColor = Color.Transparent;
                    control.ForeColor = foreColor;
                }
                else
                {
                    control.BackColor = backColor;
                    control.ForeColor = foreColor;
                }

                control.UseVisualStyleBackColor = true;

                if (checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    if (control.Appearance == Appearance.Button)
                    {
                        control.Appearance = Appearance.Normal;
                    }

                    var customAppearance = new CustomCheckBoxUIAppearance(control);
                    customAppearance.BackColor = foreColor;
                    customAppearance.EnableAntiAlias = true;
                    customAppearance.TickColor = backColor;
                    customAppearance.TickThickness = 2;
                    customAppearance.BorderThicness = 1.5f;
                    customAppearance.BorderColor = windowFrameColor;
                    customAppearance.CheckedBorderColor = windowFrameColor;
                    customAppearance.CheckedBackColor = foreColor;

                    control.CustomAppearance = customAppearance;
                }
            }

            foreach (myRadioButton control in radioButtons)
            {
                control.EnableCustomUIAppearance = checkBox_OPTIONS_Themes_UseSkinOnControls.Checked;

                control.UseVisualStyleBackColor = true;
                if (control.FlatStyle == FlatStyle.Standard && !checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    control.UseVisualStyleBackColor = true;
                    control.BackColor = Color.Transparent;
                    control.ForeColor = foreColor;
                }
                else
                {
                    control.FlatStyle = FlatStyle.Standard;
                    control.UseVisualStyleBackColor = true;
                    control.BackColor = backColor;
                    control.ForeColor = foreColor;
                }

                if (checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    var customAppearance = new CustomCheckBoxUIAppearance(control);
                    customAppearance.BackColor = foreColor;
                    customAppearance.EnableAntiAlias = true;
                    customAppearance.TickColor = backColor;
                    customAppearance.TickThickness = 2;
                    customAppearance.BorderThicness = 1.5f;
                    customAppearance.BorderColor = windowFrameColor;
                    customAppearance.CheckedBorderColor = backColor;
                    customAppearance.CheckedBackColor = foreColor;

                    control.CustomAppearance = customAppearance;
                }
            }
            foreach (listView control in myListViews)
            {

                control.BackColor = windowColor;
                control.ForeColor = windowTextColor;

            }
            foreach (Control control in listBoxes)
            {

                control.BackColor = windowColor;
                control.ForeColor = windowTextColor;

            }



            if (controlToBeApplied == this)
            {



                //this.CheckBox_PROFILE_PROFILESETUP_Information_Favourite.BackColor = windowColor;
                //this.CheckBox_PROFILE_PROFILESETUP_Information_Favourite.BackColor = windowTextColor;

                this.ListView_PROFILE_DRIVES_ListOfMountedDrives.BackColor = windowColor;
                this.ListView_PROFILE_DRIVES_ListOfMountedDrives.ForeColor = windowTextColor;
                this.ListBox_DRIVES_IMAGEFILE_ImagesToMount.BackColor = windowColor;
                this.ListBox_DRIVES_IMAGEFILE_ImagesToMount.ForeColor = windowTextColor;
                this.ListBox_OPTIONS_Languages.BackColor = windowColor;
                this.ListBox_OPTIONS_Languages.ForeColor = windowTextColor;
                this.ListView_EXPLORER_FolderFiles.BackColor = windowColor;
                this.ListView_EXPLORER_FolderFiles.ForeColor = windowTextColor;
                this.ListView_GALLERY_Pictures.BackColor = windowColor;
                this.ListView_GALLERY_Pictures.ForeColor = windowTextColor;
                this.ListView_GALLERY_Sounds.BackColor = windowColor;
                this.ListView_GALLERY_Sounds.ForeColor = windowTextColor;
                this.ListView_GALLERY_Videos.BackColor = windowColor;
                this.ListView_GALLERY_Videos.ForeColor = windowTextColor;
                this.ListView_OPTIONS_Information_InformationLEFT.BackColor = windowColor;
                this.ListView_OPTIONS_Information_InformationLEFT.ForeColor = windowTextColor;
                this.ListView_OPTIONS_Templates_TemplateList.BackColor = windowColor;
                this.ListView_OPTIONS_Templates_TemplateList.ForeColor = windowTextColor;
                this.ListView_OPTIONS_Variables_VariablesLEFT.BackColor = windowColor;
                this.ListView_OPTIONS_Variables_VariablesLEFT.ForeColor = windowTextColor;

                this.ListView_MAIN_LIST.BackColor = windowColor;
                this.ListView_MAIN_LIST.ForeColor = windowTextColor;

                this.TextBox_PROFILE_DOSBOX_AUTOEXEC_AutoGenerated.BackColor = backColor;
                this.TextBox_PROFILE_DOSBOX_AUTOEXEC_ProgramStart.BackColor = backColor;


                this.BackColor = backColor;
                this.richTextBox_MAIN_Comment.BackColor = windowColor;
                this.richTextBox_PROFILE_CustomDosboxParameters.BackColor = windowColor;
                this.richTextBox_PROFILE_PROFILESETUP_Information_Comment.BackColor = windowColor;
                //this.Panel_GAMEiNFO_Split_up.BackColor = windowColor;
                //this.Panel_GAMEiNFO_Split_down.BackColor = backColor;
                //this.Panel_ALLGAMES_Left.BackColor = backColor;
                //this.Panel_MAIN_List.BackColor = backColor;
                //this.Panel_MAIN_Right_Comment.BackColor = backColor;


                this.TreeView_MainFilter.BackColor = windowColor;
                this.TreeView_MainFilter.ForeColor = windowTextColor;
                this.TreeView_EXPLORER_FolderList.BackColor = windowColor;
                this.TreeView_EXPLORER_FolderList.ForeColor = windowTextColor;
                this.TreeView_MultiEditCategories.BackColor = windowColor;
                this.TreeView_MultiEditCategories.ForeColor = windowTextColor;
                this.TreeView_OPTIONS_OptionCategories.BackColor = windowColor;
                this.TreeView_OPTIONS_OptionCategories.ForeColor = windowTextColor;
                this.TreeView_ProfileFilter.BackColor = windowColor;
                this.TreeView_ProfileFilter.ForeColor = windowTextColor;
                this.TreeView_WIZARD_WizardType.BackColor = windowColor;
                this.TreeView_WIZARD_WizardType.ForeColor = windowTextColor;



                this.DataGridView_MAIN_LIST.BackgroundColor = backColor;
                this.DataGridView_MAIN_LIST.DefaultCellStyle.ForeColor = windowTextColor;
                this.DataGridView_MAIN_LIST.DefaultCellStyle.BackColor = windowColor;
                this.DataGridView_MAIN_LIST.GridColor = gridColor;
                this.DataGridView_MAIN_LIST.DefaultCellStyle.SelectionBackColor = selectionColor;
                this.DataGridView_MAIN_LIST.DefaultCellStyle.SelectionForeColor = selectionForeColor;
                this.DataGridView_MAIN_LIST.RowTemplate.HeaderCell.Style.ForeColor = foreColor;
                this.DataGridView_MAIN_LIST.RowTemplate.HeaderCell.Style.BackColor = backColor;
                this.DataGridView_MAIN_LIST.ColumnHeadersDefaultCellStyle.BackColor = backColor;
                this.DataGridView_MAIN_LIST.ColumnHeadersDefaultCellStyle.ForeColor = foreColor;
                this.DataGridView_MAIN_LIST.EnableHeadersVisualStyles = false;



                this.DataGridView_MAIN_GameInfo.BackgroundColor = backColor;
                this.DataGridView_MAIN_GameInfo.GridColor = backColor;
                this.DataGridView_MAIN_GameInfo.DefaultCellStyle.ForeColor = foreColor;
                this.DataGridView_MAIN_GameInfo.DefaultCellStyle.BackColor = backColor;
                this.DataGridView_MAIN_GameInfo.RowTemplate.DefaultCellStyle.BackColor = backColor;
                this.DataGridView_MAIN_GameInfo.RowTemplate.DefaultCellStyle.SelectionBackColor = selectionColor;
                this.DataGridView_MAIN_GameInfo.RowTemplate.DefaultCellStyle.SelectionForeColor = selectionForeColor;
                this.DataGridView_MAIN_GameInfo.RowTemplate.HeaderCell.Style.ForeColor = foreColor;
                this.DataGridView_MAIN_GameInfo.RowTemplate.HeaderCell.Style.BackColor = backColor;






                this.DataGridView_MAIN_Groups.BackgroundColor = windowColor;
                this.DataGridView_MAIN_Groups.DefaultCellStyle.ForeColor = windowTextColor;
                this.DataGridView_MAIN_Groups.DefaultCellStyle.BackColor = backColor;
                this.DataGridView_MAIN_Groups.GridColor = windowColor;
                this.DataGridView_MAIN_Groups.DefaultCellStyle.SelectionBackColor = selectionColor;
                this.DataGridView_MAIN_Groups.DefaultCellStyle.SelectionForeColor = selectionForeColor;

                this.DataGridViewl_OPTIONS_MainList_ColumnsVisibilty.DefaultCellStyle.ForeColor = windowTextColor;
                this.DataGridViewl_OPTIONS_MainList_ColumnsVisibilty.DefaultCellStyle.BackColor = windowColor;
                this.DataGridViewl_OPTIONS_MainList_ColumnsVisibilty.GridColor = windowColor;
                this.DataGridViewl_OPTIONS_MainList_ColumnsVisibilty.DefaultCellStyle.SelectionBackColor = selectionColor;
                this.DataGridViewl_OPTIONS_MainList_ColumnsVisibilty.DefaultCellStyle.SelectionForeColor = selectionForeColor;

                this.richTextBox_MAIN_Comment.ForeColor = windowTextColor;
                this.richTextBox_MAIN_Comment.BackColor = windowColor;
                this.richTextBox_PROFILE_CustomDosboxParameters.ForeColor = windowTextColor;
                this.richTextBox_PROFILE_CustomDosboxParameters.BackColor = windowColor;
                this.richTextBox_PROFILE_PROFILESETUP_Information_Comment.ForeColor = windowTextColor;
                this.richTextBox_PROFILE_PROFILESETUP_Information_Comment.BackColor = windowColor;

                mySplitter1.BackColor = backColor;
                mySplitter2.BackColor = backColor;
                mySplitter3.BackColor = backColor;
                mySplitter4.BackColor = backColor;
                mySplitter5.BackColor = backColor;
                mySplitter6.BackColor = backColor;
                mySplitter7.BackColor = backColor;
                mySplitter8.BackColor = backColor;
                mySplitter9.BackColor = backColor;
                mySplitter10.BackColor = backColor;
                mySplitter11.BackColor = backColor;
                //this.MenuStrip1.BackColor = menuColor;
                //this.MenuStrip_EXPLORER.BackColor = menuColor;
                //this.MenuStrip_GALLERY.BackColor = menuColor;
                //this.MenuStrip_GROUPS.BackColor = menuColor;
                //this.MenuStrip_MAIN.BackColor = menuColor;
                //this.MenuStrip_PROFILE.BackColor = menuColor;
                //this.MenuStrip_SETTINGS.BackColor = menuColor;
                //this.toolStrip_PROFILE_CustomDosboxParameters.BackColor = menuColor;
                //this.ToolStrip_PROFILE_DOSBOX_AUTOEXEC_AtTheEnd.BackColor = menuColor;
                //this.ToolStrip_PROFILE_DOSBOX_AUTOEXEC_AutoexecBat.BackColor = menuColor;
                //this.ToolStrip_PROFILE_DOSBOX_AUTOEXEC_AutoGenerated.BackColor = menuColor;
                //this.ToolStrip_PROFILE_DOSBOX_AUTOEXEC_DosVariables.BackColor = menuColor;
                //this.ToolStrip_PROFILE_DOSBOX_AUTOEXEC_ProgramStart.BackColor = menuColor;
                //this.ToolStrip_PROFILE_Information_Comment.BackColor = menuColor;
                //this.ToolStrip_PROFILE_TEMPLATES.BackColor = menuColor;
                //this.ToolStrip_MAIN_Comment.BackColor = menuColor;

            }



            //loadControlImages(1, controlToBeApplied); // 1 = add controls 0 = reset controls

            //this.ActiveFormStyle = CreateFormStyle(); 

            this.Activate();

        }




















    }
}
