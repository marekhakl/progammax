﻿using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Drawing;

namespace ProGammaX
{
    public class ThemedColors
    {
        #region "    Variables and Constants "
           private static int themeIndex = 1;
           private static Color backColor;
           private static Color foreColor;
           private static Color selectionColor;
           private static Color menuColor;
           private static Color menuTextColor;
           private static Color disabledMenuTextColor;
           private static Color windowColor;
           private static Color windowTextColor;
           private static Color selectionForeColor;
           private static Color labelColor;
           private static Color tabPageBorderColor;
           private static Color gridColor;
           private static Color controlsColor;
           private static Color windowFrameColor;
           private static Color sliderColor;    

        #endregion

           public static int ThemeIndex { get { return themeIndex; } set { themeIndex = value; } }

           public static Color BackColor { get { return backColor; } set { backColor = value; } }

           public static Color ForeColor { get { return foreColor; } set { foreColor = value; } }

           public static Color SelectionColor { get { return selectionColor; } set { selectionColor = value; } }

           public static Color MenuColor { get { return menuColor; } set { menuColor = value; } }

           public static Color MenuTextColor { get { return menuTextColor; } set { menuTextColor = value; } }

           public static Color DisabledMenuTextColor { get { return disabledMenuTextColor; } set { disabledMenuTextColor = value; } }

           public static Color WindowColor { get { return windowColor; } set { windowColor = value; } }

           public static Color SelectionForeColor { get { return selectionForeColor; } set { selectionForeColor = value; } }

           public static Color WindowTextColor { get { return windowTextColor; } set { windowTextColor = value; } }

           public static Color LabelColor { get { return labelColor; } set { labelColor = value; } }

           public static Color TabPageBorderColor { get { return tabPageBorderColor; } set { tabPageBorderColor = value; } }

           public static Color GridColor { get { return gridColor; } set { gridColor = value; } }

           public static Color ControlsColor { get { return controlsColor; } set { controlsColor = value; } }

           public static Color WindowFrameColor { get { return windowFrameColor; } set { windowFrameColor = value; } }

           public static Color SliderColor { get { return sliderColor; } set { sliderColor = value; } }



        public ThemedColors()
        {
          
            
        }
 
   

        static ThemedColors()
        {


            backColor = setThemedColors(getThemeIndex.getTheIndex())[0];
            foreColor = setThemedColors(getThemeIndex.getTheIndex())[1];
            selectionColor = setThemedColors(getThemeIndex.getTheIndex())[2];
            menuColor = setThemedColors(getThemeIndex.getTheIndex())[3];
            menuTextColor = setThemedColors(getThemeIndex.getTheIndex())[4];
            disabledMenuTextColor = setThemedColors(getThemeIndex.getTheIndex())[5];
            windowColor = setThemedColors(getThemeIndex.getTheIndex())[6];
            windowTextColor = setThemedColors(getThemeIndex.getTheIndex())[7];
            selectionForeColor = setThemedColors(getThemeIndex.getTheIndex())[8];
            labelColor = setThemedColors(getThemeIndex.getTheIndex())[9];
            tabPageBorderColor = setThemedColors(getThemeIndex.getTheIndex())[10];
            gridColor = setThemedColors(getThemeIndex.getTheIndex())[11];
            controlsColor = setThemedColors(getThemeIndex.getTheIndex())[12];
            windowFrameColor = setThemedColors(getThemeIndex.getTheIndex())[13];
            sliderColor = setThemedColors(getThemeIndex.getTheIndex())[14];



        }

        private static Color[] setThemedColors(int themeindex = 0)
        {

            switch (themeindex)
            {
                
                case 0:
                    return new Color[]
                    { 
                    backColor = Color.Lavender,
                    foreColor = Color.DarkSlateGray,
                    selectionColor = Color.Honeydew,
                    menuColor = Color.NavajoWhite,
                    menuTextColor = Color.DarkSlateGray,
                    disabledMenuTextColor = Color.DarkGray,
                    windowColor = Color.LightSteelBlue,
                    selectionForeColor = Color.DarkSlateGray,
                    windowTextColor = Color.DarkSlateGray,
                    labelColor = Color.DarkSlateGray,
                    tabPageBorderColor = Color.DarkSlateGray,
                    gridColor = Color.DarkSlateGray,
                    controlsColor = Color.Beige,
                    windowFrameColor = Color.DarkBlue,
                    sliderColor = Color.Gray
                    };

                    //break;
                case 1:
                    return new Color[]
                    { 
                    backColor = Color.FromArgb(250, 247, 240),
                    foreColor = Color.Black,
                    selectionColor = Color.FromArgb(98, 98, 126),
                    menuColor = Color.Linen,
                    menuTextColor = Color.DarkSlateGray,
                    disabledMenuTextColor = Color.DarkGray,
                    windowColor = Color.Snow,
                    selectionForeColor = Color.Beige,
                    windowTextColor = Color.FromArgb(22, 22, 44),
                    labelColor = Color.FromArgb(22, 22, 44),
                    tabPageBorderColor = Color.FromArgb(225, 225, 200),
                    gridColor = Color.LightGray,   
                    controlsColor = Color.Beige,
                    windowFrameColor = Color.Gray,
                    sliderColor = Color.Gray
                    };
                    //break;
                default:
                    return new Color[]
                    { 
                    backColor = Color.FromArgb(250, 247, 240),
                    foreColor = Color.Black,
                    selectionColor = Color.FromArgb(98, 98, 126),
                    menuColor = Color.Linen,
                    menuTextColor = Color.DarkSlateGray,
                    disabledMenuTextColor = Color.DarkGray,
                    windowColor = Color.Snow,
                    selectionForeColor = Color.Beige,
                    windowTextColor = Color.FromArgb(22, 22, 44),
                    labelColor = Color.FromArgb(22, 22, 44),
                    tabPageBorderColor = Color.FromArgb(225, 225, 200),
                    gridColor = Color.LightGray,
                    controlsColor = Color.Beige,
                    windowFrameColor = Color.DarkBlue,
                    sliderColor = Color.Gray   
                    };
                    //break;











            }





        }








    
    }


    static class getThemeIndex
    {
        public static int i = ThemedColors.ThemeIndex;

        public static int getTheIndex()
        {

            return i;
        }

    }


}
