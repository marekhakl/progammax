﻿using System.Drawing;
using System.Drawing.Imaging;

namespace ProGammaX
{
    public static class cColorBalance
    {




        public static Bitmap MakeGrayscale3(ref Bitmap original)
        {
            Graphics g = Graphics.FromImage(original);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(
               new float[][]
                  {
                     new float[] {.3f, .3f, .3f, 0, 0},
                     new float[] {.59f, .59f, .59f, 0, 0},
                     new float[] {.11f, .11f, .11f, 0, 0},
                     new float[] {0, 0, 0, 1, 0},
                     new float[] {0, 0, 0, 0, 1}
                  });

            ImageAttributes attributes = new ImageAttributes();
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();

            return original;
        }






        ///// <summary>
        ///// Creates a Color from alpha, hue, saturation and brightness.
        ///// </summary>
        ///// <param name="alpha">The alpha channel value.</param>
        ///// <param name="hue">The hue value.</param>
        ///// <param name="saturation">The saturation value.</param>
        ///// <param name="brightness">The brightness value.</param>
        ///// <returns>A Color with the given values.</returns>
        //public static Color fromAhsb(int alpha, float hue, float saturation, float brightness)
        //{

        //    if (0 > alpha || 255 < alpha)
        //    {
        //        throw new ArgumentOutOfRangeException("alpha", alpha,
        //          "Value must be within a range of 0 - 255.");
        //    }
        //    if (0f > hue || 360f < hue)
        //    {
        //        throw new ArgumentOutOfRangeException("hue", hue,
        //          "Value must be within a range of 0 - 360.");
        //    }
        //    if (0f > saturation || 1f < saturation)
        //    {
        //        throw new ArgumentOutOfRangeException("saturation", saturation,
        //          "Value must be within a range of 0 - 1.");
        //    }
        //    if (0f > brightness || 1f < brightness)
        //    {
        //        throw new ArgumentOutOfRangeException("brightness", brightness,
        //          "Value must be within a range of 0 - 1.");
        //    }

        //    if (0 == saturation)
        //    {
        //        return Color.FromArgb(alpha, Convert.ToInt32(brightness * 255),
        //          Convert.ToInt32(brightness * 255), Convert.ToInt32(brightness * 255));
        //    }

        //    float fMax, fMid, fMin;
        //    int iSextant, iMax, iMid, iMin;

        //    if (0.5 < brightness)
        //    {
        //        fMax = brightness - (brightness * saturation) + saturation;
        //        fMin = brightness + (brightness * saturation) - saturation;
        //    }
        //    else
        //    {
        //        fMax = brightness + (brightness * saturation);
        //        fMin = brightness - (brightness * saturation);
        //    }

        //    iSextant = (int)Math.Floor(hue / 60f);
        //    if (300f <= hue)
        //    {
        //        hue -= 360f;
        //    }
        //    hue /= 60f;
        //    hue -= 2f * (float)Math.Floor(((iSextant + 1f) % 6f) / 2f);
        //    if (0 == iSextant % 2)
        //    {
        //        fMid = hue * (fMax - fMin) + fMin;
        //    }
        //    else
        //    {
        //        fMid = fMin - hue * (fMax - fMin);
        //    }

        //    iMax = Convert.ToInt32(fMax * 255);
        //    iMid = Convert.ToInt32(fMid * 255);
        //    iMin = Convert.ToInt32(fMin * 255);

        //    switch (iSextant)
        //    {
        //        case 1:
        //            return Color.FromArgb(alpha, iMid, iMax, iMin);
        //        case 2:
        //            return Color.FromArgb(alpha, iMin, iMax, iMid);
        //        case 3:
        //            return Color.FromArgb(alpha, iMin, iMid, iMax);
        //        case 4:
        //            return Color.FromArgb(alpha, iMid, iMin, iMax);
        //        case 5:
        //            return Color.FromArgb(alpha, iMax, iMin, iMid);
        //        default:
        //            return Color.FromArgb(alpha, iMax, iMid, iMin);
        //    }
        //}











        public static Bitmap colorBalance(Bitmap tempBitmap, Color targetColor)
        {
            float alpha = (float)targetColor.A / 128f;
            float red = (float)targetColor.R / 128f;
            float green = (float)targetColor.G / 128f;
            float blue = (float)targetColor.B / 128f;

            MakeGrayscale3(ref tempBitmap);

            float[][] colorMatrixElements = {
                       new float[] {red,  0,  0,  0, 0},
                       new float[] {0,  green,  0,  0, 0},
                       new float[] {0,  0,  blue,  0, 0},
                       new float[] {0,  0,  0,  1, 0},
                       new float[] {0, 0, 0, 0, 1}};

            ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);

            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            Graphics graphics = Graphics.FromImage(tempBitmap);

            graphics.DrawImage(tempBitmap, new Rectangle(0, 0, tempBitmap.Width, tempBitmap.Height), 0, 0, tempBitmap.Width, tempBitmap.Height, GraphicsUnit.Pixel, imageAttributes);
            graphics.Dispose();

            return tempBitmap;
        }





















    }





}
