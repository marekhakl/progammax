﻿/*
 * This code is provided under the Code Project Open Licence (CPOL)
 * See http://www.codeproject.com/info/cpol10.aspx for details
 */

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace ProGammaX
{

    [System.ComponentModel.ToolboxItem(false)]
    public class TabStyleDefaultProvider : TabStyleProvider
    {
        public TabStyleDefaultProvider(myTabControl tabControl) : base(tabControl)
        {
            this._FocusTrack = false;
            this._Radius = 2;
            this._ShowTabCloser = false;
            //this._CloserColorActive = Color.Black;
            //this._CloserColor = Color.FromArgb(117, 99, 61);
            //this._TextColor = ThemedColors.ForeColor;
            //this._TextColorDisabled = ThemedColors.DisabledMenuTextColor;
            //this._BorderColor = Color.Transparent;
            //this._BorderColorHot = ThemedColors.SelectionColor;
            //this._TextColorSelected = ThemedColors.SelectionColor;
        }

        protected override Brush GetTabBackgroundBrush(int index)
        {
            LinearGradientBrush fillBrush = null;

            //	Capture the colours dependant on selection state of the tab
            Color dark = Color.Transparent;
            Color light = Color.Transparent;

            if (this._TabControl.SelectedIndex == index)
            {
                dark = ThemedColors.WindowColor;
                light = ThemedColors.WindowColor;
            }
            else if (!this._TabControl.TabPages[index].Enabled)
            {
                light = ThemedColors.TabPageBorderColor;
                dark = ThemedColors.WindowColor;
            }
            else if (this.HotTrack && index == this._TabControl.ActiveIndex)
            {
                //	Enable hot tracking
                dark = ThemedColors.WindowColor;
                light = ThemedColors.WindowColor;
            }

            //	Get the correctly aligned gradient
            Rectangle tabBounds = this.GetTabRect(index);
            tabBounds.Inflate(3, 3);
            tabBounds.X -= 1;
            tabBounds.Y -= 1;
            switch (this._TabControl.Alignment)
            {
                case TabAlignment.Top:
                    fillBrush = new LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Vertical);
                    break;
                case TabAlignment.Bottom:
                    fillBrush = new LinearGradientBrush(tabBounds, dark, light, LinearGradientMode.Vertical);
                    break;
                case TabAlignment.Left:
                    fillBrush = new LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Horizontal);
                    break;
                case TabAlignment.Right:
                    fillBrush = new LinearGradientBrush(tabBounds, dark, light, LinearGradientMode.Horizontal);
                    break;
            }

            //	Add the blend
            fillBrush.Blend = GetBackgroundBlend();

            return fillBrush;
        }

        private static Blend GetBackgroundBlend()
        {
            float[] relativeIntensities = new float[] { 0f, 0.5f, 1f, 1f };
            float[] relativePositions = new float[] { 0f, 0.5f, 0.51f, 1f };


            Blend blend = new Blend();
            blend.Factors = relativeIntensities;
            blend.Positions = relativePositions;

            return blend;
        }

        public override Brush GetPageBackgroundBrush(int index)
        {

            //	Capture the colours dependant on selection state of the tab
            Color light = Color.Transparent;

            //if (this._TabControl.SelectedIndex == index)
            //{
            //    light = ThemedColors.SelectionForeColor;
            //}
            //else if (!this._TabControl.TabPages[index].Enabled)
            //{
            //    light = Color.Transparent;
            //}
            //else if (this._HotTrack && index == this._TabControl.ActiveIndex)
            //{
            //    //	Enable hot tracking
            //    light = Color.Transparent;
            //}

            return new SolidBrush(light);
        }

        public override void AddTabBorder(System.Drawing.Drawing2D.GraphicsPath path, System.Drawing.Rectangle tabBounds)
        {
            switch (this._TabControl.Alignment)
            {
                case TabAlignment.Top:
                    path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y);
                    path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y);
                    path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom);
                    break;
                case TabAlignment.Bottom:
                    path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom);
                    path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom);
                    path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y);
                    break;
                case TabAlignment.Left:
                    path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom);
                    path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y);
                    path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y);
                    break;
                case TabAlignment.Right:
                    path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y);
                    path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom);
                    path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom);
                    break;
            }
        }

        public override Rectangle GetTabRect(int index)
        {
            if (index < 0)
            {
                return new Rectangle();
            }

            Rectangle tabBounds = base.GetTabRect(index);
            bool firstTabinRow = this._TabControl.IsFirstTabInRow(index);

            //	Make non-SelectedTabs smaller and selected tab bigger
            if (index != this._TabControl.SelectedIndex)
            {
                switch (this._TabControl.Alignment)
                {
                    case TabAlignment.Top:
                        tabBounds.Y += 1;
                        tabBounds.Height -= 1;
                        break;
                    case TabAlignment.Bottom:
                        tabBounds.Height -= 1;
                        break;
                    case TabAlignment.Left:
                        tabBounds.X += 1;
                        tabBounds.Width -= 1;
                        break;
                    case TabAlignment.Right:
                        tabBounds.Width -= 1;
                        break;
                }
            }
            else
            {
                switch (this._TabControl.Alignment)
                {
                    case TabAlignment.Top:
                        if (tabBounds.Y > 0)
                        {
                            tabBounds.Y -= 1;
                            tabBounds.Height += 1;
                        }

                        if (firstTabinRow)
                        {
                            tabBounds.Width += 1;
                        }
                        else
                        {
                            tabBounds.X -= 1;
                            tabBounds.Width += 2;
                        }
                        break;
                    case TabAlignment.Bottom:
                        if (tabBounds.Bottom < this._TabControl.Bottom)
                        {
                            tabBounds.Height += 1;
                        }
                        if (firstTabinRow)
                        {
                            tabBounds.Width += 1;
                        }
                        else
                        {
                            tabBounds.X -= 1;
                            tabBounds.Width += 2;
                        }
                        break;
                    case TabAlignment.Left:
                        if (tabBounds.X > 0)
                        {
                            tabBounds.X -= 1;
                            tabBounds.Width += 1;
                        }

                        if (firstTabinRow)
                        {
                            tabBounds.Height += 1;
                        }
                        else
                        {
                            tabBounds.Y -= 1;
                            tabBounds.Height += 2;
                        }
                        break;
                    case TabAlignment.Right:
                        if (tabBounds.Right < this._TabControl.Right)
                        {
                            tabBounds.Width += 1;
                        }
                        if (firstTabinRow)
                        {
                            tabBounds.Height += 1;
                        }
                        else
                        {
                            tabBounds.Y -= 1;
                            tabBounds.Height += 2;
                        }
                        break;
                }
            }

            //	Adjust first tab in the row to align with tabpage
            this.EnsureFirstTabIsInView(ref tabBounds, index);

            return tabBounds;
        }
    }
}
