﻿//namespace ProGammaX
//{
//    using System;
//    using System.ComponentModel;
//    using System.Drawing;
//    using System.Runtime.InteropServices;
//    using System.Windows.Forms;
//    using System.ComponentModel;
//    using System.Windows.Forms.Design;
//    using System;
//    using System.Drawing;
//    using System.Drawing.Drawing2D;
//    using System.Drawing.Imaging;


//    /// <summary>
//    /// The scrollbar states.
//    /// </summary>
//    internal enum ScrollBarState
//    {
//        /// <summary>
//        /// Indicates a normal scrollbar state.
//        /// </summary>
//        Normal,

//        /// <summary>
//        /// Indicates a hot scrollbar state.
//        /// </summary>
//        Hot,

//        /// <summary>
//        /// Indicates an active scrollbar state.
//        /// </summary>
//        Active,

//        /// <summary>
//        /// Indicates a pressed scrollbar state.
//        /// </summary>
//        Pressed,

//        /// <summary>
//        /// Indicates a disabled scrollbar state.
//        /// </summary>
//        Disabled
//    }

//    /// <summary>
//    /// Enum for the scrollbar orientation.
//    /// </summary>
//    public enum ScrollBarOrientation
//    {
//        /// <summary>
//        /// Indicates a horizontal scrollbar.
//        /// </summary>
//        Horizontal,

//        /// <summary>
//        /// Indicates a vertical scrollbar.
//        /// </summary>
//        Vertical
//    }
//    /// <summary>
//    /// The scrollbar renderer class.
//    /// </summary>
//    internal static class ScrollBarExRenderer
//    {
//        #region fields

//        /// <summary>
//        /// The colors of the thumb in the 3 states.
//        /// </summary>
//        private static Color[,] thumbColors = new Color[3, 8];

//        /// <summary>
//        /// The arrow colors in the three states.
//        /// </summary>
//        private static Color[,] arrowColors = new Color[3, 8];

//        /// <summary>
//        /// The arrow border colors.
//        /// </summary>
//        private static Color[] arrowBorderColors = new Color[4];

//        /// <summary>
//        /// The background colors.
//        /// </summary>
//        private static Color[] backgroundColors = new Color[5];

//        /// <summary>
//        /// The track colors.
//        /// </summary>
//        private static Color[] trackColors = new Color[2];

//        #endregion

//        #region constructor

//        /// <summary>
//        /// Initializes static members of the <see cref="ScrollBarExRenderer"/> class.
//        /// </summary>
//        static ScrollBarExRenderer()
//        {
//            // hot state
//            thumbColors[0, 0] = Color.FromArgb(96, 111, 148); // border color
//            thumbColors[0, 1] = Color.FromArgb(232, 233, 233); // left/top start color
//            thumbColors[0, 2] = Color.FromArgb(230, 233, 241); // left/top end color
//            thumbColors[0, 3] = Color.FromArgb(233, 237, 242); // right/bottom line color
//            thumbColors[0, 4] = Color.FromArgb(209, 218, 228); // right/bottom start color
//            thumbColors[0, 5] = Color.FromArgb(218, 227, 235); // right/bottom end color
//            thumbColors[0, 6] = Color.FromArgb(190, 202, 219); // right/bottom middle color
//            thumbColors[0, 7] = Color.FromArgb(96, 11, 148); // left/top line color

//            // over state
//            thumbColors[1, 0] = Color.FromArgb(60, 110, 176);
//            thumbColors[1, 1] = Color.FromArgb(187, 204, 228);
//            thumbColors[1, 2] = Color.FromArgb(205, 227, 254);
//            thumbColors[1, 3] = Color.FromArgb(252, 253, 255);
//            thumbColors[1, 4] = Color.FromArgb(170, 207, 247);
//            thumbColors[1, 5] = Color.FromArgb(219, 232, 251);
//            thumbColors[1, 6] = Color.FromArgb(190, 202, 219);
//            thumbColors[1, 7] = Color.FromArgb(233, 233, 235);

//            // pressed state
//            thumbColors[2, 0] = Color.FromArgb(23, 73, 138);
//            thumbColors[2, 1] = Color.FromArgb(154, 184, 225);
//            thumbColors[2, 2] = Color.FromArgb(166, 202, 250);
//            thumbColors[2, 3] = Color.FromArgb(221, 235, 251);
//            thumbColors[2, 4] = Color.FromArgb(110, 166, 240);
//            thumbColors[2, 5] = Color.FromArgb(194, 218, 248);
//            thumbColors[2, 6] = Color.FromArgb(190, 202, 219);
//            thumbColors[2, 7] = Color.FromArgb(194, 211, 231);

//            /* picture of colors and indices
//             *(0,0)
//             * -----------------------------------------------
//             * |                                             |
//             * | |-----------------------------------------| |
//             * | |                  (2)                    | |
//             * | | |-------------------------------------| | |
//             * | | |                (0)                  | | |
//             * | | |                                     | | |
//             * | | |                                     | | |
//             * | |3|                (1)                  |3| |
//             * | |6|                (4)                  |6| |
//             * | | |                                     | | |
//             * | | |                (5)                  | | |
//             * | | |-------------------------------------| | |
//             * | |                  (12)                   | |
//             * | |-----------------------------------------| |
//             * |                                             |
//             * ----------------------------------------------- (15,17)
//             */

//            // hot state
//            arrowColors[0, 0] = Color.FromArgb(223, 236, 252);
//            arrowColors[0, 1] = Color.FromArgb(207, 225, 248);
//            arrowColors[0, 2] = Color.FromArgb(245, 249, 255);
//            arrowColors[0, 3] = Color.FromArgb(237, 244, 252);
//            arrowColors[0, 4] = Color.FromArgb(244, 249, 255);
//            arrowColors[0, 5] = Color.FromArgb(244, 249, 255);
//            arrowColors[0, 6] = Color.FromArgb(251, 253, 255);
//            arrowColors[0, 7] = Color.FromArgb(251, 253, 255);

//            // over state
//            arrowColors[1, 0] = Color.FromArgb(205, 222, 243);
//            arrowColors[1, 1] = Color.FromArgb(186, 208, 235);
//            arrowColors[1, 2] = Color.FromArgb(238, 244, 252);
//            arrowColors[1, 3] = Color.FromArgb(229, 237, 247);
//            arrowColors[1, 4] = Color.FromArgb(223, 234, 247);
//            arrowColors[1, 5] = Color.FromArgb(241, 246, 254);
//            arrowColors[1, 6] = Color.FromArgb(243, 247, 252);
//            arrowColors[1, 7] = Color.FromArgb(250, 252, 255);

//            // pressed state
//            arrowColors[2, 0] = Color.FromArgb(215, 220, 225);
//            arrowColors[2, 1] = Color.FromArgb(195, 202, 210);
//            arrowColors[2, 2] = Color.FromArgb(242, 244, 245);
//            arrowColors[2, 3] = Color.FromArgb(232, 235, 238);
//            arrowColors[2, 4] = Color.FromArgb(226, 228, 230);
//            arrowColors[2, 5] = Color.FromArgb(230, 233, 236);
//            arrowColors[2, 6] = Color.FromArgb(244, 245, 245);
//            arrowColors[2, 7] = Color.FromArgb(245, 247, 248);

//            // background colors
//            backgroundColors[0] = Color.FromArgb(235, 237, 239);
//            backgroundColors[1] = Color.FromArgb(252, 252, 252);
//            backgroundColors[2] = Color.FromArgb(247, 247, 247);
//            backgroundColors[3] = Color.FromArgb(238, 238, 238);
//            backgroundColors[4] = Color.FromArgb(240, 240, 240);

//            // track colors
//            trackColors[0] = Color.FromArgb(204, 204, 204);
//            trackColors[1] = Color.FromArgb(220, 220, 220);

//            // arrow border colors
//            arrowBorderColors[0] = Color.FromArgb(135, 146, 160);
//            arrowBorderColors[1] = Color.FromArgb(140, 151, 165);
//            arrowBorderColors[2] = Color.FromArgb(128, 139, 153);
//            arrowBorderColors[3] = Color.FromArgb(99, 110, 125);
//        }

//        #endregion

//        #region methods

//        #region public methods

//        /// <summary>
//        /// Draws the background.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        /// <param name="orientation">The <see cref="ScrollBarOrientation"/>.</param>
//        public static void DrawBackground(
//           Graphics g,
//           Rectangle rect,
//           ScrollBarOrientation orientation)
//        {
//            if (g == null)
//            {
//                throw new ArgumentNullException("g");
//            }

//            if (rect.IsEmpty || g.IsVisibleClipEmpty
//               || !g.VisibleClipBounds.IntersectsWith(rect))
//            {
//                return;
//            }

//            if (orientation == ScrollBarOrientation.Vertical)
//            {
//                DrawBackgroundVertical(g, rect);
//            }
//            else
//            {
//                DrawBackgroundHorizontal(g, rect);
//            }
//        }

//        /// <summary>
//        /// Draws the channel ( or track ).
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        /// <param name="state">The scrollbar state.</param>
//        /// <param name="orientation">The <see cref="ScrollBarOrientation"/>.</param>
//        public static void DrawTrack(
//           Graphics g,
//           Rectangle rect,
//           ScrollBarState state,
//           ScrollBarOrientation orientation)
//        {
//            if (g == null)
//            {
//                throw new ArgumentNullException("g");
//            }

//            if (rect.Width <= 0 || rect.Height <= 0
//               || state != ScrollBarState.Pressed || g.IsVisibleClipEmpty
//               || !g.VisibleClipBounds.IntersectsWith(rect))
//            {
//                return;
//            }

//            if (orientation == ScrollBarOrientation.Vertical)
//            {
//                DrawTrackVertical(g, rect);
//            }
//            else
//            {
//                DrawTrackHorizontal(g, rect);
//            }
//        }

//        /// <summary>
//        /// Draws the thumb.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        /// <param name="state">The <see cref="ScrollBarState"/> of the thumb.</param>
//        /// <param name="orientation">The <see cref="ScrollBarOrientation"/>.</param>
//        public static void DrawThumb(
//           Graphics g,
//           Rectangle rect,
//           ScrollBarState state,
//           ScrollBarOrientation orientation)
//        {
//            if (g == null)
//            {
//                throw new ArgumentNullException("g");
//            }

//            if (rect.IsEmpty || g.IsVisibleClipEmpty
//               || !g.VisibleClipBounds.IntersectsWith(rect)
//               || state == ScrollBarState.Disabled)
//            {
//                return;
//            }

//            if (orientation == ScrollBarOrientation.Vertical)
//            {
//                DrawThumbVertical(g, rect, state);
//            }
//            else
//            {
//                DrawThumbHorizontal(g, rect, state);
//            }
//        }

//        /// <summary>
//        /// Draws the grip of the thumb.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        /// <param name="orientation">The <see cref="ScrollBarOrientation"/>.</param>
//        public static void DrawThumbGrip(
//           Graphics g,
//           Rectangle rect,
//           ScrollBarOrientation orientation)
//        {
//            if (g == null)
//            {
//                throw new ArgumentNullException("g");
//            }

//            if (rect.IsEmpty || g.IsVisibleClipEmpty
//               || !g.VisibleClipBounds.IntersectsWith(rect))
//            {
//                return;
//            }

//            // get grip image
//            using (Image gripImage = ProGammaX.Properties.Resources.GripNormal)
//            {
//                // adjust rectangle and rotate grip image if necessary
//                Rectangle r = AdjustThumbGrip(rect, orientation, gripImage);

//                // adjust alpha channel of grip image
//                using (ImageAttributes attr = new ImageAttributes())
//                {
//                    attr.SetColorMatrix(
//                       new ColorMatrix(new float[][] {
//                  new[] { 1F, 0, 0, 0, 0 },
//                  new[] { 0, 1F, 0, 0, 0 },
//                  new[] { 0, 0, 1F, 0, 0 },
//                  new[] { 0, 0, 0,  .8F, 0 },
//                  new[] { 0, 0, 0, 0, 1F }
//                  }),
//                       ColorMatrixFlag.Default,
//                       ColorAdjustType.Bitmap
//                    );

//                    // draw grip image
//                    g.DrawImage(gripImage, r, 0, 0, r.Width, r.Height, GraphicsUnit.Pixel, attr);
//                }
//            }
//        }

//        /// <summary>
//        /// Draws an arrow button.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        /// <param name="state">The <see cref="ScrollBarArrowButtonState"/> of the arrow button.</param>
//        /// <param name="arrowUp">true for an up arrow, false otherwise.</param>
//        /// <param name="orientation">The <see cref="ScrollBarOrientation"/>.</param>
//        public static void DrawArrowButton(
//           Graphics g,
//           Rectangle rect,
//           ScrollBarArrowButtonState state,
//           bool arrowUp,
//           ScrollBarOrientation orientation)
//        {
//            if (g == null)
//            {
//                throw new ArgumentNullException("g");
//            }

//            if (rect.IsEmpty || g.IsVisibleClipEmpty
//               || !g.VisibleClipBounds.IntersectsWith(rect))
//            {
//                return;
//            }

//            if (orientation == ScrollBarOrientation.Vertical)
//            {
//                DrawArrowButtonVertical(g, rect, state, arrowUp);
//            }
//            else
//            {
//                DrawArrowButtonHorizontal(g, rect, state, arrowUp);
//            }
//        }

//        #endregion

//        #region private methods

//        /// <summary>
//        /// Draws the background.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        private static void DrawBackgroundVertical(Graphics g, Rectangle rect)
//        {
//            using (Pen p = new Pen(backgroundColors[0]))
//            {
//                g.DrawLine(p, rect.Left + 1, rect.Top + 1, rect.Left + 1, rect.Bottom - 1);
//                g.DrawLine(p, rect.Right - 2, rect.Top + 1, rect.Right - 2, rect.Bottom - 1);
//            }

//            using (Pen p = new Pen(backgroundColors[1]))
//            {
//                g.DrawLine(p, rect.Left + 2, rect.Top + 1, rect.Left + 2, rect.Bottom - 1);
//            }

//            Rectangle firstRect = new Rectangle(
//               rect.Left + 3,
//               rect.Top,
//               8,
//               rect.Height);

//            Rectangle secondRect = new Rectangle(
//               firstRect.Right - 1,
//               firstRect.Top,
//               7,
//               firstRect.Height);

//            using (LinearGradientBrush brush = new LinearGradientBrush(
//               firstRect,
//               backgroundColors[2],
//               backgroundColors[3],
//               LinearGradientMode.Horizontal))
//            {
//                g.FillRectangle(brush, firstRect);
//            }

//            using (LinearGradientBrush brush = new LinearGradientBrush(
//               secondRect,
//               backgroundColors[3],
//               backgroundColors[4],
//               LinearGradientMode.Horizontal))
//            {
//                g.FillRectangle(brush, secondRect);
//            }
//        }

//        /// <summary>
//        /// Draws the background.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        private static void DrawBackgroundHorizontal(Graphics g, Rectangle rect)
//        {
//            using (Pen p = new Pen(backgroundColors[0]))
//            {
//                g.DrawLine(p, rect.Left + 1, rect.Top + 1, rect.Right - 1, rect.Top + 1);
//                g.DrawLine(p, rect.Left + 1, rect.Bottom - 2, rect.Right - 1, rect.Bottom - 2);
//            }

//            using (Pen p = new Pen(backgroundColors[1]))
//            {
//                g.DrawLine(p, rect.Left + 1, rect.Top + 2, rect.Right - 1, rect.Top + 2);
//            }

//            Rectangle firstRect = new Rectangle(
//               rect.Left,
//               rect.Top + 3,
//               rect.Width,
//               8);

//            Rectangle secondRect = new Rectangle(
//               firstRect.Left,
//               firstRect.Bottom - 1,
//               firstRect.Width,
//               7);

//            using (LinearGradientBrush brush = new LinearGradientBrush(
//               firstRect,
//               backgroundColors[2],
//               backgroundColors[3],
//               LinearGradientMode.Vertical))
//            {
//                g.FillRectangle(brush, firstRect);
//            }

//            using (LinearGradientBrush brush = new LinearGradientBrush(
//               secondRect,
//               backgroundColors[3],
//               backgroundColors[4],
//               LinearGradientMode.Vertical))
//            {
//                g.FillRectangle(brush, secondRect);
//            }
//        }

//        /// <summary>
//        /// Draws the channel ( or track ).
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        private static void DrawTrackVertical(Graphics g, Rectangle rect)
//        {
//            Rectangle innerRect = new Rectangle(rect.Left + 1, rect.Top, 15, rect.Height);

//            using (LinearGradientBrush brush = new LinearGradientBrush(
//               innerRect,
//               trackColors[0],
//               trackColors[1],
//               LinearGradientMode.Horizontal))
//            {
//                g.FillRectangle(brush, innerRect);
//            }
//        }

//        /// <summary>
//        /// Draws the channel ( or track ).
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        private static void DrawTrackHorizontal(Graphics g, Rectangle rect)
//        {
//            Rectangle innerRect = new Rectangle(rect.Left, rect.Top + 1, rect.Width, 15);

//            using (LinearGradientBrush brush = new LinearGradientBrush(
//               innerRect,
//               trackColors[0],
//               trackColors[1],
//               LinearGradientMode.Vertical))
//            {
//                g.FillRectangle(brush, innerRect);
//            }
//        }

//        /// <summary>
//        /// Adjusts the thumb grip according to the specified <see cref="ScrollBarOrientation"/>.
//        /// </summary>
//        /// <param name="rect">The rectangle to adjust.</param>
//        /// <param name="orientation">The scrollbar orientation.</param>
//        /// <param name="gripImage">The grip image.</param>
//        /// <returns>The adjusted rectangle.</returns>
//        /// <remarks>Also rotates the grip image if necessary.</remarks>
//        private static Rectangle AdjustThumbGrip(
//           Rectangle rect,
//           ScrollBarOrientation orientation,
//           Image gripImage)
//        {
//            Rectangle r = rect;

//            r.Inflate(-1, -1);

//            if (orientation == ScrollBarOrientation.Vertical)
//            {
//                r.X += 3;
//                r.Y += (r.Height / 2) - (gripImage.Height / 2);
//            }
//            else
//            {
//                gripImage.RotateFlip(RotateFlipType.Rotate90FlipNone);

//                r.X += (r.Width / 2) - (gripImage.Width / 2);
//                r.Y += 3;
//            }

//            r.Width = gripImage.Width;
//            r.Height = gripImage.Height;

//            return r;
//        }

//        /// <summary>
//        /// Draws the thumb.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        /// <param name="state">The <see cref="ScrollBarState"/> of the thumb.</param>
//        private static void DrawThumbVertical(
//           Graphics g,
//           Rectangle rect,
//           ScrollBarState state)
//        {
//            int index = 0;

//            switch (state)
//            {
//                case ScrollBarState.Hot:
//                    {
//                        index = 1;
//                        break;
//                    }

//                case ScrollBarState.Pressed:
//                    {
//                        index = 2;
//                        break;
//                    }
//            }

//            Rectangle innerRect = rect;
//            innerRect.Inflate(-1, -1);

//            Rectangle r = innerRect;
//            r.Width = 6;
//            r.Height++;

//            // draw left gradient
//            using (LinearGradientBrush brush = new LinearGradientBrush(
//               r,
//               thumbColors[index, 1],
//               thumbColors[index, 2],
//               LinearGradientMode.Horizontal))
//            {
//                g.FillRectangle(brush, r);
//            }

//            r.X = r.Right;

//            // draw right gradient
//            if (index == 0)
//            {
//                using (LinearGradientBrush brush = new LinearGradientBrush(
//                   r,
//                   thumbColors[index, 4],
//                   thumbColors[index, 5],
//                   LinearGradientMode.Horizontal))
//                {
//                    brush.InterpolationColors = new ColorBlend(3)
//                    {
//                        Colors = new[]
//                            {
//                               thumbColors[index, 4],
//                               thumbColors[index, 6],
//                               thumbColors[index, 5]
//                            },
//                        Positions = new[] { 0f, .5f, 1f }
//                    };

//                    g.FillRectangle(brush, r);
//                }
//            }
//            else
//            {
//                using (LinearGradientBrush brush = new LinearGradientBrush(
//                   r,
//                   thumbColors[index, 4],
//                   thumbColors[index, 5],
//                   LinearGradientMode.Horizontal))
//                {
//                    g.FillRectangle(brush, r);
//                }

//                // draw left line
//                using (Pen p = new Pen(thumbColors[index, 7]))
//                {
//                    g.DrawLine(p, innerRect.X, innerRect.Y, innerRect.X, innerRect.Bottom);
//                }
//            }

//            // draw right line
//            using (Pen p = new Pen(thumbColors[index, 3]))
//            {
//                g.DrawLine(p, innerRect.Right, innerRect.Y, innerRect.Right, innerRect.Bottom);
//            }

//            g.SmoothingMode = SmoothingMode.AntiAlias;

//            // draw border
//            using (Pen p = new Pen(thumbColors[index, 0]))
//            {
//                using (GraphicsPath path = CreateRoundPath(rect, 2f, 2f))
//                {
//                    g.DrawPath(p, path);
//                }
//            }

//            g.SmoothingMode = SmoothingMode.None;
//        }

//        /// <summary>
//        /// Draws the thumb.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        /// <param name="state">The <see cref="ScrollBarState"/> of the thumb.</param>
//        private static void DrawThumbHorizontal(
//           Graphics g,
//           Rectangle rect,
//           ScrollBarState state)
//        {
//            int index = 0;

//            switch (state)
//            {
//                case ScrollBarState.Hot:
//                    {
//                        index = 1;
//                        break;
//                    }

//                case ScrollBarState.Pressed:
//                    {
//                        index = 2;
//                        break;
//                    }
//            }

//            Rectangle innerRect = rect;
//            innerRect.Inflate(-1, -1);

//            Rectangle r = innerRect;
//            r.Height = 6;
//            r.Width++;

//            // draw left gradient
//            using (LinearGradientBrush brush = new LinearGradientBrush(
//               r, thumbColors[index, 1],
//               thumbColors[index, 2],
//               LinearGradientMode.Vertical))
//            {
//                g.FillRectangle(brush, r);
//            }

//            r.Y = r.Bottom;

//            // draw right gradient
//            if (index == 0)
//            {
//                using (LinearGradientBrush brush = new LinearGradientBrush(
//                   r,
//                   thumbColors[index, 4],
//                   thumbColors[index, 5],
//                   LinearGradientMode.Vertical))
//                {
//                    brush.InterpolationColors = new ColorBlend(3)
//                    {
//                        Colors = new[]
//                            {
//                               thumbColors[index, 4],
//                               thumbColors[index, 6],
//                               thumbColors[index, 5]
//                            },
//                        Positions = new[] { 0f, .5f, 1f }
//                    };

//                    g.FillRectangle(brush, r);
//                }
//            }
//            else
//            {
//                using (LinearGradientBrush brush = new LinearGradientBrush(
//                   r, thumbColors[index, 4],
//                   thumbColors[index, 5],
//                   LinearGradientMode.Vertical))
//                {
//                    g.FillRectangle(brush, r);
//                }

//                // draw left line
//                using (Pen p = new Pen(thumbColors[index, 7]))
//                {
//                    g.DrawLine(p, innerRect.X, innerRect.Y, innerRect.Right, innerRect.Y);
//                }
//            }

//            // draw right line
//            using (Pen p = new Pen(thumbColors[index, 3]))
//            {
//                g.DrawLine(p, innerRect.X, innerRect.Bottom, innerRect.Right, innerRect.Bottom);
//            }

//            g.SmoothingMode = SmoothingMode.AntiAlias;

//            // draw border
//            using (Pen p = new Pen(thumbColors[index, 0]))
//            {
//                using (GraphicsPath path = CreateRoundPath(rect, 2f, 2f))
//                {
//                    g.DrawPath(p, path);
//                }
//            }

//            g.SmoothingMode = SmoothingMode.None;
//        }

//        /// <summary>
//        /// Draws an arrow button.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        /// <param name="state">The <see cref="ScrollBarArrowButtonState"/> of the arrow button.</param>
//        /// <param name="arrowUp">true for an up arrow, false otherwise.</param>
//        private static void DrawArrowButtonVertical(
//           Graphics g,
//           Rectangle rect,
//           ScrollBarArrowButtonState state,
//           bool arrowUp)
//        {
//            using (Image arrowImage = GetArrowDownButtonImage(state))
//            {
//                if (arrowUp)
//                {
//                    arrowImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
//                }

//                g.DrawImage(arrowImage, rect);
//            }
//        }

//        /// <summary>
//        /// Draws an arrow button.
//        /// </summary>
//        /// <param name="g">The <see cref="Graphics"/> used to paint.</param>
//        /// <param name="rect">The rectangle in which to paint.</param>
//        /// <param name="state">The <see cref="ScrollBarArrowButtonState"/> of the arrow button.</param>
//        /// <param name="arrowUp">true for an up arrow, false otherwise.</param>
//        private static void DrawArrowButtonHorizontal(
//           Graphics g,
//           Rectangle rect,
//           ScrollBarArrowButtonState state,
//           bool arrowUp)
//        {
//            using (Image arrowImage = GetArrowDownButtonImage(state))
//            {
//                if (arrowUp)
//                {
//                    arrowImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
//                }
//                else
//                {
//                    arrowImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
//                }

//                g.DrawImage(arrowImage, rect);
//            }
//        }

//        /// <summary>
//        /// Draws the arrow down button for the scrollbar.
//        /// </summary>
//        /// <param name="state">The button state.</param>
//        /// <returns>The arrow down button as <see cref="Image"/>.</returns>
//        private static Image GetArrowDownButtonImage(
//           ScrollBarArrowButtonState state)
//        {
//            Rectangle rect = new Rectangle(0, 0, 15, 17);
//            Bitmap bitmap = new Bitmap(15, 17, PixelFormat.Format32bppArgb);
//            bitmap.SetResolution(72f, 72f);

//            using (Graphics g = Graphics.FromImage(bitmap))
//            {
//                g.SmoothingMode = SmoothingMode.None;
//                g.InterpolationMode = InterpolationMode.Low;

//                int index = -1;

//                switch (state)
//                {
//                    case ScrollBarArrowButtonState.UpHot:
//                    case ScrollBarArrowButtonState.DownHot:
//                        {
//                            index = 1;

//                            break;
//                        }

//                    case ScrollBarArrowButtonState.UpActive:
//                    case ScrollBarArrowButtonState.DownActive:
//                        {
//                            index = 0;

//                            break;
//                        }

//                    case ScrollBarArrowButtonState.UpPressed:
//                    case ScrollBarArrowButtonState.DownPressed:
//                        {
//                            index = 2;

//                            break;
//                        }
//                }

//                if (index != -1)
//                {
//                    using (Pen p1 = new Pen(arrowBorderColors[0]),
//                       p2 = new Pen(arrowBorderColors[1]))
//                    {
//                        g.DrawLine(p1, rect.X, rect.Y, rect.Right - 1, rect.Y);
//                        g.DrawLine(p2, rect.X, rect.Bottom - 1, rect.Right - 1, rect.Bottom - 1);
//                    }

//                    rect.Inflate(0, -1);

//                    using (LinearGradientBrush brush = new LinearGradientBrush(
//                       rect,
//                       arrowBorderColors[2],
//                       arrowBorderColors[1],
//                       LinearGradientMode.Vertical))
//                    {
//                        ColorBlend blend = new ColorBlend(3)
//                        {
//                            Positions = new[] { 0f, .5f, 1f },
//                            Colors = new[] {
//                     arrowBorderColors[2],
//                     arrowBorderColors[3],
//                     arrowBorderColors[0] }
//                        };

//                        brush.InterpolationColors = blend;

//                        using (Pen p = new Pen(brush))
//                        {
//                            g.DrawLine(p, rect.X, rect.Y, rect.X, rect.Bottom - 1);
//                            g.DrawLine(p, rect.Right - 1, rect.Y, rect.Right - 1, rect.Bottom - 1);
//                        }
//                    }

//                    rect.Inflate(0, 1);

//                    Rectangle upper = rect;
//                    upper.Inflate(-1, 0);
//                    upper.Y++;
//                    upper.Height = 7;

//                    using (LinearGradientBrush brush = new LinearGradientBrush(
//                       upper,
//                       arrowColors[index, 2],
//                       arrowColors[index, 3],
//                       LinearGradientMode.Vertical))
//                    {
//                        g.FillRectangle(brush, upper);
//                    }

//                    upper.Inflate(-1, 0);
//                    upper.Height = 6;

//                    using (LinearGradientBrush brush = new LinearGradientBrush(
//                       upper,
//                       arrowColors[index, 0],
//                       arrowColors[index, 1],
//                       LinearGradientMode.Vertical))
//                    {
//                        g.FillRectangle(brush, upper);
//                    }

//                    Rectangle lower = rect;
//                    lower.Inflate(-1, 0);
//                    lower.Y = 8;
//                    lower.Height = 8;

//                    using (LinearGradientBrush brush = new LinearGradientBrush(
//                       lower,
//                       arrowColors[index, 6],
//                       arrowColors[index, 7],
//                       LinearGradientMode.Vertical))
//                    {
//                        g.FillRectangle(brush, lower);
//                    }

//                    lower.Inflate(-1, 0);

//                    using (LinearGradientBrush brush = new LinearGradientBrush(
//                       lower,
//                       arrowColors[index, 4],
//                       arrowColors[index, 5],
//                       LinearGradientMode.Vertical))
//                    {
//                        g.FillRectangle(brush, lower);
//                    }
//                }

//                using (Image arrowIcon = ProGammaX.Properties.Resources.ScrollBarArrowDown)
//                {
//                    if (state == ScrollBarArrowButtonState.DownDisabled
//                       || state == ScrollBarArrowButtonState.UpDisabled)
//                    {
//                        System.Windows.Forms.ControlPaint.DrawImageDisabled(
//                           g,
//                           arrowIcon,
//                           3,
//                           6,
//                           Color.Transparent);
//                    }
//                    else
//                    {
//                        g.DrawImage(arrowIcon, 3, 6);
//                    }
//                }
//            }

//            return bitmap;
//        }

//        /// <summary>
//        /// Creates a rounded rectangle.
//        /// </summary>
//        /// <param name="r">The rectangle to create the rounded rectangle from.</param>
//        /// <param name="radiusX">The x-radius.</param>
//        /// <param name="radiusY">The y-radius.</param>
//        /// <returns>A <see cref="GraphicsPath"/> object representing the rounded rectangle.</returns>
//        private static GraphicsPath CreateRoundPath(
//           Rectangle r,
//           float radiusX,
//           float radiusY)
//        {
//            // create new graphics path object
//            GraphicsPath path = new GraphicsPath();

//            // calculate radius of edges
//            PointF d = new PointF(Math.Min(radiusX * 2, r.Width), Math.Min(radiusY * 2, r.Height));

//            // make sure radius is valid
//            d.X = Math.Max(1, d.X);
//            d.Y = Math.Max(1, d.Y);

//            // add top left arc
//            path.AddArc(r.X, r.Y, d.X, d.Y, 180, 90);

//            // add top right arc
//            path.AddArc(r.Right - d.X, r.Y, d.X, d.Y, 270, 90);

//            // add bottom right arc
//            path.AddArc(r.Right - d.X, r.Bottom - d.Y, d.X, d.Y, 0, 90);

//            // add bottom left arc
//            path.AddArc(r.X, r.Bottom - d.Y, d.X, d.Y, 90, 90);

//            // close path
//            path.CloseFigure();

//            return path;
//        }

//        #endregion

//        #endregion
//    }



//    /// <summary>
//    /// The designer for the <see cref="ScrollBarEx"/> control.
//    /// </summary>
//    internal class ScrollBarControlDesigner : ControlDesigner
//    {
//        /// <summary>
//        /// Gets the <see cref="SelectionRules"/> for the control.
//        /// </summary>
//        public override SelectionRules SelectionRules
//        {
//            get
//            {
//                // gets the property descriptor for the property "Orientation"
//                PropertyDescriptor propDescriptor =
//                   TypeDescriptor.GetProperties(this.Component)["Orientation"];

//                // if not null - we can read the current orientation of the scroll bar
//                if (propDescriptor != null)
//                {
//                    // get the current orientation
//                    ScrollBarOrientation orientation =
//                       (ScrollBarOrientation)propDescriptor.GetValue(this.Component);

//                    // if vertical orientation
//                    if (orientation == ScrollBarOrientation.Vertical)
//                    {
//                        return SelectionRules.Visible
//                           | SelectionRules.Moveable
//                           | SelectionRules.BottomSizeable
//                           | SelectionRules.TopSizeable;
//                    }

//                    return SelectionRules.Visible | SelectionRules.Moveable
//                       | SelectionRules.LeftSizeable | SelectionRules.RightSizeable;
//                }

//                return base.SelectionRules;
//            }
//        }

//        /// <summary>
//        /// Prefilters the properties so that unnecessary properties are hidden
//        /// in the property browser of Visual Studio.
//        /// </summary>
//        /// <param name="properties">The property dictionary.</param>
//        protected override void PreFilterProperties(
//           System.Collections.IDictionary properties)
//        {
//            properties.Remove("Text");
//            properties.Remove("BackgroundImage");
//            properties.Remove("ForeColor");
//            properties.Remove("ImeMode");
//            properties.Remove("Padding");
//            properties.Remove("BackgroundImageLayout");
//            properties.Remove("BackColor");
//            properties.Remove("Font");
//            properties.Remove("RightToLeft");

//            base.PreFilterProperties(properties);
//        }
//    }









//    /// <summary>
//    /// The scrollbar arrow button states.
//    /// </summary>
//    internal enum ScrollBarArrowButtonState
//    {
//        /// <summary>
//        /// Indicates the up arrow is in normal state.
//        /// </summary>
//        UpNormal,

//        /// <summary>
//        /// Indicates the up arrow is in hot state.
//        /// </summary>
//        UpHot,

//        /// <summary>
//        /// Indicates the up arrow is in active state.
//        /// </summary>
//        UpActive,

//        /// <summary>
//        /// Indicates the up arrow is in pressed state.
//        /// </summary>
//        UpPressed,

//        /// <summary>
//        /// Indicates the up arrow is in disabled state.
//        /// </summary>
//        UpDisabled,

//        /// <summary>
//        /// Indicates the down arrow is in normal state.
//        /// </summary>
//        DownNormal,

//        /// <summary>
//        /// Indicates the down arrow is in hot state.
//        /// </summary>
//        DownHot,

//        /// <summary>
//        /// Indicates the down arrow is in active state.
//        /// </summary>
//        DownActive,

//        /// <summary>
//        /// Indicates the down arrow is in pressed state.
//        /// </summary>
//        DownPressed,

//        /// <summary>
//        /// Indicates the down arrow is in disabled state.
//        /// </summary>
//        DownDisabled
//    }
//    /// <summary>
//    /// A custom scrollbar control.
//    /// </summary>
//    [Designer(typeof(ScrollBarControlDesigner))]
//    [DefaultEvent("Scroll")]
//    [DefaultProperty("Value")]
//    public class ScrollBarEx : Control
//    {
//        #region fields

//        /// <summary>
//        /// Redraw const.
//        /// </summary>
//        private const int SETREDRAW = 11;

//        /// <summary>
//        /// Indicates many changes to the scrollbar are happening, so stop painting till finished.
//        /// </summary>
//        private bool inUpdate;

//        /// <summary>
//        /// The scrollbar orientation - horizontal / vertical.
//        /// </summary>
//        private ScrollBarOrientation orientation = ScrollBarOrientation.Vertical;

//        /// <summary>
//        /// The scroll orientation in scroll events.
//        /// </summary>
//        private ScrollOrientation scrollOrientation = ScrollOrientation.VerticalScroll;

//        /// <summary>
//        /// The clicked channel rectangle.
//        /// </summary>
//        private Rectangle clickedBarRectangle;

//        /// <summary>
//        /// The thumb rectangle.
//        /// </summary>
//        private Rectangle thumbRectangle;

//        /// <summary>
//        /// The top arrow rectangle.
//        /// </summary>
//        private Rectangle topArrowRectangle;

//        /// <summary>
//        /// The bottom arrow rectangle.
//        /// </summary>
//        private Rectangle bottomArrowRectangle;

//        /// <summary>
//        /// The channel rectangle.
//        /// </summary>
//        private Rectangle channelRectangle;

//        /// <summary>
//        /// Indicates if top arrow was clicked.
//        /// </summary>
//        private bool topArrowClicked;

//        /// <summary>
//        /// Indicates if bottom arrow was clicked.
//        /// </summary>
//        private bool bottomArrowClicked;

//        /// <summary>
//        /// Indicates if channel rectangle above the thumb was clicked.
//        /// </summary>
//        private bool topBarClicked;

//        /// <summary>
//        /// Indicates if channel rectangle under the thumb was clicked.
//        /// </summary>
//        private bool bottomBarClicked;

//        /// <summary>
//        /// Indicates if the thumb was clicked.
//        /// </summary>
//        private bool thumbClicked;

//        /// <summary>
//        /// The state of the thumb.
//        /// </summary>
//        private ScrollBarState thumbState = ScrollBarState.Normal;

//        /// <summary>
//        /// The state of the top arrow.
//        /// </summary>
//        private ScrollBarArrowButtonState topButtonState = ScrollBarArrowButtonState.UpNormal;

//        /// <summary>
//        /// The state of the bottom arrow.
//        /// </summary>
//        private ScrollBarArrowButtonState bottomButtonState = ScrollBarArrowButtonState.DownNormal;

//        /// <summary>
//        /// The scrollbar value minimum.
//        /// </summary>
//        private int minimum;

//        /// <summary>
//        /// The scrollbar value maximum.
//        /// </summary>
//        private int maximum = 100;

//        /// <summary>
//        /// The small change value.
//        /// </summary>
//        private int smallChange = 1;

//        /// <summary>
//        /// The large change value.
//        /// </summary>
//        private int largeChange = 10;

//        /// <summary>
//        /// The value of the scrollbar.
//        /// </summary>
//        private int value;

//        /// <summary>
//        /// The width of the thumb.
//        /// </summary>
//        private int thumbWidth = 15;

//        /// <summary>
//        /// The height of the thumb.
//        /// </summary>
//        private int thumbHeight;

//        /// <summary>
//        /// The width of an arrow.
//        /// </summary>
//        private int arrowWidth = 15;

//        /// <summary>
//        /// The height of an arrow.
//        /// </summary>
//        private int arrowHeight = 17;

//        /// <summary>
//        /// The bottom limit for the thumb bottom.
//        /// </summary>
//        private int thumbBottomLimitBottom;

//        /// <summary>
//        /// The bottom limit for the thumb top.
//        /// </summary>
//        private int thumbBottomLimitTop;

//        /// <summary>
//        /// The top limit for the thumb top.
//        /// </summary>
//        private int thumbTopLimit;

//        /// <summary>
//        /// The current position of the thumb.
//        /// </summary>
//        private int thumbPosition;

//        /// <summary>
//        /// The track position.
//        /// </summary>
//        private int trackPosition;

//        /// <summary>
//        /// The progress timer for moving the thumb.
//        /// </summary>
//        private Timer progressTimer = new Timer();

//        /// <summary>
//        /// The border color.
//        /// </summary>
//        private Color borderColor = Color.FromArgb(93, 140, 201);

//        /// <summary>
//        /// The border color in disabled state.
//        /// </summary>
//        private Color disabledBorderColor = Color.Gray;

//        #region context menu items

//        /// <summary>
//        /// Context menu strip.
//        /// </summary>
//        private ContextMenuStrip contextMenu;

//        /// <summary>
//        /// Container for components.
//        /// </summary>
//        private IContainer components;

//        /// <summary>
//        /// Menu item.
//        /// </summary>
//        private ToolStripMenuItem tsmiScrollHere;

//        /// <summary>
//        /// Menu separator.
//        /// </summary>
//        private ToolStripSeparator toolStripSeparator1;

//        /// <summary>
//        /// Menu item.
//        /// </summary>
//        private ToolStripMenuItem tsmiTop;

//        /// <summary>
//        /// Menu item.
//        /// </summary>
//        private ToolStripMenuItem tsmiBottom;

//        /// <summary>
//        /// Menu separator.
//        /// </summary>
//        private ToolStripSeparator toolStripSeparator2;

//        /// <summary>
//        /// Menu item.
//        /// </summary>
//        private ToolStripMenuItem tsmiLargeUp;

//        /// <summary>
//        /// Menu item.
//        /// </summary>
//        private ToolStripMenuItem tsmiLargeDown;

//        /// <summary>
//        /// Menu separator.
//        /// </summary>
//        private ToolStripSeparator toolStripSeparator3;

//        /// <summary>
//        /// Menu item.
//        /// </summary>
//        private ToolStripMenuItem tsmiSmallUp;

//        /// <summary>
//        /// Menu item.
//        /// </summary>
//        private ToolStripMenuItem tsmiSmallDown;

//        #endregion

//        #endregion

//        #region constructor

//        /// <summary>
//        /// Initializes a new instance of the <see cref="ScrollBarEx"/> class.
//        /// </summary>
//        public ScrollBarEx()
//        {
//            // sets the control styles of the control
//            SetStyle(
//                  ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw
//                  | ControlStyles.Selectable | ControlStyles.AllPaintingInWmPaint
//                  | ControlStyles.UserPaint, true);

//            // initializes the context menu
//            this.InitializeComponent();

//            this.Width = 19;
//            this.Height = 200;

//            // sets the scrollbar up
//            this.SetUpScrollBar();

//            // timer for clicking and holding the mouse button
//            // over/below the thumb and on the arrow buttons
//            this.progressTimer.Interval = 20;
//            this.progressTimer.Tick += this.ProgressTimerTick;

//            // no image margin in context menu
//            this.contextMenu.ShowImageMargin = false;
//            this.ContextMenuStrip = this.contextMenu;
//        }

//        #endregion

//        #region events
//        /// <summary>
//        /// Occurs when the scrollbar scrolled.
//        /// </summary>
//        [Category("Behavior")]
//        [Description("Is raised, when the scrollbar was scrolled.")]
//        public event ScrollEventHandler Scroll;
//        #endregion

//        #region properties

//        /// <summary>
//        /// Gets or sets the orientation.
//        /// </summary>
//        [Category("Layout")]
//        [Description("Gets or sets the orientation.")]
//        [DefaultValue(ScrollBarOrientation.Vertical)]
//        public ScrollBarOrientation Orientation
//        {
//            get
//            {
//                return this.orientation;
//            }

//            set
//            {
//                // no change - return
//                if (value == this.orientation)
//                {
//                    return;
//                }

//                this.orientation = value;

//                // change text of context menu entries
//                this.ChangeContextMenuItems();

//                // save scroll orientation for scroll event
//                this.scrollOrientation = value == ScrollBarOrientation.Vertical ?
//                   ScrollOrientation.VerticalScroll : ScrollOrientation.HorizontalScroll;

//                // only in DesignMode switch width and height
//                if (this.DesignMode)
//                {
//                    this.Size = new Size(this.Height, this.Width);
//                }

//                // sets the scrollbar up
//                this.SetUpScrollBar();
//            }
//        }

//        /// <summary>
//        /// Gets or sets the minimum value.
//        /// </summary>
//        [Category("Behavior")]
//        [Description("Gets or sets the minimum value.")]
//        [DefaultValue(0)]
//        public int Minimum
//        {
//            get
//            {
//                return this.minimum;
//            }

//            set
//            {
//                // no change or value invalid - return
//                if (this.minimum == value || value < 0 || value >= this.maximum)
//                {
//                    return;
//                }

//                this.minimum = value;

//                // current value less than new minimum value - adjust
//                if (this.value < value)
//                {
//                    this.value = value;
//                }

//                // is current large change value invalid - adjust
//                if (this.largeChange > this.maximum - this.minimum)
//                {
//                    this.largeChange = this.maximum - this.minimum;
//                }

//                this.SetUpScrollBar();

//                // current value less than new minimum value - adjust
//                if (this.value < value)
//                {
//                    this.Value = value;
//                }
//                else
//                {
//                    // current value is valid - adjust thumb position
//                    this.ChangeThumbPosition(this.GetThumbPosition());

//                    this.Refresh();
//                }
//            }
//        }

//        /// <summary>
//        /// Gets or sets the maximum value.
//        /// </summary>
//        [Category("Behavior")]
//        [Description("Gets or sets the maximum value.")]
//        [DefaultValue(100)]
//        public int Maximum
//        {
//            get
//            {
//                return this.maximum;
//            }

//            set
//            {
//                // no change or new max. value invalid - return
//                if (value == this.maximum || value < 1 || value <= this.minimum)
//                {
//                    return;
//                }

//                this.maximum = value;

//                // is large change value invalid - adjust
//                if (this.largeChange > this.maximum - this.minimum)
//                {
//                    this.largeChange = this.maximum - this.minimum;
//                }

//                this.SetUpScrollBar();

//                // is current value greater than new maximum value - adjust
//                if (this.value > value)
//                {
//                    this.Value = this.maximum;
//                }
//                else
//                {
//                    // current value is valid - adjust thumb position
//                    this.ChangeThumbPosition(this.GetThumbPosition());

//                    this.Refresh();
//                }
//            }
//        }

//        /// <summary>
//        /// Gets or sets the small change amount.
//        /// </summary>
//        [Category("Behavior")]
//        [Description("Gets or sets the small change value.")]
//        [DefaultValue(1)]
//        public int SmallChange
//        {
//            get
//            {
//                return this.smallChange;
//            }

//            set
//            {
//                // no change or new small change value invalid - return
//                if (value == this.smallChange || value < 1 || value >= this.largeChange)
//                {
//                    return;
//                }

//                this.smallChange = value;

//                this.SetUpScrollBar();
//            }
//        }

//        /// <summary>
//        /// Gets or sets the large change amount.
//        /// </summary>
//        [Category("Behavior")]
//        [Description("Gets or sets the large change value.")]
//        [DefaultValue(10)]
//        public int LargeChange
//        {
//            get
//            {
//                return this.largeChange;
//            }

//            set
//            {
//                // no change or new large change value is invalid - return
//                if (value == this.largeChange || value < this.smallChange || value < 2)
//                {
//                    return;
//                }

//                // if value is greater than scroll area - adjust
//                if (value > this.maximum - this.minimum)
//                {
//                    this.largeChange = this.maximum - this.minimum;
//                }
//                else
//                {
//                    // set new value
//                    this.largeChange = value;
//                }

//                this.SetUpScrollBar();
//            }
//        }

//        /// <summary>
//        /// Gets or sets the value.
//        /// </summary>
//        [Category("Behavior")]
//        [Description("Gets or sets the current value.")]
//        [DefaultValue(0)]
//        public int Value
//        {
//            get
//            {
//                return this.value;
//            }

//            set
//            {
//                // no change or invalid value - return
//                if (this.value == value || value < this.minimum || value > this.maximum)
//                {
//                    return;
//                }

//                this.value = value;

//                // adjust thumb position
//                this.ChangeThumbPosition(this.GetThumbPosition());

//                // raise scroll event
//                this.OnScroll(new ScrollEventArgs(ScrollEventType.ThumbPosition, -1, this.value, this.scrollOrientation));

//                this.Refresh();
//            }
//        }

//        /// <summary>
//        /// Gets or sets the border color.
//        /// </summary>
//        [Category("Appearance")]
//        [Description("Gets or sets the border color.")]
//        [DefaultValue(typeof(Color), "93, 140, 201")]
//        public Color BorderColor
//        {
//            get
//            {
//                return this.borderColor;
//            }

//            set
//            {
//                this.borderColor = value;

//                this.Invalidate();
//            }
//        }

//        /// <summary>
//        /// Gets or sets the border color in disabled state.
//        /// </summary>
//        [Category("Appearance")]
//        [Description("Gets or sets the border color in disabled state.")]
//        [DefaultValue(typeof(Color), "Gray")]
//        public Color DisabledBorderColor
//        {
//            get
//            {
//                return this.disabledBorderColor;
//            }

//            set
//            {
//                this.disabledBorderColor = value;

//                this.Invalidate();
//            }
//        }

//        /// <summary>
//        /// Gets or sets the opacity of the context menu (from 0 - 1).
//        /// </summary>
//        [Category("Appearance")]
//        [Description("Gets or sets the opacity of the context menu (from 0 - 1).")]
//        [DefaultValue(typeof(double), "1")]
//        public double Opacity
//        {
//            get
//            {
//                return this.contextMenu.Opacity;
//            }

//            set
//            {
//                // no change - return
//                if (value == this.contextMenu.Opacity)
//                {
//                    return;
//                }

//                this.contextMenu.AllowTransparency = value != 1;

//                this.contextMenu.Opacity = value;
//            }
//        }

//        #endregion

//        #region methods

//        #region public methods

//        /// <summary>
//        /// Prevents the drawing of the control until <see cref="EndUpdate"/> is called.
//        /// </summary>
//        public void BeginUpdate()
//        {
//            SendMessage(this.Handle, SETREDRAW, false, 0);
//            this.inUpdate = true;
//        }

//        /// <summary>
//        /// Ends the updating process and the control can draw itself again.
//        /// </summary>
//        public void EndUpdate()
//        {
//            SendMessage(this.Handle, SETREDRAW, true, 0);
//            this.inUpdate = false;
//            this.SetUpScrollBar();
//            this.Refresh();
//        }

//        #endregion

//        #region protected methods

//        /// <summary>
//        /// Raises the <see cref="Scroll"/> event.
//        /// </summary>
//        /// <param name="e">The <see cref="ScrollEventArgs"/> that contains the event data.</param>
//        protected virtual void OnScroll(ScrollEventArgs e)
//        {
//            // if event handler is attached - raise scroll event
//            if (this.Scroll != null)
//            {
//                this.Scroll(this, e);
//            }
//        }

//        /// <summary>
//        /// Paints the background of the control.
//        /// </summary>
//        /// <param name="e">A <see cref="PaintEventArgs"/> that contains information about the control to paint.</param>
//        protected override void OnPaintBackground(PaintEventArgs e)
//        {
//            // no painting here
//        }

//        /// <summary>
//        /// Paints the control.
//        /// </summary>
//        /// <param name="e">A <see cref="PaintEventArgs"/> that contains information about the control to paint.</param>
//        protected override void OnPaint(PaintEventArgs e)
//        {
//            // sets the smoothing mode to none
//            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;

//            // save client rectangle
//            Rectangle rect = ClientRectangle;

//            // adjust the rectangle
//            if (this.orientation == ScrollBarOrientation.Vertical)
//            {
//                rect.X++;
//                rect.Y += this.arrowHeight + 1;
//                rect.Width -= 2;
//                rect.Height -= (this.arrowHeight * 2) + 2;
//            }
//            else
//            {
//                rect.X += this.arrowWidth + 1;
//                rect.Y++;
//                rect.Width -= (this.arrowWidth * 2) + 2;
//                rect.Height -= 2;
//            }

//            // draws the background
//            ScrollBarExRenderer.DrawBackground(
//               e.Graphics,
//               ClientRectangle,
//               this.orientation);

//            // draws the track
//            ScrollBarExRenderer.DrawTrack(
//               e.Graphics,
//               rect,
//               ScrollBarState.Normal,
//               this.orientation);

//            // draw thumb and grip
//            ScrollBarExRenderer.DrawThumb(
//               e.Graphics,
//               this.thumbRectangle,
//               this.thumbState,
//               this.orientation);

//            if (this.Enabled)
//            {
//                ScrollBarExRenderer.DrawThumbGrip(
//                   e.Graphics,
//                   this.thumbRectangle,
//                   this.orientation);
//            }

//            // draw arrows
//            ScrollBarExRenderer.DrawArrowButton(
//               e.Graphics,
//               this.topArrowRectangle,
//               this.topButtonState,
//               true,
//               this.orientation);

//            ScrollBarExRenderer.DrawArrowButton(
//               e.Graphics,
//               this.bottomArrowRectangle,
//               this.bottomButtonState,
//               false,
//               this.orientation);

//            // check if top or bottom bar was clicked
//            if (this.topBarClicked)
//            {
//                if (this.orientation == ScrollBarOrientation.Vertical)
//                {
//                    this.clickedBarRectangle.Y = this.thumbTopLimit;
//                    this.clickedBarRectangle.Height =
//                       this.thumbRectangle.Y - this.thumbTopLimit;
//                }
//                else
//                {
//                    this.clickedBarRectangle.X = this.thumbTopLimit;
//                    this.clickedBarRectangle.Width =
//                       this.thumbRectangle.X - this.thumbTopLimit;
//                }

//                ScrollBarExRenderer.DrawTrack(
//                   e.Graphics,
//                   this.clickedBarRectangle,
//                   ScrollBarState.Pressed,
//                   this.orientation);
//            }
//            else if (this.bottomBarClicked)
//            {
//                if (this.orientation == ScrollBarOrientation.Vertical)
//                {
//                    this.clickedBarRectangle.Y = this.thumbRectangle.Bottom + 1;
//                    this.clickedBarRectangle.Height =
//                       this.thumbBottomLimitBottom - this.clickedBarRectangle.Y + 1;
//                }
//                else
//                {
//                    this.clickedBarRectangle.X = this.thumbRectangle.Right + 1;
//                    this.clickedBarRectangle.Width =
//                       this.thumbBottomLimitBottom - this.clickedBarRectangle.X + 1;
//                }

//                ScrollBarExRenderer.DrawTrack(
//                   e.Graphics,
//                   this.clickedBarRectangle,
//                   ScrollBarState.Pressed,
//                   this.orientation);
//            }

//            // draw border
//            using (Pen pen = new Pen(
//               (this.Enabled ? this.borderColor : this.disabledBorderColor)))
//            {
//                e.Graphics.DrawRectangle(pen, 0, 0, this.Width - 1, this.Height - 1);
//            }
//        }

//        /// <summary>
//        /// Raises the MouseDown event.
//        /// </summary>
//        /// <param name="e">A <see cref="MouseEventArgs"/> that contains the event data.</param>
//        protected override void OnMouseDown(MouseEventArgs e)
//        {
//            base.OnMouseDown(e);

//            this.Focus();

//            if (e.Button == MouseButtons.Left)
//            {
//                // prevents showing the context menu if pressing the right mouse
//                // button while holding the left
//                this.ContextMenuStrip = null;

//                Point mouseLocation = e.Location;

//                if (this.thumbRectangle.Contains(mouseLocation))
//                {
//                    this.thumbClicked = true;
//                    this.thumbPosition = this.orientation == ScrollBarOrientation.Vertical ? mouseLocation.Y - this.thumbRectangle.Y : mouseLocation.X - this.thumbRectangle.X;
//                    this.thumbState = ScrollBarState.Pressed;

//                    Invalidate(this.thumbRectangle);
//                }
//                else if (this.topArrowRectangle.Contains(mouseLocation))
//                {
//                    this.topArrowClicked = true;
//                    this.topButtonState = ScrollBarArrowButtonState.UpPressed;

//                    this.Invalidate(this.topArrowRectangle);

//                    this.ProgressThumb(true);
//                }
//                else if (this.bottomArrowRectangle.Contains(mouseLocation))
//                {
//                    this.bottomArrowClicked = true;
//                    this.bottomButtonState = ScrollBarArrowButtonState.DownPressed;

//                    this.Invalidate(this.bottomArrowRectangle);

//                    this.ProgressThumb(true);
//                }
//                else
//                {
//                    this.trackPosition =
//                       this.orientation == ScrollBarOrientation.Vertical ?
//                          mouseLocation.Y : mouseLocation.X;

//                    if (this.trackPosition <
//                       (this.orientation == ScrollBarOrientation.Vertical ?
//                          this.thumbRectangle.Y : this.thumbRectangle.X))
//                    {
//                        this.topBarClicked = true;
//                    }
//                    else
//                    {
//                        this.bottomBarClicked = true;
//                    }

//                    this.ProgressThumb(true);
//                }
//            }
//            else if (e.Button == MouseButtons.Right)
//            {
//                this.trackPosition =
//                   this.orientation == ScrollBarOrientation.Vertical ? e.Y : e.X;
//            }
//        }

//        /// <summary>
//        /// Raises the MouseUp event.
//        /// </summary>
//        /// <param name="e">A <see cref="MouseEventArgs"/> that contains the event data.</param>
//        protected override void OnMouseUp(MouseEventArgs e)
//        {
//            base.OnMouseUp(e);

//            if (e.Button == MouseButtons.Left)
//            {
//                this.ContextMenuStrip = this.contextMenu;

//                if (this.thumbClicked)
//                {
//                    this.thumbClicked = false;
//                    this.thumbState = ScrollBarState.Normal;

//                    this.OnScroll(new ScrollEventArgs(
//                       ScrollEventType.EndScroll,
//                       -1,
//                       this.value,
//                       this.scrollOrientation)
//                    );
//                }
//                else if (this.topArrowClicked)
//                {
//                    this.topArrowClicked = false;
//                    this.topButtonState = ScrollBarArrowButtonState.UpNormal;
//                    this.StopTimer();
//                }
//                else if (this.bottomArrowClicked)
//                {
//                    this.bottomArrowClicked = false;
//                    this.bottomButtonState = ScrollBarArrowButtonState.DownNormal;
//                    this.StopTimer();
//                }
//                else if (this.topBarClicked)
//                {
//                    this.topBarClicked = false;
//                    this.StopTimer();
//                }
//                else if (this.bottomBarClicked)
//                {
//                    this.bottomBarClicked = false;
//                    this.StopTimer();
//                }

//                Invalidate();
//            }
//        }

//        /// <summary>
//        /// Raises the MouseEnter event.
//        /// </summary>
//        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
//        protected override void OnMouseEnter(EventArgs e)
//        {
//            base.OnMouseEnter(e);

//            this.bottomButtonState = ScrollBarArrowButtonState.DownActive;
//            this.topButtonState = ScrollBarArrowButtonState.UpActive;
//            this.thumbState = ScrollBarState.Active;

//            Invalidate();
//        }

//        /// <summary>
//        /// Raises the MouseLeave event.
//        /// </summary>
//        /// <param name="e">A <see cref="EventArgs"/> that contains the event data.</param>
//        protected override void OnMouseLeave(EventArgs e)
//        {
//            base.OnMouseLeave(e);

//            this.ResetScrollStatus();
//        }

//        /// <summary>
//        /// Raises the MouseMove event.
//        /// </summary>
//        /// <param name="e">A <see cref="MouseEventArgs"/> that contains the event data.</param>
//        protected override void OnMouseMove(MouseEventArgs e)
//        {
//            base.OnMouseMove(e);

//            // moving and holding the left mouse button
//            if (e.Button == MouseButtons.Left)
//            {
//                // Update the thumb position, if the new location is within the bounds.
//                if (this.thumbClicked)
//                {
//                    int oldScrollValue = this.value;

//                    this.topButtonState = ScrollBarArrowButtonState.UpActive;
//                    this.bottomButtonState = ScrollBarArrowButtonState.DownActive;
//                    int pos = this.orientation == ScrollBarOrientation.Vertical ?
//                       e.Location.Y : e.Location.X;

//                    // The thumb is all the way to the top
//                    if (pos <= (this.thumbTopLimit + this.thumbPosition))
//                    {
//                        this.ChangeThumbPosition(this.thumbTopLimit);

//                        this.value = this.minimum;
//                    }
//                    else if (pos >= (this.thumbBottomLimitTop + this.thumbPosition))
//                    {
//                        // The thumb is all the way to the bottom
//                        this.ChangeThumbPosition(this.thumbBottomLimitTop);

//                        this.value = this.maximum;
//                    }
//                    else
//                    {
//                        // The thumb is between the ends of the track.
//                        this.ChangeThumbPosition(pos - this.thumbPosition);

//                        int pixelRange, thumbPos, arrowSize;

//                        // calculate the value - first some helper variables
//                        // dependent on the current orientation
//                        if (this.orientation == ScrollBarOrientation.Vertical)
//                        {
//                            pixelRange = this.Height - (2 * this.arrowHeight) - this.thumbHeight;
//                            thumbPos = this.thumbRectangle.Y;
//                            arrowSize = this.arrowHeight;
//                        }
//                        else
//                        {
//                            pixelRange = this.Width - (2 * this.arrowWidth) - this.thumbWidth;
//                            thumbPos = this.thumbRectangle.X;
//                            arrowSize = this.arrowWidth;
//                        }

//                        float perc = 0f;

//                        if (pixelRange != 0)
//                        {
//                            // percent of the new position
//                            perc = (float)(thumbPos - arrowSize) / (float)pixelRange;
//                        }

//                        // the new value is somewhere between max and min, starting
//                        // at min position
//                        this.value = Convert.ToInt32((perc * (this.maximum - this.minimum)) + this.minimum);
//                    }

//                    // raise scroll event if new value different
//                    if (oldScrollValue != this.value)
//                    {
//                        this.OnScroll(new ScrollEventArgs(ScrollEventType.ThumbTrack, oldScrollValue, this.value, this.scrollOrientation));

//                        this.Refresh();
//                    }
//                }
//            }
//            else if (!this.ClientRectangle.Contains(e.Location))
//            {
//                this.ResetScrollStatus();
//            }
//            else if (e.Button == MouseButtons.None) // only moving the mouse
//            {
//                if (this.topArrowRectangle.Contains(e.Location))
//                {
//                    this.topButtonState = ScrollBarArrowButtonState.UpHot;

//                    this.Invalidate(this.topArrowRectangle);
//                }
//                else if (this.bottomArrowRectangle.Contains(e.Location))
//                {
//                    this.bottomButtonState = ScrollBarArrowButtonState.DownHot;

//                    Invalidate(this.bottomArrowRectangle);
//                }
//                else if (this.thumbRectangle.Contains(e.Location))
//                {
//                    this.thumbState = ScrollBarState.Hot;

//                    this.Invalidate(this.thumbRectangle);
//                }
//                else if (this.ClientRectangle.Contains(e.Location))
//                {
//                    this.topButtonState = ScrollBarArrowButtonState.UpActive;
//                    this.bottomButtonState = ScrollBarArrowButtonState.DownActive;
//                    this.thumbState = ScrollBarState.Active;

//                    Invalidate();
//                }
//            }
//        }

//        /// <summary>
//        /// Performs the work of setting the specified bounds of this control.
//        /// </summary>
//        /// <param name="x">The new x value of the control.</param>
//        /// <param name="y">The new y value of the control.</param>
//        /// <param name="width">The new width value of the control.</param>
//        /// <param name="height">The new height value of the control.</param>
//        /// <param name="specified">A bitwise combination of the <see cref="BoundsSpecified"/> values.</param>
//        protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
//        {
//            // only in design mode - constrain size
//            if (this.DesignMode)
//            {
//                if (this.orientation == ScrollBarOrientation.Vertical)
//                {
//                    if (height < (2 * this.arrowHeight) + 10)
//                    {
//                        height = (2 * this.arrowHeight) + 10;
//                    }

//                    width = 19;
//                }
//                else
//                {
//                    if (width < (2 * this.arrowWidth) + 10)
//                    {
//                        width = (2 * this.arrowWidth) + 10;
//                    }

//                    height = 19;
//                }
//            }

//            base.SetBoundsCore(x, y, width, height, specified);

//            if (this.DesignMode)
//            {
//                this.SetUpScrollBar();
//            }
//        }

//        /// <summary>
//        /// Raises the <see cref="System.Windows.Forms.Control.SizeChanged"/> event.
//        /// </summary>
//        /// <param name="e">An <see cref="EventArgs"/> that contains the event data.</param>
//        protected override void OnSizeChanged(EventArgs e)
//        {
//            base.OnSizeChanged(e);
//            this.SetUpScrollBar();
//        }

//        /// <summary>
//        /// Processes a dialog key.
//        /// </summary>
//        /// <param name="keyData">One of the <see cref="System.Windows.Forms.Keys"/> values that represents the key to process.</param>
//        /// <returns>true, if the key was processed by the control, false otherwise.</returns>
//        protected override bool ProcessDialogKey(Keys keyData)
//        {
//            // key handling is here - keys recognized by the control
//            // Up&Down or Left&Right, PageUp, PageDown, Home, End
//            Keys keyUp = Keys.Up;
//            Keys keyDown = Keys.Down;

//            if (this.orientation == ScrollBarOrientation.Horizontal)
//            {
//                keyUp = Keys.Left;
//                keyDown = Keys.Right;
//            }

//            if (keyData == keyUp)
//            {
//                this.Value -= this.smallChange;

//                return true;
//            }

//            if (keyData == keyDown)
//            {
//                this.Value += this.smallChange;

//                return true;
//            }

//            if (keyData == Keys.PageUp)
//            {
//                this.Value = this.GetValue(false, true);

//                return true;
//            }

//            if (keyData == Keys.PageDown)
//            {
//                if (this.value + this.largeChange > this.maximum)
//                {
//                    this.Value = this.maximum;
//                }
//                else
//                {
//                    this.Value += this.largeChange;
//                }

//                return true;
//            }

//            if (keyData == Keys.Home)
//            {
//                this.Value = this.minimum;

//                return true;
//            }

//            if (keyData == Keys.End)
//            {
//                this.Value = this.maximum;

//                return true;
//            }

//            return base.ProcessDialogKey(keyData);
//        }

//        /// <summary>
//        /// Raises the <see cref="System.Windows.Forms.Control.EnabledChanged"/> event.
//        /// </summary>
//        /// <param name="e">An <see cref="EventArgs"/> that contains the event data.</param>
//        protected override void OnEnabledChanged(EventArgs e)
//        {
//            base.OnEnabledChanged(e);

//            if (this.Enabled)
//            {
//                this.thumbState = ScrollBarState.Normal;
//                this.topButtonState = ScrollBarArrowButtonState.UpNormal;
//                this.bottomButtonState = ScrollBarArrowButtonState.DownNormal;
//            }
//            else
//            {
//                this.thumbState = ScrollBarState.Disabled;
//                this.topButtonState = ScrollBarArrowButtonState.UpDisabled;
//                this.bottomButtonState = ScrollBarArrowButtonState.DownDisabled;
//            }

//            this.Refresh();
//        }

//        #endregion

//        #region misc methods

//        /// <summary>
//        /// Sends a message.
//        /// </summary>
//        /// <param name="wnd">The handle of the control.</param>
//        /// <param name="msg">The message as int.</param>
//        /// <param name="param">param - true or false.</param>
//        /// <param name="lparam">Additional parameter.</param>
//        /// <returns>0 or error code.</returns>
//        /// <remarks>Needed for sending the stop/start drawing of the control.</remarks>
//        [DllImport("user32.dll")]
//        private static extern int SendMessage(
//           IntPtr wnd,
//           int msg,
//           bool param,
//           int lparam);

//        /// <summary>
//        /// Sets up the scrollbar.
//        /// </summary>
//        private void SetUpScrollBar()
//        {
//            // if no drawing - return
//            if (this.inUpdate)
//            {
//                return;
//            }

//            // set up the width's, height's and rectangles for the different
//            // elements
//            if (this.orientation == ScrollBarOrientation.Vertical)
//            {
//                this.arrowHeight = 17;
//                this.arrowWidth = 15;
//                this.thumbWidth = 15;
//                this.thumbHeight = this.GetThumbSize();

//                this.clickedBarRectangle = this.ClientRectangle;
//                this.clickedBarRectangle.Inflate(-1, -1);
//                this.clickedBarRectangle.Y += this.arrowHeight;
//                this.clickedBarRectangle.Height -= this.arrowHeight * 2;

//                this.channelRectangle = this.clickedBarRectangle;

//                this.thumbRectangle = new Rectangle(
//                   ClientRectangle.X + 2,
//                   ClientRectangle.Y + this.arrowHeight + 1,
//                   this.thumbWidth - 1,
//                   this.thumbHeight
//                );

//                this.topArrowRectangle = new Rectangle(
//                   ClientRectangle.X + 2,
//                   ClientRectangle.Y + 1,
//                   this.arrowWidth,
//                   this.arrowHeight
//                );

//                this.bottomArrowRectangle = new Rectangle(
//                   ClientRectangle.X + 2,
//                   ClientRectangle.Bottom - this.arrowHeight - 1,
//                   this.arrowWidth,
//                   this.arrowHeight
//                );

//                // Set the default starting thumb position.
//                this.thumbPosition = this.thumbRectangle.Height / 2;

//                // Set the bottom limit of the thumb's bottom border.
//                this.thumbBottomLimitBottom =
//                   ClientRectangle.Bottom - this.arrowHeight - 2;

//                // Set the bottom limit of the thumb's top border.
//                this.thumbBottomLimitTop =
//                   this.thumbBottomLimitBottom - this.thumbRectangle.Height;

//                // Set the top limit of the thumb's top border.
//                this.thumbTopLimit = ClientRectangle.Y + this.arrowHeight + 1;
//            }
//            else
//            {
//                this.arrowHeight = 15;
//                this.arrowWidth = 17;
//                this.thumbHeight = 15;
//                this.thumbWidth = this.GetThumbSize();

//                this.clickedBarRectangle = this.ClientRectangle;
//                this.clickedBarRectangle.Inflate(-1, -1);
//                this.clickedBarRectangle.X += this.arrowWidth;
//                this.clickedBarRectangle.Width -= this.arrowWidth * 2;

//                this.channelRectangle = this.clickedBarRectangle;

//                this.thumbRectangle = new Rectangle(
//                   ClientRectangle.X + this.arrowWidth + 1,
//                   ClientRectangle.Y + 2,
//                   this.thumbWidth,
//                   this.thumbHeight - 1
//                );

//                this.topArrowRectangle = new Rectangle(
//                   ClientRectangle.X + 1,
//                   ClientRectangle.Y + 2,
//                   this.arrowWidth,
//                   this.arrowHeight
//                );

//                this.bottomArrowRectangle = new Rectangle(
//                   ClientRectangle.Right - this.arrowWidth - 1,
//                   ClientRectangle.Y + 2,
//                   this.arrowWidth,
//                   this.arrowHeight
//                );

//                // Set the default starting thumb position.
//                this.thumbPosition = this.thumbRectangle.Width / 2;

//                // Set the bottom limit of the thumb's bottom border.
//                this.thumbBottomLimitBottom =
//                   ClientRectangle.Right - this.arrowWidth - 2;

//                // Set the bottom limit of the thumb's top border.
//                this.thumbBottomLimitTop =
//                   this.thumbBottomLimitBottom - this.thumbRectangle.Width;

//                // Set the top limit of the thumb's top border.
//                this.thumbTopLimit = ClientRectangle.X + this.arrowWidth + 1;
//            }

//            this.ChangeThumbPosition(this.GetThumbPosition());

//            this.Refresh();
//        }

//        /// <summary>
//        /// Handles the updating of the thumb.
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">An object that contains the event data.</param>
//        private void ProgressTimerTick(object sender, EventArgs e)
//        {
//            this.ProgressThumb(true);
//        }

//        /// <summary>
//        /// Resets the scroll status of the scrollbar.
//        /// </summary>
//        private void ResetScrollStatus()
//        {
//            // get current mouse position
//            Point pos = this.PointToClient(Cursor.Position);

//            // set appearance of buttons in relation to where the mouse is -
//            // outside or inside the control
//            if (this.ClientRectangle.Contains(pos))
//            {
//                this.bottomButtonState = ScrollBarArrowButtonState.DownActive;
//                this.topButtonState = ScrollBarArrowButtonState.UpActive;
//            }
//            else
//            {
//                this.bottomButtonState = ScrollBarArrowButtonState.DownNormal;
//                this.topButtonState = ScrollBarArrowButtonState.UpNormal;
//            }

//            // set appearance of thumb
//            this.thumbState = this.thumbRectangle.Contains(pos) ?
//               ScrollBarState.Hot : ScrollBarState.Normal;

//            this.bottomArrowClicked = this.bottomBarClicked =
//               this.topArrowClicked = this.topBarClicked = false;

//            this.StopTimer();

//            this.Refresh();
//        }

//        /// <summary>
//        /// Calculates the new value of the scrollbar.
//        /// </summary>
//        /// <param name="smallIncrement">true for a small change, false otherwise.</param>
//        /// <param name="up">true for up movement, false otherwise.</param>
//        /// <returns>The new scrollbar value.</returns>
//        private int GetValue(bool smallIncrement, bool up)
//        {
//            int newValue;

//            // calculate the new value of the scrollbar
//            // with checking if new value is in bounds (min/max)
//            if (up)
//            {
//                newValue = this.value - (smallIncrement ? this.smallChange : this.largeChange);

//                if (newValue < this.minimum)
//                {
//                    newValue = this.minimum;
//                }
//            }
//            else
//            {
//                newValue = this.value + (smallIncrement ? this.smallChange : this.largeChange);

//                if (newValue > this.maximum)
//                {
//                    newValue = this.maximum;
//                }
//            }

//            return newValue;
//        }

//        /// <summary>
//        /// Calculates the new thumb position.
//        /// </summary>
//        /// <returns>The new thumb position.</returns>
//        private int GetThumbPosition()
//        {
//            int pixelRange, arrowSize;

//            if (this.orientation == ScrollBarOrientation.Vertical)
//            {
//                pixelRange = this.Height - (2 * this.arrowHeight) - this.thumbHeight;
//                arrowSize = this.arrowHeight;
//            }
//            else
//            {
//                pixelRange = this.Width - (2 * this.arrowWidth) - this.thumbWidth;
//                arrowSize = this.arrowWidth;
//            }

//            int realRange = this.maximum - this.minimum;
//            float perc = 0f;

//            if (realRange != 0)
//            {
//                perc = ((float)this.value - (float)this.minimum) / (float)realRange;
//            }

//            return Math.Max(this.thumbTopLimit, Math.Min(
//               this.thumbBottomLimitTop,
//               Convert.ToInt32((perc * pixelRange) + arrowSize)));
//        }

//        /// <summary>
//        /// Calculates the height of the thumb.
//        /// </summary>
//        /// <returns>The height of the thumb.</returns>
//        private int GetThumbSize()
//        {
//            int trackSize =
//               this.orientation == ScrollBarOrientation.Vertical ?
//               this.Height - (2 * this.arrowHeight) : this.Width - (2 * this.arrowWidth);

//            if (this.maximum == 0 || this.largeChange == 0)
//            {
//                return trackSize;
//            }

//            float newThumbSize = ((float)this.largeChange * (float)trackSize) / (float)this.maximum;

//            return Convert.ToInt32(Math.Min((float)trackSize, Math.Max(newThumbSize, 10f)));
//        }

//        /// <summary>
//        /// Enables the timer.
//        /// </summary>
//        private void EnableTimer()
//        {
//            // if timer is not already enabled - enable it
//            if (!this.progressTimer.Enabled)
//            {
//                this.progressTimer.Interval = 600;
//                this.progressTimer.Start();
//            }
//            else
//            {
//                // if already enabled, change tick time
//                this.progressTimer.Interval = 10;
//            }
//        }

//        /// <summary>
//        /// Stops the progress timer.
//        /// </summary>
//        private void StopTimer()
//        {
//            this.progressTimer.Stop();
//        }

//        /// <summary>
//        /// Changes the position of the thumb.
//        /// </summary>
//        /// <param name="position">The new position.</param>
//        private void ChangeThumbPosition(int position)
//        {
//            if (this.orientation == ScrollBarOrientation.Vertical)
//            {
//                this.thumbRectangle.Y = position;
//            }
//            else
//            {
//                this.thumbRectangle.X = position;
//            }
//        }

//        /// <summary>
//        /// Controls the movement of the thumb.
//        /// </summary>
//        /// <param name="enableTimer">true for enabling the timer, false otherwise.</param>
//        private void ProgressThumb(bool enableTimer)
//        {
//            int scrollOldValue = this.value;
//            ScrollEventType type = ScrollEventType.First;
//            int thumbSize, thumbPos;

//            if (this.orientation == ScrollBarOrientation.Vertical)
//            {
//                thumbPos = this.thumbRectangle.Y;
//                thumbSize = this.thumbRectangle.Height;
//            }
//            else
//            {
//                thumbPos = this.thumbRectangle.X;
//                thumbSize = this.thumbRectangle.Width;
//            }

//            // arrow down or shaft down clicked
//            if (this.bottomArrowClicked || (this.bottomBarClicked && (thumbPos + thumbSize) < this.trackPosition))
//            {
//                type = this.bottomArrowClicked ? ScrollEventType.SmallIncrement : ScrollEventType.LargeIncrement;

//                this.value = this.GetValue(this.bottomArrowClicked, false);

//                if (this.value == this.maximum)
//                {
//                    this.ChangeThumbPosition(this.thumbBottomLimitTop);

//                    type = ScrollEventType.Last;
//                }
//                else
//                {
//                    this.ChangeThumbPosition(Math.Min(this.thumbBottomLimitTop, this.GetThumbPosition()));
//                }
//            }
//            else if (this.topArrowClicked || (this.topBarClicked && thumbPos > this.trackPosition))
//            {
//                type = this.topArrowClicked ? ScrollEventType.SmallDecrement : ScrollEventType.LargeDecrement;

//                // arrow up or shaft up clicked
//                this.value = this.GetValue(this.topArrowClicked, true);

//                if (this.value == this.minimum)
//                {
//                    this.ChangeThumbPosition(this.thumbTopLimit);

//                    type = ScrollEventType.First;
//                }
//                else
//                {
//                    this.ChangeThumbPosition(Math.Max(this.thumbTopLimit, this.GetThumbPosition()));
//                }
//            }
//            else if (!((this.topArrowClicked && thumbPos == this.thumbTopLimit) || (this.bottomArrowClicked && thumbPos == this.thumbBottomLimitTop)))
//            {
//                this.ResetScrollStatus();

//                return;
//            }

//            if (scrollOldValue != this.value)
//            {
//                this.OnScroll(new ScrollEventArgs(type, scrollOldValue, this.value, this.scrollOrientation));

//                this.Invalidate(this.channelRectangle);

//                if (enableTimer)
//                {
//                    this.EnableTimer();
//                }
//            }
//            else
//            {
//                if (this.topArrowClicked)
//                {
//                    type = ScrollEventType.SmallDecrement;
//                }
//                else if (this.bottomArrowClicked)
//                {
//                    type = ScrollEventType.SmallIncrement;
//                }

//                this.OnScroll(new ScrollEventArgs(type, this.value));
//            }
//        }

//        /// <summary>
//        /// Changes the displayed text of the context menu items dependent of the current <see cref="ScrollBarOrientation"/>.
//        /// </summary>
//        private void ChangeContextMenuItems()
//        {
//            if (this.orientation == ScrollBarOrientation.Vertical)
//            {
//                this.tsmiTop.Text = "Top";
//                this.tsmiBottom.Text = "Bottom";
//                this.tsmiLargeDown.Text = "Page down";
//                this.tsmiLargeUp.Text = "Page up";
//                this.tsmiSmallDown.Text = "Scroll down";
//                this.tsmiSmallUp.Text = "Scroll up";
//                this.tsmiScrollHere.Text = "Scroll here";
//            }
//            else
//            {
//                this.tsmiTop.Text = "Left";
//                this.tsmiBottom.Text = "Right";
//                this.tsmiLargeDown.Text = "Page left";
//                this.tsmiLargeUp.Text = "Page right";
//                this.tsmiSmallDown.Text = "Scroll right";
//                this.tsmiSmallUp.Text = "Scroll left";
//                this.tsmiScrollHere.Text = "Scroll here";
//            }
//        }

//        #endregion

//        #region context menu methods

//        /// <summary>
//        /// Initializes the context menu.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.components = new System.ComponentModel.Container();
//            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
//            this.tsmiScrollHere = new System.Windows.Forms.ToolStripMenuItem();
//            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
//            this.tsmiTop = new System.Windows.Forms.ToolStripMenuItem();
//            this.tsmiBottom = new System.Windows.Forms.ToolStripMenuItem();
//            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
//            this.tsmiLargeUp = new System.Windows.Forms.ToolStripMenuItem();
//            this.tsmiLargeDown = new System.Windows.Forms.ToolStripMenuItem();
//            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
//            this.tsmiSmallUp = new System.Windows.Forms.ToolStripMenuItem();
//            this.tsmiSmallDown = new System.Windows.Forms.ToolStripMenuItem();
//            this.contextMenu.SuspendLayout();
//            this.SuspendLayout();
//            // 
//            // contextMenu
//            // 
//            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
//            this.tsmiScrollHere,
//            this.toolStripSeparator1,
//            this.tsmiTop,
//            this.tsmiBottom,
//            this.toolStripSeparator2,
//            this.tsmiLargeUp,
//            this.tsmiLargeDown,
//            this.toolStripSeparator3,
//            this.tsmiSmallUp,
//            this.tsmiSmallDown});
//            this.contextMenu.Name = "contextMenu";
//            this.contextMenu.Size = new System.Drawing.Size(151, 176);
//            // 
//            // tsmiScrollHere
//            // 
//            this.tsmiScrollHere.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
//            this.tsmiScrollHere.Name = "tsmiScrollHere";
//            this.tsmiScrollHere.Size = new System.Drawing.Size(150, 22);
//            this.tsmiScrollHere.Text = "Scroll here";
//            this.tsmiScrollHere.Click += ScrollHereClick;
//            // 
//            // toolStripSeparator1
//            // 
//            this.toolStripSeparator1.Name = "toolStripSeparator1";
//            this.toolStripSeparator1.Size = new System.Drawing.Size(147, 6);
//            // 
//            // tsmiTop
//            // 
//            this.tsmiTop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
//            this.tsmiTop.Name = "tsmiTop";
//            this.tsmiTop.Size = new System.Drawing.Size(150, 22);
//            this.tsmiTop.Text = "Top";
//            this.tsmiTop.Click += this.TopClick;
//            // 
//            // tsmiBottom
//            // 
//            this.tsmiBottom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
//            this.tsmiBottom.Name = "tsmiBottom";
//            this.tsmiBottom.Size = new System.Drawing.Size(150, 22);
//            this.tsmiBottom.Text = "Bottom";
//            this.tsmiBottom.Click += this.BottomClick;
//            // 
//            // toolStripSeparator2
//            // 
//            this.toolStripSeparator2.Name = "toolStripSeparator2";
//            this.toolStripSeparator2.Size = new System.Drawing.Size(147, 6);
//            // 
//            // tsmiLargeUp
//            // 
//            this.tsmiLargeUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
//            this.tsmiLargeUp.Name = "tsmiLargeUp";
//            this.tsmiLargeUp.Size = new System.Drawing.Size(150, 22);
//            this.tsmiLargeUp.Text = "Page up";
//            this.tsmiLargeUp.Click += this.LargeUpClick;
//            // 
//            // tsmiLargeDown
//            // 
//            this.tsmiLargeDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
//            this.tsmiLargeDown.Name = "tsmiLargeDown";
//            this.tsmiLargeDown.Size = new System.Drawing.Size(150, 22);
//            this.tsmiLargeDown.Text = "Page down";
//            this.tsmiLargeDown.Click += this.LargeDownClick;
//            // 
//            // toolStripSeparator3
//            // 
//            this.toolStripSeparator3.Name = "toolStripSeparator3";
//            this.toolStripSeparator3.Size = new System.Drawing.Size(147, 6);
//            // 
//            // tsmiSmallUp
//            // 
//            this.tsmiSmallUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
//            this.tsmiSmallUp.Name = "tsmiSmallUp";
//            this.tsmiSmallUp.Size = new System.Drawing.Size(150, 22);
//            this.tsmiSmallUp.Text = "Scroll up";
//            this.tsmiSmallUp.Click += this.SmallUpClick;
//            // 
//            // tsmiSmallDown
//            // 
//            this.tsmiSmallDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
//            this.tsmiSmallDown.Name = "tsmiSmallDown";
//            this.tsmiSmallDown.Size = new System.Drawing.Size(150, 22);
//            this.tsmiSmallDown.Text = "Scroll down";
//            this.tsmiSmallDown.Click += this.SmallDownClick;
//            this.contextMenu.ResumeLayout(false);
//            this.ResumeLayout(false);
//        }

//        /// <summary>
//        /// Context menu handler.
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">The event arguments.</param>
//        private void ScrollHereClick(object sender, EventArgs e)
//        {
//            int thumbSize, thumbPos, arrowSize, size;

//            if (this.orientation == ScrollBarOrientation.Vertical)
//            {
//                thumbSize = this.thumbHeight;
//                arrowSize = this.arrowHeight;
//                size = this.Height;

//                this.ChangeThumbPosition(Math.Max(this.thumbTopLimit, Math.Min(this.thumbBottomLimitTop, this.trackPosition - (this.thumbRectangle.Height / 2))));

//                thumbPos = this.thumbRectangle.Y;
//            }
//            else
//            {
//                thumbSize = this.thumbWidth;
//                arrowSize = this.arrowWidth;
//                size = this.Width;

//                this.ChangeThumbPosition(Math.Max(this.thumbTopLimit, Math.Min(this.thumbBottomLimitTop, this.trackPosition - (this.thumbRectangle.Width / 2))));

//                thumbPos = this.thumbRectangle.X;
//            }

//            int pixelRange = size - (2 * arrowSize) - thumbSize;
//            float perc = 0f;

//            if (pixelRange != 0)
//            {
//                perc = (float)(thumbPos - arrowSize) / (float)pixelRange;
//            }

//            int oldValue = this.value;

//            this.value = Convert.ToInt32((perc * (this.maximum - this.minimum)) + this.minimum);

//            this.OnScroll(new ScrollEventArgs(ScrollEventType.ThumbPosition, oldValue, this.value, this.scrollOrientation));

//            this.Refresh();
//        }

//        /// <summary>
//        /// Context menu handler.
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">The event arguments.</param>
//        private void TopClick(object sender, EventArgs e)
//        {
//            this.Value = this.minimum;
//        }

//        /// <summary>
//        /// Context menu handler.
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">The event arguments.</param>
//        private void BottomClick(object sender, EventArgs e)
//        {
//            this.Value = this.maximum;
//        }

//        /// <summary>
//        /// Context menu handler.
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">The event arguments.</param>
//        private void LargeUpClick(object sender, EventArgs e)
//        {
//            this.Value = this.GetValue(false, true);
//        }

//        /// <summary>
//        /// Context menu handler.
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">The event arguments.</param>
//        private void LargeDownClick(object sender, EventArgs e)
//        {
//            this.Value = this.GetValue(false, false);
//        }

//        /// <summary>
//        /// Context menu handler.
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">The event arguments.</param>
//        private void SmallUpClick(object sender, EventArgs e)
//        {
//            this.Value = this.GetValue(true, true);
//        }

//        /// <summary>
//        /// Context menu handler.
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">The event arguments.</param>
//        private void SmallDownClick(object sender, EventArgs e)
//        {
//            this.Value = this.GetValue(true, false);
//        }

//        #endregion

//        #endregion
//    }
//}














////using System;
////using System.Collections.Generic;
////using System.ComponentModel;
////using System.Drawing;
////using System.Data;
////using System.Text;
////using System.Windows.Forms;
////using System.Windows.Forms.Design;
////using System.Diagnostics;


////namespace ProGammaX
////{

////    [Designer(typeof(ScrollbarControlDesigner))]
////    public class CustomScrollbar : UserControl
////    {

////        protected Color moChannelColor = Color.Empty;
////        protected Image moUpArrowImage = null;
////        //protected Image moUpArrowImage_Over = null;
////        //protected Image moUpArrowImage_Down = null;
////        protected Image moDownArrowImage = null;
////        //protected Image moDownArrowImage_Over = null;
////        //protected Image moDownArrowImage_Down = null;
////        protected Image moThumbArrowImage = null;

////        protected Image moThumbTopImage = null;
////        protected Image moThumbTopSpanImage = null;
////        protected Image moThumbBottomImage = null;
////        protected Image moThumbBottomSpanImage = null;
////        protected Image moThumbMiddleImage = null;

////        protected int moLargeChange = 10;
////        protected int moSmallChange = 1;
////        protected int moMinimum = 0;
////        protected int moMaximum = 100;
////        protected int moValue = 0;
////        private int nClickPoint;

////        protected int moThumbTop = 0;

////        protected bool moAutoSize = false;

////        private bool moThumbDown = false;
////        private bool moThumbDragging = false;

////        public new event EventHandler Scroll = null;
////        public event EventHandler ValueChanged = null;

////        private int GetThumbHeight()
////        {
////            int nTrackHeight = (this.Height - (UpArrowImage.Height + DownArrowImage.Height));
////            float fThumbHeight = ((float)LargeChange / (float)Maximum) * nTrackHeight;
////            int nThumbHeight = (int)fThumbHeight;

////            if (nThumbHeight > nTrackHeight)
////            {
////                nThumbHeight = nTrackHeight;
////                fThumbHeight = nTrackHeight;
////            }
////            if (nThumbHeight < 56)
////            {
////                nThumbHeight = 56;
////                fThumbHeight = 56;
////            }

////            return nThumbHeight;
////        }

////        public CustomScrollbar()
////        {

////            InitializeComponent();
////            SetStyle(ControlStyles.ResizeRedraw, true);
////            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
////            SetStyle(ControlStyles.DoubleBuffer, true);

////            moChannelColor = Color.FromArgb(51, 166, 3);
////            UpArrowImage = ProGammaX.Properties.Resources.Arrow_Up1;
////            DownArrowImage = ProGammaX.Properties.Resources.Arrow_Down1;


////            ThumbBottomImage = ProGammaX.Properties.Resources.Arrow_Down1; // Resource.ThumbBottom;
////            ThumbBottomSpanImage = ProGammaX.Properties.Resources.Arrow_Down1; // Resource.ThumbSpanBottom;
////            ThumbTopImage = ProGammaX.Properties.Resources.Arrow_Up1; // Resource.ThumbTop;
////            ThumbTopSpanImage = ProGammaX.Properties.Resources.Arrow_Up1; // Resource.ThumbSpanTop;
////            ThumbMiddleImage = ProGammaX.Properties.Resources.Plus; // Resource.ThumbMiddle;

////            this.Width = UpArrowImage.Width;
////            base.MinimumSize = new Size(UpArrowImage.Width, UpArrowImage.Height + DownArrowImage.Height + GetThumbHeight());
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Behavior"), Description("LargeChange")]
////        public int LargeChange
////        {
////            get { return moLargeChange; }
////            set
////            {
////                moLargeChange = value;
////                Invalidate();
////            }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Behavior"), Description("SmallChange")]
////        public int SmallChange
////        {
////            get { return moSmallChange; }
////            set
////            {
////                moSmallChange = value;
////                Invalidate();
////            }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Behavior"), Description("Minimum")]
////        public int Minimum
////        {
////            get { return moMinimum; }
////            set
////            {
////                moMinimum = value;
////                Invalidate();
////            }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Behavior"), Description("Maximum")]
////        public int Maximum
////        {
////            get { return moMaximum; }
////            set
////            {
////                moMaximum = value;
////                Invalidate();
////            }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Behavior"), Description("Value")]
////        public int Value
////        {
////            get { return moValue; }
////            set
////            {
////                moValue = value;

////                int nTrackHeight = (this.Height - (UpArrowImage.Height + DownArrowImage.Height));
////                float fThumbHeight = ((float)LargeChange / (float)Maximum) * nTrackHeight;
////                int nThumbHeight = (int)fThumbHeight;

////                if (nThumbHeight > nTrackHeight)
////                {
////                    nThumbHeight = nTrackHeight;
////                    fThumbHeight = nTrackHeight;
////                }
////                if (nThumbHeight < 56)
////                {
////                    nThumbHeight = 56;
////                    fThumbHeight = 56;
////                }

////                //figure out value
////                int nPixelRange = nTrackHeight - nThumbHeight;
////                int nRealRange = (Maximum - Minimum) - LargeChange;
////                float fPerc = 0.0f;
////                if (nRealRange != 0)
////                {
////                    fPerc = (float)moValue / (float)nRealRange;

////                }

////                float fTop = fPerc * nPixelRange;
////                moThumbTop = (int)fTop;


////                Invalidate();
////            }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Skin"), Description("Channel Color")]
////        public Color ChannelColor
////        {
////            get { return moChannelColor; }
////            set { moChannelColor = value; }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Skin"), Description("Up Arrow Graphic")]
////        public Image UpArrowImage
////        {
////            get { return moUpArrowImage; }
////            set { moUpArrowImage = value; }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Skin"), Description("Up Arrow Graphic")]
////        public Image DownArrowImage
////        {
////            get { return moDownArrowImage; }
////            set { moDownArrowImage = value; }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Skin"), Description("Up Arrow Graphic")]
////        public Image ThumbTopImage
////        {
////            get { return moThumbTopImage; }
////            set { moThumbTopImage = value; }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Skin"), Description("Up Arrow Graphic")]
////        public Image ThumbTopSpanImage
////        {
////            get { return moThumbTopSpanImage; }
////            set { moThumbTopSpanImage = value; }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Skin"), Description("Up Arrow Graphic")]
////        public Image ThumbBottomImage
////        {
////            get { return moThumbBottomImage; }
////            set { moThumbBottomImage = value; }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Skin"), Description("Up Arrow Graphic")]
////        public Image ThumbBottomSpanImage
////        {
////            get { return moThumbBottomSpanImage; }
////            set { moThumbBottomSpanImage = value; }
////        }

////        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true), DefaultValue(false), Category("Skin"), Description("Up Arrow Graphic")]
////        public Image ThumbMiddleImage
////        {
////            get { return moThumbMiddleImage; }
////            set { moThumbMiddleImage = value; }
////        }

////        protected override void OnPaint(PaintEventArgs e)
////        {

////            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;

////            if (UpArrowImage != null)
////            {
////                e.Graphics.DrawImage(UpArrowImage, new Rectangle(new Point(0, 0), new Size(this.Width, UpArrowImage.Height)));
////            }

////            Brush oBrush = new SolidBrush(moChannelColor);
////            Brush oWhiteBrush = new SolidBrush(Color.FromArgb(255, 255, 255));

////            //draw channel left and right border colors
////            e.Graphics.FillRectangle(oWhiteBrush, new Rectangle(0, UpArrowImage.Height, 1, (this.Height - DownArrowImage.Height)));
////            e.Graphics.FillRectangle(oWhiteBrush, new Rectangle(this.Width - 1, UpArrowImage.Height, 1, (this.Height - DownArrowImage.Height)));

////            //draw channel
////            e.Graphics.FillRectangle(oBrush, new Rectangle(1, UpArrowImage.Height, this.Width - 2, (this.Height - DownArrowImage.Height)));

////            //draw thumb
////            int nTrackHeight = (this.Height - (UpArrowImage.Height + DownArrowImage.Height));
////            float fThumbHeight = ((float)LargeChange / (float)Maximum) * nTrackHeight;
////            int nThumbHeight = (int)fThumbHeight;

////            if (nThumbHeight > nTrackHeight)
////            {
////                nThumbHeight = nTrackHeight;
////                fThumbHeight = nTrackHeight;
////            }
////            if (nThumbHeight < 56)
////            {
////                nThumbHeight = 56;
////                fThumbHeight = 56;
////            }

////            //Debug.WriteLine(nThumbHeight.ToString());

////            float fSpanHeight = (fThumbHeight - (ThumbMiddleImage.Height + ThumbTopImage.Height + ThumbBottomImage.Height)) / 2.0f;
////            int nSpanHeight = (int)fSpanHeight;

////            int nTop = moThumbTop;
////            nTop += UpArrowImage.Height;

////            //draw top
////            e.Graphics.DrawImage(ThumbTopImage, new Rectangle(1, nTop, this.Width - 2, ThumbTopImage.Height));

////            nTop += ThumbTopImage.Height;
////            //draw top span
////            Rectangle rect = new Rectangle(1, nTop, this.Width - 2, nSpanHeight);


////            e.Graphics.DrawImage(ThumbTopSpanImage, 1.0f, (float)nTop, (float)this.Width - 2.0f, (float)fSpanHeight * 2);

////            nTop += nSpanHeight;
////            //draw middle
////            e.Graphics.DrawImage(ThumbMiddleImage, new Rectangle(1, nTop, this.Width - 2, ThumbMiddleImage.Height));


////            nTop += ThumbMiddleImage.Height;
////            //draw top span
////            rect = new Rectangle(1, nTop, this.Width - 2, nSpanHeight * 2);
////            e.Graphics.DrawImage(ThumbBottomSpanImage, rect);

////            nTop += nSpanHeight;
////            //draw bottom
////            e.Graphics.DrawImage(ThumbBottomImage, new Rectangle(1, nTop, this.Width - 2, nSpanHeight));

////            if (DownArrowImage != null)
////            {
////                e.Graphics.DrawImage(DownArrowImage, new Rectangle(new Point(0, (this.Height - DownArrowImage.Height)), new Size(this.Width, DownArrowImage.Height)));
////            }

////        }

////        public override bool AutoSize
////        {
////            get
////            {
////                return base.AutoSize;
////            }
////            set
////            {
////                base.AutoSize = value;
////                if (base.AutoSize)
////                {
////                    this.Width = moUpArrowImage.Width;
////                }
////            }
////        }

////        private void InitializeComponent()
////        {
////            this.SuspendLayout();
////            // 
////            // CustomScrollbar
////            // 
////            this.Name = "CustomScrollbar";
////            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CustomScrollbar_MouseDown);
////            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.CustomScrollbar_MouseMove);
////            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.CustomScrollbar_MouseUp);
////            this.ResumeLayout(false);

////        }

////        private void CustomScrollbar_MouseDown(object sender, MouseEventArgs e)
////        {
////            Point ptPoint = this.PointToClient(Cursor.Position);
////            int nTrackHeight = (this.Height - (UpArrowImage.Height + DownArrowImage.Height));
////            float fThumbHeight = ((float)LargeChange / (float)Maximum) * nTrackHeight;
////            int nThumbHeight = (int)fThumbHeight;

////            if (nThumbHeight > nTrackHeight)
////            {
////                nThumbHeight = nTrackHeight;
////                fThumbHeight = nTrackHeight;
////            }
////            if (nThumbHeight < 56)
////            {
////                nThumbHeight = 56;
////                fThumbHeight = 56;
////            }

////            int nTop = moThumbTop;
////            nTop += UpArrowImage.Height;


////            Rectangle thumbrect = new Rectangle(new Point(1, nTop), new Size(ThumbMiddleImage.Width, nThumbHeight));
////            if (thumbrect.Contains(ptPoint))
////            {

////                //hit the thumb
////                nClickPoint = (ptPoint.Y - nTop);
////                //MessageBox.Show(Convert.ToString((ptPoint.Y - nTop)));
////                this.moThumbDown = true;
////            }

////            Rectangle uparrowrect = new Rectangle(new Point(1, 0), new Size(UpArrowImage.Width, UpArrowImage.Height));
////            if (uparrowrect.Contains(ptPoint))
////            {

////                int nRealRange = (Maximum - Minimum) - LargeChange;
////                int nPixelRange = (nTrackHeight - nThumbHeight);
////                if (nRealRange > 0)
////                {
////                    if (nPixelRange > 0)
////                    {
////                        if ((moThumbTop - SmallChange) < 0)
////                            moThumbTop = 0;
////                        else
////                            moThumbTop -= SmallChange;

////                        //figure out value
////                        float fPerc = (float)moThumbTop / (float)nPixelRange;
////                        float fValue = fPerc * (Maximum - LargeChange);

////                        moValue = (int)fValue;
////                        Debug.WriteLine(moValue.ToString());

////                        if (ValueChanged != null)
////                            ValueChanged(this, new EventArgs());

////                        if (Scroll != null)
////                            Scroll(this, new EventArgs());

////                        Invalidate();
////                    }
////                }
////            }

////            Rectangle downarrowrect = new Rectangle(new Point(1, UpArrowImage.Height + nTrackHeight), new Size(UpArrowImage.Width, UpArrowImage.Height));
////            if (downarrowrect.Contains(ptPoint))
////            {
////                int nRealRange = (Maximum - Minimum) - LargeChange;
////                int nPixelRange = (nTrackHeight - nThumbHeight);
////                if (nRealRange > 0)
////                {
////                    if (nPixelRange > 0)
////                    {
////                        if ((moThumbTop + SmallChange) > nPixelRange)
////                            moThumbTop = nPixelRange;
////                        else
////                            moThumbTop += SmallChange;

////                        //figure out value
////                        float fPerc = (float)moThumbTop / (float)nPixelRange;
////                        float fValue = fPerc * (Maximum - LargeChange);

////                        moValue = (int)fValue;
////                        Debug.WriteLine(moValue.ToString());

////                        if (ValueChanged != null)
////                            ValueChanged(this, new EventArgs());

////                        if (Scroll != null)
////                            Scroll(this, new EventArgs());

////                        Invalidate();
////                    }
////                }
////            }
////        }

////        private void CustomScrollbar_MouseUp(object sender, MouseEventArgs e)
////        {
////            this.moThumbDown = false;
////            this.moThumbDragging = false;
////        }

////        private void MoveThumb(int y)
////        {
////            int nRealRange = Maximum - Minimum;
////            int nTrackHeight = (this.Height - (UpArrowImage.Height + DownArrowImage.Height));
////            float fThumbHeight = ((float)LargeChange / (float)Maximum) * nTrackHeight;
////            int nThumbHeight = (int)fThumbHeight;

////            if (nThumbHeight > nTrackHeight)
////            {
////                nThumbHeight = nTrackHeight;
////                fThumbHeight = nTrackHeight;
////            }
////            if (nThumbHeight < 56)
////            {
////                nThumbHeight = 56;
////                fThumbHeight = 56;
////            }

////            int nSpot = nClickPoint;

////            int nPixelRange = (nTrackHeight - nThumbHeight);
////            if (moThumbDown && nRealRange > 0)
////            {
////                if (nPixelRange > 0)
////                {
////                    int nNewThumbTop = y - (UpArrowImage.Height + nSpot);

////                    if (nNewThumbTop < 0)
////                    {
////                        moThumbTop = nNewThumbTop = 0;
////                    }
////                    else if (nNewThumbTop > nPixelRange)
////                    {
////                        moThumbTop = nNewThumbTop = nPixelRange;
////                    }
////                    else
////                    {
////                        moThumbTop = y - (UpArrowImage.Height + nSpot);
////                    }

////                    //figure out value
////                    float fPerc = (float)moThumbTop / (float)nPixelRange;
////                    float fValue = fPerc * (Maximum - LargeChange);
////                    moValue = (int)fValue;
////                    Debug.WriteLine(moValue.ToString());

////                    Application.DoEvents();

////                    Invalidate();
////                }
////            }
////        }

////        private void CustomScrollbar_MouseMove(object sender, MouseEventArgs e)
////        {
////            if (moThumbDown == true)
////            {
////                this.moThumbDragging = true;
////            }

////            if (this.moThumbDragging)
////            {

////                MoveThumb(e.Y);
////            }

////            if (ValueChanged != null)
////                ValueChanged(this, new EventArgs());

////            if (Scroll != null)
////                Scroll(this, new EventArgs());
////        }

////    }

////    internal class ScrollbarControlDesigner : System.Windows.Forms.Design.ControlDesigner
////    {



////        public override SelectionRules SelectionRules
////        {
////            get
////            {
////                SelectionRules selectionRules = base.SelectionRules;
////                PropertyDescriptor propDescriptor = TypeDescriptor.GetProperties(this.Component)["AutoSize"];
////                if (propDescriptor != null)
////                {
////                    bool autoSize = (bool)propDescriptor.GetValue(this.Component);
////                    if (autoSize)
////                    {
////                        selectionRules = SelectionRules.Visible | SelectionRules.Moveable | SelectionRules.BottomSizeable | SelectionRules.TopSizeable;
////                    }
////                    else
////                    {
////                        selectionRules = SelectionRules.Visible | SelectionRules.AllSizeable | SelectionRules.Moveable;
////                    }
////                }
////                return selectionRules;
////            }
////        }
////    }
////}