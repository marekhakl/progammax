﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Reflection;
using System.Drawing.Imaging;

namespace ProGammaX
{
    public class dataGridView : DataGridView
    {
        private Bitmap Vienna_ScrollHorzThumb = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollHorzShaft = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollHorzArrow = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollVertThumb = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollVertShaft = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollVertArrow = new Bitmap(1, 1);
        private Bitmap Fader = new Bitmap(1, 1);
        private Dictionary<IntPtr, cScrollBar> _oVScrollbarSkin = new Dictionary<IntPtr, cScrollBar>() { };
        private Dictionary<IntPtr, cScrollBar> _oHScrollbarSkin = new Dictionary<IntPtr, cScrollBar>() { };
        private HScrollBar HScrollBarNew;
        private VScrollBar VScrollBarNew;

        public dataGridView()
        {
            HScrollBarNew = this.Controls.OfType<HScrollBar>().First();
            VScrollBarNew = this.Controls.OfType<VScrollBar>().First();

            DoubleBuffered = true;

            HScrollBarNew.VisibleChanged += HScrollBarNew_VisibleChanged;
            VScrollBarNew.VisibleChanged += VScrollBarNew_VisibleChanged;
        }

        private void HScrollBarNew_VisibleChanged(object sender, EventArgs e)
        {
            Start();
        }

        private void VScrollBarNew_VisibleChanged(object sender, EventArgs e)
        {

            Start();
        }
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            try
            {
                Start();
            }
            catch
            {

            }
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);

            try
            {
                if (this.Visible && this.FindForm().Visible)
                {
                    Start();
                }
            }
            catch
            {
            }
        }

        private void Start()
        {
            if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
            {
                RemoveHScrollBar(HScrollBarNew.Handle);
                RemoveVScrollBar(VScrollBarNew.Handle);

                DisposeVImages();
                DisposeHImages();
                return;
            }

            AddHScrollBar();
            AddVScrollBar();
        }

        private void DisposeHImages()
        {
            if (Vienna_ScrollHorzThumb != null)
                Vienna_ScrollHorzThumb.Dispose();
            if (Vienna_ScrollHorzShaft != null)
                Vienna_ScrollHorzShaft.Dispose();
            if (Vienna_ScrollHorzArrow != null)
                Vienna_ScrollHorzArrow.Dispose();
        }

        private void DisposeVImages()
        {
            if (Vienna_ScrollVertThumb != null)
                Vienna_ScrollVertThumb.Dispose();
            if (Vienna_ScrollVertShaft != null)
                Vienna_ScrollVertShaft.Dispose();
            if (Vienna_ScrollVertArrow != null)
                Vienna_ScrollVertArrow.Dispose();
        }

        private void LoadHImages()
        {
            DisposeHImages();

            Vienna_ScrollHorzThumb = MainForm.mainFormRef.vienna_ScrollHorzThumb.Clone(new Rectangle(0,0,MainForm.mainFormRef.vienna_ScrollHorzThumb.Width, MainForm.mainFormRef.vienna_ScrollHorzThumb.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollHorzShaft = MainForm.mainFormRef.vienna_ScrollHorzShaft.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollHorzShaft.Width, MainForm.mainFormRef.vienna_ScrollHorzShaft.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollHorzArrow = MainForm.mainFormRef.vienna_ScrollHorzArrow.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollHorzArrow.Width, MainForm.mainFormRef.vienna_ScrollHorzArrow.Height), PixelFormat.Format16bppRgb555);
            Fader = null;// new Bitmap(ProGammaX.Properties.Resources.fader);
        }

        private void LoadVImages()
        {
            DisposeVImages();

           Vienna_ScrollVertThumb = MainForm.mainFormRef.vienna_ScrollVertThumb.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollVertThumb.Width, MainForm.mainFormRef.vienna_ScrollVertThumb.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollVertShaft = MainForm.mainFormRef.vienna_ScrollVertShaft.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollVertShaft.Width, MainForm.mainFormRef.vienna_ScrollVertShaft.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollVertArrow = MainForm.mainFormRef.vienna_ScrollVertArrow.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollVertArrow.Width, MainForm.mainFormRef.vienna_ScrollVertArrow.Height), PixelFormat.Format16bppRgb555);
            Fader = null;
        }

        /// <summary>
        /// Remove a specific control from the skin engine
        /// </summary>
        /// <param name="handle">Control handle</param>
        public void RemoveHScrollBar(IntPtr handle)
        {
            if (handle != IntPtr.Zero)
            {
                Control ctl = Control.FromHandle(handle);
                cScrollBar value;

                if (ctl != null && _oHScrollbarSkin.TryGetValue(ctl.Handle, out value))
                {
                    value.Dispose();
                    _oHScrollbarSkin.Remove(ctl.Handle);
                }
            }
        }

        /// <summary>
        /// Remove a specific control from the skin engine
        /// </summary>
        /// <param name="handle">Control handle</param>
        public void RemoveVScrollBar(IntPtr handle)
        {
            if (handle != IntPtr.Zero)
            {
                Control ctl = Control.FromHandle(handle);
                cScrollBar value;

                if (ctl != null && _oVScrollbarSkin.TryGetValue(ctl.Handle, out value))
                {
                    value.Dispose();
                    _oVScrollbarSkin.Remove(ctl.Handle);
                }
            }
        }

        /// <summary>
        /// Add method for the ScrollBar control
        /// </summary>
        /// <param name="ct">Control type [ScrollBar]</param>
        /// <param name="track">Track image</param>
        /// <param name="arrow">Arrow image</param>
        /// <param name="thumb">Thumb image</param>
        /// <param name="orientation">ScrollbBar orientation</param>
        public void AddVScrollBar()
        {
            if (_oVScrollbarSkin == null)
                _oVScrollbarSkin = new Dictionary<IntPtr, cScrollBar>();

            Control ctl = Control.FromHandle(VScrollBarNew.Handle);
            if (ctl != null)
            {
                Type t = ctl.GetType();
                string name = t.Name.ToLower();

                if (name == "vscrollbar")
                {
                    if (_oVScrollbarSkin.ContainsKey(ctl.Handle))
                    {
                        //var removeItem = _oVScrollbar[this.Handle];
                        RemoveVScrollBar(ctl.Handle);

                        //_oVScrollbar.Remove(removeItem);
                    }
                    //_oVScrollbar.Add(VScrollBarNew.Handle, VScrollBarNew.Handle);
                    if (ctl.Visible)
                    {
                        LoadVImages();
                        _oVScrollbarSkin.Add(ctl.Handle, new cScrollBar(ctl.Handle, Orientation.Vertical, Vienna_ScrollVertThumb, Vienna_ScrollVertShaft, Vienna_ScrollVertArrow, Fader));
                    }

                    ctl.Refresh();
                }

            }

        }

        /// <summary>
        /// Add method for the ScrollBar control
        /// </summary>
        /// <param name="ct">Control type [ScrollBar]</param>
        /// <param name="track">Track image</param>
        /// <param name="arrow">Arrow image</param>
        /// <param name="thumb">Thumb image</param>
        /// <param name="orientation">ScrollbBar orientation</param>
        public void AddHScrollBar()
        {

            if (_oHScrollbarSkin == null)
                _oHScrollbarSkin = new Dictionary<IntPtr, cScrollBar>();

            Control ctl = Control.FromHandle(HScrollBarNew.Handle);
            if (ctl != null)
            {
                Type t = ctl.GetType();
                string name = t.Name.ToLower();

                if (name == "hscrollbar")
                {
                    if (_oHScrollbarSkin.ContainsKey(ctl.Handle))
                    {
                        RemoveHScrollBar(HScrollBarNew.Handle);
                    }

                    if (ctl.Visible)
                    {
                        LoadHImages();
                        _oHScrollbarSkin.Add(ctl.Handle, new cScrollBar(ctl.Handle, Orientation.Horizontal, Vienna_ScrollHorzThumb, Vienna_ScrollHorzShaft, Vienna_ScrollHorzArrow, Fader));
                    }

                    ctl.Refresh();
                }
            }

        }

        //[DllImport("user32")]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //private static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr i);

        //// enum child delegate
        //public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        //private bool EnumWindow(IntPtr handle, IntPtr pointer)
        //{
        //    GCHandle gch = GCHandle.FromIntPtr(pointer);
        //    List<IntPtr> list = gch.Target as List<IntPtr>;

        //    if (list != null)
        //    {
        //        list.Add(handle);
        //        return true;
        //    }
        //    return false;
        //}

        //private List<IntPtr> GetChildWindows(IntPtr parent)
        //{
        //    List<IntPtr> result = new List<IntPtr>();
        //    GCHandle listHandle = GCHandle.Alloc(result);
        //    try
        //    {
        //        EnumWindowProc childProc = new EnumWindowProc(EnumWindow);
        //        EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
        //    }
        //    finally
        //    {
        //        if (listHandle.IsAllocated)
        //            listHandle.Free();
        //    }
        //    return result;
        //}

        protected override void OnKeyDown(KeyEventArgs e)
        {


            // Extract the key code from the key value.
            try
            {

                if ((e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z) || (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9))
                {
                    string pressedKey = string.Empty;

                    if ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9))
                    {

                        pressedKey = Strings.Right(e.KeyCode.ToString(), 1);

                    }
                    else
                    {

                        pressedKey = e.KeyCode.ToString();

                    }




                    foreach (DataGridViewRow tempRow in this.Rows)
                    {
                        if ((tempRow.Cells[2].Value.ToString()).StartsWith(pressedKey))
                        {


                            this.CurrentCell = tempRow.Cells[2];
                            break;
                        }
                    }

                }
            }
            catch
            {


            }


            base.OnKeyDown(e);
        }


        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, Flags = System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)]
        protected override bool ProcessDataGridViewKey(System.Windows.Forms.KeyEventArgs e)
        {
            // Handle the ENTER key as if it were a RIGHT ARROW key.
            if (e.KeyCode == Keys.Enter)
            {
                return false;
                //  Return Me.ProcessRightKey(e.KeyData)
            }


            return base.ProcessDataGridViewKey(e);
        }

        [System.Security.Permissions.UIPermission(System.Security.Permissions.SecurityAction.LinkDemand, Window = System.Security.Permissions.UIPermissionWindow.AllWindows)]
        protected override bool ProcessDialogKey(Keys keyData)
        {
            // Extract the key code from the key value.
            Keys key = keyData & Keys.KeyCode;

            // Handle the ENTER key as if it were a RIGHT ARROW key.
            if (key == Keys.Enter)
            {
                return false;
                // Return Me.ProcessRightKey(keyData)
            }



            return base.ProcessDialogKey(keyData);
        }
    }
}
