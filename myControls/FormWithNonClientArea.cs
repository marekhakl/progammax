#region Custom Border Forms - Copyright (C) 2005 Szymon Kobalczyk

// Custom Border Forms
// Copyright (C) 2005 Szymon Kobalczyk
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// Szymon Kobalczyk (http://www.geekswithblogs.com/kobush)

#endregion

#region Using directives

using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.Reflection; // used for logging 
using System.Drawing.Drawing2D;
using ProGammaX.Properties;
using ProGammaX;

#endregion

namespace Kobush.Windows.Forms
{
    // Nonclient Area
    // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/gdi/pantdraw_9uht.asp
    // http://support.microsoft.com/kb/q99046/

    //see also http://www.google.pl/groups?q=ncpaint+group:*dotnet*&hl=pl&lr=&c2coff=1&selm=%23%245FnRUpEHA.2304%40TK2MSFTNGP14.phx.gbl&rnum=7
    //http://www.google.pl/groups?hl=pl&lr=&c2coff=1&selm=02EF5D19-D288-4C0E-818E-752DB176F5C0%40microsoft.com&rnum=15
    //http://www.codeguru.com/Cpp/controls/buttonctrl/advancedbuttons/article.php/c5179/

    //http://dotnetrix.co.uk/ncpanel.html

    //http://www.vbaccelerator.com/home/VB/Code/Libraries/XP_Visual_Styles/Drawing_with_XP_Visual_Styles/article.asp

    // Display a shaded title bar
    // http://www.vb-helper.com/howto_shaded_titlebar.html

    //http://www.catch22.net/tuts/titlebar.asp

    // The best of all
    //http://www.mindspring.com/~cityzoo/ttlbar1.html

    /// <summary>
    /// Extended form class that suports drawing in non-client area.
    /// </summary>
#if !DEBUGFORM
    [DebuggerStepThrough]
#endif
    [System.ComponentModel.DesignerCategory("code")]
    public class FormWithNonClientArea : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style = cp.Style | (int)NativeMethods.WindowStyle.WS_SYSMENU;
                return cp;
            }
        }

        private bool _nonClientAreaDoubleBuffering;
        private FormWindowState WindowFormState = FormWindowState.Normal;

        [DefaultValue(false)]
        public virtual bool NonClientAreaDoubleBuffering
        {
            get { return _nonClientAreaDoubleBuffering; }
            set
            {
                _nonClientAreaDoubleBuffering = value;
                // No need to invalidate anything,
                // next painting will use double-buffering.
            }
        }

        private bool _enableNonClientAreaPaint = true;

        [DefaultValue(true)]
        public bool EnableNonClientAreaPaint
        {
            get { return _enableNonClientAreaPaint; }
            set
            {
                _enableNonClientAreaPaint = value;
                InvalidateWindow();
            }
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            // Disable theming on current window so we don't get 
            // any funny artifacts (round corners, etc.)
            NativeMethods.SetWindowTheme(this.Handle, "", "");

#if !DEBUG
            // When application window stops responding to messages
            // system will finally loose patience and repaint it with default theme.
            // This prevents such behavior for entire application.
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/lowlevelclientsupport/misc/rtldisableprocesswindowsghosting.asp
            NativeMethods.DisableProcessWindowsGhosting();
#endif
            base.OnHandleCreated(e);
        }

        protected override void WndProc(ref Message m)
        {
            Log(MethodInfo.GetCurrentMethod(), "Message = {0}", (NativeMethods.WindowMessages)m.Msg);

            if (!this.EnableNonClientAreaPaint)
            {
                base.WndProc(ref m);
                return;
            }

            switch (m.Msg)
            {
                case (int)NativeMethods.WindowMessages.WM_NCCALCSIZE:
                    {
                        // Provides new coordinates for the window client area.
                        WmNCCalcSize(ref m);
                        this.Update();
                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_NCHITTEST:
                    {
                        // Tell the system what element is at the current mouse location. 
                        WmNCHitTest(ref m);
                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_NCPAINT:
                    {
                        // Here should all our painting occur, but...
                        WmNCPaint(ref m);
                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_NCACTIVATE:
                    {
                        // ... WM_NCACTIVATE does some painting directly 
                        // without bothering with WM_NCPAINT ...
                        WmNCActivate(ref m);
                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_SETTEXT:
                    {
                        // ... and some painting is required in here as well
                        WmSetText(ref m);
                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_NCMOUSEMOVE:
                    {
                        WmNCMouseMove(ref m);
                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_NCMOUSELEAVE:
                    {
                        WmNCMouseLeave(ref m);
                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_NCLBUTTONDOWN:
                    {
                        WmNCLButtonDown(ref m);
                        break;
                    }
                case 174: // ignore magic message number
                    {
                        //Update();
                        Log(MethodInfo.GetCurrentMethod(), "### Magic message ### WParam = {0}", m.WParam.ToInt32());
                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_SYSCOMMAND:
                    {
                        WmSysCommand(ref m);

                        if ((int)m.WParam == (int)NativeMethods.SystemCommands.SC_RESTORE)
                        {
                            if (WindowState == FormWindowState.Maximized && WindowFormState == FormWindowState.Minimized)
                            {
                                SuspendLayout();
                                WindowState = FormWindowState.Normal;
                                Size = new Size(Width, Height);
                                WindowState = FormWindowState.Maximized;
                                WindowFormState = WindowState;
                                ResumeLayout();
                            }
                        }

                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_WINDOWPOSCHANGED:
                    {
                        WmWindowPosChanged(ref m);
                        break;
                    }
                case (int)NativeMethods.WindowMessages.WM_ERASEBKGND:
                    {

                        WmEraseBkgnd(ref m);
                        //this.Update();
                        break;
                    }
                default:
                    {
                        base.WndProc(ref m);
                        break;
                    }
            }
        }

        //protected virtual void UpdateWindowState()
        //{

        //}

        private void WmEraseBkgnd(ref Message m)
        {
            base.WndProc(ref m);

            Log(MethodInfo.GetCurrentMethod(), "{0}", WindowState);
            UpdateWindowState();
        }

        private void WmWindowPosChanged(ref Message m)
        {
            base.WndProc(ref m);

            Log(MethodInfo.GetCurrentMethod(), "{0}", WindowState);
            UpdateWindowState();
        }

        protected virtual void OnSystemCommand(int sc)
        {
        }

        private void WmSysCommand(ref Message m)
        {
            Log(MethodInfo.GetCurrentMethod(), "{0}", (NativeMethods.SystemCommands)m.WParam.ToInt32());

            this.OnSystemCommand(m.WParam.ToInt32());

            DefWndProc(ref m);
        }

        public Point PointToWindow(Point screenPoint)
        {
            return new Point(screenPoint.X - Location.X, screenPoint.Y - Location.Y);
        }

        /// <summary>
        /// Adjust the supplied Rectangle to the desired position of the client rectangle. 
        /// </summary>
        //protected virtual void OnNonClientAreaCalcSize(ref Rectangle bounds, bool update)
        //{ }

        private void WmNCCalcSize(ref Message m)
        {
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/windowing/windows/windowreference/windowmessages/wm_nccalcsize.asp
            // http://groups.google.pl/groups?selm=OnRNaGfDEHA.1600%40tk2msftngp13.phx.gbl

            if (m.WParam == NativeMethods.FALSE)
            {
                NativeMethods.RECT ncRect = (NativeMethods.RECT)m.GetLParam(typeof(NativeMethods.RECT));
                Rectangle proposed = ncRect.Rect;

                Log(MethodInfo.GetCurrentMethod(), string.Format("### Client Rect [0] = ({0},{1}) x ({2},{3})",
                    proposed.Left, proposed.Top, proposed.Width, proposed.Height));

                OnNonClientAreaCalcSize(ref proposed, true);
                ncRect = NativeMethods.RECT.FromRectangle(proposed);

                Marshal.StructureToPtr(ncRect, m.LParam, false);
            }
            else if (m.WParam == NativeMethods.TRUE)
            {
                NativeMethods.NCCALCSIZE_PARAMS ncParams = (NativeMethods.NCCALCSIZE_PARAMS)m.GetLParam(typeof(NativeMethods.NCCALCSIZE_PARAMS));
                Rectangle proposed = ncParams.rectProposed.Rect;

                Log(MethodInfo.GetCurrentMethod(), string.Format("### Client Rect [1] = ({0},{1}) x ({2},{3})",
                    proposed.Left, proposed.Top, proposed.Width, proposed.Height));

                OnNonClientAreaCalcSize(ref proposed, true);
                ncParams.rectProposed = NativeMethods.RECT.FromRectangle(proposed);

                Marshal.StructureToPtr(ncParams, m.LParam, false);
            }
            m.Result = IntPtr.Zero;
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            if (Visible)
                this.Refresh();
        }

        protected override void OnClientSizeChanged(EventArgs e)
        {
            base.OnClientSizeChanged(e);

            Log(MethodInfo.GetCurrentMethod(), String.Format("$$$$$ ClientSize = ({0},{1}),  FormSize = ({2},{3})",
                ClientSize.Width, ClientSize.Height, Size.Width, Size.Height));
        }

        protected override void SetClientSizeCore(int x, int y)
        {
            if (!EnableNonClientAreaPaint)
                base.SetClientSizeCore(x, y);

            this.Size = SizeFromClientSize(x, y, true);

            //  this.clientWidth = x;
            //  this.clientHeight = y;
            //  this.OnClientSizeChanged(EventArgs.Empty);
            // do this instead of above.
            this.UpdateBounds(Location.X, Location.Y, Size.Width, Size.Height, x, y);
        }

        private Size SizeFromClientSize(int x, int y, bool updatebuttons)
        {
            Rectangle bounds = new Rectangle(0, 0, 1000, 1000);
            OnNonClientAreaCalcSize(ref bounds, updatebuttons);

            return new Size(x + (1000 - bounds.Width), y + (1000 - bounds.Height));
        }

        protected override Size SizeFromClientSize(Size clientSize)
        {
            if (!EnableNonClientAreaPaint)
                return base.SizeFromClientSize(clientSize);

            return SizeFromClientSize(clientSize.Width, clientSize.Height, false);
        }

        /// <summary>
        /// Returns a value from a NCHITTEST enumeration specifing the window element on given point.
        /// </summary>
        //protected virtual int OnNonClientAreaHitTest(Point clientPoint)
        //{
        //    return (int)NativeMethods.NCHITTEST.HTCLIENT;
        //}

        private void WmNCHitTest(ref Message m)
        {
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/userinput/mouseinput/mouseinputreference/mouseinputmessages/wm_nchittest.asp

            Point screenPoint = new Point(m.LParam.ToInt32());
            Log(MethodInfo.GetCurrentMethod(), string.Format("### Screen Point ({0},{1})", screenPoint.X, screenPoint.Y));

            // convert to local coordinates
            Point clientPoint = PointToWindow(screenPoint);
            Log(MethodInfo.GetCurrentMethod(), string.Format("### Client Point ({0},{1})", clientPoint.X, clientPoint.Y));
            m.Result = new System.IntPtr(OnNonClientAreaHitTest(clientPoint));
        }

        /// <summary>
        /// Delivers new mouse position when it is moved over the non client area of the window. 
        /// </summary>
        //protected virtual void OnNonClientMouseMove(MouseEventArgs args)
        //{
        //}

        private void WmNCMouseMove(ref Message msg)
        {
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/userinput/mouseinput/mouseinputreference/mouseinputmessages/wm_nchittest.asp

            Point clientPoint = this.PointToWindow(new Point(msg.LParam.ToInt32()));
            OnNonClientMouseMove(new MouseEventArgs(MouseButtons.None, 0,
                clientPoint.X, clientPoint.Y, 0));
            msg.Result = IntPtr.Zero;
        }

        /// <summary>
        /// Called when mouse cursor leaves the non client window area.
        /// </summary>
        /// <param name="args"></param>
        //protected virtual void OnNonClientMouseLeave(EventArgs args)
        //{
        //}

        private void WmNCMouseLeave(ref Message m)
        {
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/userinput/mouseinput/mouseinputreference/mouseinputmessages/wm_ncmouseleave.asp
            OnNonClientMouseLeave(EventArgs.Empty);
        }

        /// <summary>
        /// Called each time one of the mouse buttons was pressed over the non-client area.
        /// </summary>
        /// <param name="args">NonClientMouseEventArgs contain mouse position, button pressed,
        /// and hit-test code for current position. </param>
        //protected virtual void OnNonClientMouseDown(NonClientMouseEventArgs args)
        //{

        //}

        private void WmNCLButtonDown(ref Message msg)
        {
            Point pt = this.PointToWindow(new Point(msg.LParam.ToInt32()));
            NonClientMouseEventArgs args = new NonClientMouseEventArgs(
                MouseButtons.Left, 1, pt.X, pt.Y, 0, msg.WParam.ToInt32());
            OnNonClientMouseDown(args);
            if (!args.Handled)
            {
                DefWndProc(ref msg);
            }
            msg.Result = NativeMethods.TRUE;

            WindowFormState = WindowState;
        }

        private void WmNCPaint(ref Message msg)
        {
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/gdi/pantdraw_8gdw.asp
            // example in q. 2.9 on http://www.syncfusion.com/FAQ/WindowsForms/FAQ_c41c.aspx#q1026q

            // The WParam contains handle to clipRegion or 1 if entire window should be repainted
            PaintNonClientArea(msg.HWnd, (IntPtr)msg.WParam);

            // we handled everything
            msg.Result = NativeMethods.TRUE;
        }

        private void WmSetText(ref Message msg)
        {
            // allow the system to receive the new window title
            DefWndProc(ref msg);

            // repaint title bar
            PaintNonClientArea(msg.HWnd, (IntPtr)1);
        }

        private void WmNCActivate(ref Message msg)
        {
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/windowing/windows/windowreference/windowmessages/wm_ncactivate.asp

            bool active = (msg.WParam == NativeMethods.TRUE);
            Log(MethodInfo.GetCurrentMethod(), "### Draw active title bar = {0}", active);

            if (WindowState == FormWindowState.Minimized)
                DefWndProc(ref msg);
            else
            {
                // repaint title bar
                PaintNonClientArea(msg.HWnd, (IntPtr)1);

                // allow to deactivate window
                msg.Result = NativeMethods.TRUE;
            }

            WindowFormState = WindowState;
        }

        /// <summary>
        /// Paints the client rect - e.ClipingRect has the correct window size, since this.Width, this.Height
        /// aren't always correct when calling this methode (because window is actually resizing)
        /// </summary>
        /// <param name="e"></param>
        //protected virtual void OnNonClientAreaPaint(NonClientPaintEventArgs e)
        //{
        //    e.Graphics.FillRectangle(new SolidBrush(Color.DarkViolet), 0, 0, e.Bounds.Width, e.Bounds.Height);
        //}

        private void PaintNonClientArea(IntPtr hWnd, IntPtr hRgn)
        {
            NativeMethods.RECT windowRect = new NativeMethods.RECT();
            if (NativeMethods.GetWindowRect(hWnd, ref windowRect) == 0)
                return;

            Rectangle bounds = new Rectangle(0, 0,
                windowRect.right - windowRect.left,
                windowRect.bottom - windowRect.top);

            if (bounds.Width == 0 || bounds.Height == 0)
                return;

            // The update region is clipped to the window frame. When wParam is 1, the entire window frame needs to be updated. 
            Region clipRegion = null;
            if (hRgn != (IntPtr)1)
                clipRegion = System.Drawing.Region.FromHrgn(hRgn);

            // MSDN states that only WINDOW and INTERSECTRGN are needed,
            // but other sources confirm that CACHE is required on Win9x
            // and you need CLIPSIBLINGS to prevent painting on overlapping windows.
            IntPtr hDC = NativeMethods.GetDCEx(hWnd, hRgn,
                (int)(NativeMethods.DCX.DCX_WINDOW | NativeMethods.DCX.DCX_INTERSECTRGN
                    | NativeMethods.DCX.DCX_CACHE | NativeMethods.DCX.DCX_CLIPSIBLINGS));

            if (hDC == IntPtr.Zero)
                return;


            try
            {

                if (!this.NonClientAreaDoubleBuffering)
                {
                    using (Graphics g = Graphics.FromHdc(hDC))
                    {
                        //cliping rect is not cliping rect but actual rectangle
                        OnNonClientAreaPaint(new NonClientPaintEventArgs(g, bounds, clipRegion));
                    }

                    //NOTE: The Graphics object would realease the HDC on Dispose.
                    // So there is no need to call NativeMethods.ReleaseDC(msg.HWnd, hDC);
                    //http://groups.google.pl/groups?hl=pl&lr=&c2coff=1&client=firefox-a&rls=org.mozilla:en-US:official_s&threadm=%23DDSaH7BFHA.3644%40TK2MSFTNGP15.phx.gbl&rnum=15&prev=/groups%3Fq%3DWM_NCPaint%2B%2BGetDCEx%26start%3D10%26hl%3Dpl%26lr%3D%26c2coff%3D1%26client%3Dfirefox-a%26rls%3Dorg.mozilla:en-US:official_s%26selm%3D%2523DDSaH7BFHA.3644%2540TK2MSFTNGP15.phx.gbl%26rnum%3D15
                    //http://groups.google.pl/groups?hl=pl&lr=&c2coff=1&client=firefox-a&rls=org.mozilla:en-US:official_s&threadm=cmo00r%24j9v%241%40mamut1.aster.pl&rnum=1&prev=/groups%3Fq%3DDCX_PARENTCLIP%26hl%3Dpl%26lr%3D%26c2coff%3D1%26client%3Dfirefox-a%26rls%3Dorg.mozilla:en-US:official_s%26selm%3Dcmo00r%2524j9v%25241%2540mamut1.aster.pl%26rnum%3D1
                }
                else
                {
                    //http://www.codeproject.com/csharp/flicker_free.asp
                    //http://www.pinvoke.net/default.aspx/gdi32/BitBlt.html

                    //IntPtr CompatiblehDC = NativeMethods.CreateCompatibleDC(hDC);
                    //IntPtr CompatibleBitmap = NativeMethods.CreateCompatibleBitmap(hDC, bounds.Width, bounds.Height);

                    //try
                    //{
                    //    NativeMethods.SelectObject(CompatiblehDC, CompatibleBitmap);

                    //    // copy current screen to bitmap
                    //    //TODO: this is quite slow (80% of this method). Why?
                    //    NativeMethods.BitBlt(CompatiblehDC, 0, 0, bounds.Width, bounds.Height, hDC, 0, 0, NativeMethods.TernaryRasterOperations.SRCCOPY);

                    //    using (Graphics g = Graphics.FromHdc(CompatiblehDC))
                    //    {
                    //        OnNonClientAreaPaint(new NonClientPaintEventArgs(g, bounds, clipRegion));
                    //    }

                    //    // copy current from bitmap to screen
                    //    NativeMethods.BitBlt(hDC, 0, 0, bounds.Width, bounds.Height, CompatiblehDC, 0, 0, NativeMethods.TernaryRasterOperations.SRCCOPY);
                    //}
                    //finally
                    //{
                    //    NativeMethods.DeleteObject(CompatibleBitmap);
                    //    NativeMethods.DeleteDC(CompatiblehDC);

                    //}

#if !NET1X
                    //.NET 2.0 has this new class BufferedGraphics. But it paints the clien area in all black.
                    //I dont know how to use it properly.

                    using (BufferedGraphics bg = BufferedGraphicsManager.Current.Allocate(hDC, bounds))
                    {
                        Graphics g = bg.Graphics;
                        //Rectangle clientRect = this.ClientRectangle;

                        OnNonClientAreaPaint(new NonClientPaintEventArgs(g, bounds, clipRegion));
                        bg.Render();

                        g.Dispose();
                    }
#endif
                }
            }
            finally
            {
                NativeMethods.ReleaseDC(this.Handle, hDC);
            }
        }

        /// <summary>
        /// This method should invalidate entire window including the non-client area.
        /// </summary>
        protected void InvalidateWindow()
        {
            if (!IsDisposed && IsHandleCreated)
            {
                NativeMethods.SetWindowPos(this.Handle, IntPtr.Zero, 0, 0, 0, 0,
                    (int)(NativeMethods.SetWindowPosOptions.SWP_NOACTIVATE | NativeMethods.SetWindowPosOptions.SWP_NOMOVE | NativeMethods.SetWindowPosOptions.SWP_NOSIZE |
                    NativeMethods.SetWindowPosOptions.SWP_NOZORDER | NativeMethods.SetWindowPosOptions.SWP_NOOWNERZORDER | NativeMethods.SetWindowPosOptions.SWP_FRAMECHANGED));

                NativeMethods.RedrawWindow(this.Handle, IntPtr.Zero, IntPtr.Zero,
                    (int)(NativeMethods.RedrawWindowOptions.RDW_FRAME | NativeMethods.RedrawWindowOptions.RDW_UPDATENOW | NativeMethods.RedrawWindowOptions.RDW_INVALIDATE));
            }
        }

        #region Log
        [Conditional("DEBUGFORM")]
        protected internal static void Log(System.Reflection.MemberInfo callingMethod, string message, params object[] args)
        {
            if (args != null)
                message = String.Format(message, args);
            Debug.WriteLine(String.Format("{0}.{1} - {2}", callingMethod.DeclaringType.Name, callingMethod.Name, message));
        }
        #endregion //Log











        private CaptionButton closeButton = new CaptionButton();
        private CaptionButton maximizeButton = new CaptionButton();
        private CaptionButton minimizeButton = new CaptionButton();
        private CaptionButton helpButton = new CaptionButton();

        private CaptionButtonCollection _captionButtons = new CaptionButtonCollection();

        private CaptionButtonCollection CaptionButtons
        {
            get { return _captionButtons; }
        }

        public FormStyle CreateFormStyle()
        {
            FormStyle style = new FormStyle();
            style.NormalState.Image = cColorBalance.colorBalance((Bitmap)Resources.Border, Color.FromArgb(255, ThemedColors.WindowFrameColor.R, ThemedColors.WindowFrameColor.G, ThemedColors.WindowFrameColor.B));
            style.NormalState.SizeMode = ImageSizeMode.Stretched;
            style.NormalState.StretchMargins = new Padding(3, 23, 3, 3);

            style.CloseButton.Size = Resources.Close.Size;
            style.CloseButton.Margin = new Padding(1, 5, 5, 0);
            style.CloseButton.NormalState.Image = Resources.Close;
            style.CloseButton.DisabledState.Image = Resources.CloseDisabled;
            style.CloseButton.ActiveState.Image = Resources.ClosePressed;
            style.CloseButton.HoverState.Image = Resources.CloseHot;

            style.MaximizeButton.Size = Resources.Maximize.Size;
            style.MaximizeButton.Margin = new Padding(1, 5, 1, 0);
            style.MaximizeButton.NormalState.Image = Resources.Maximize;
            style.MaximizeButton.DisabledState.Image = Resources.MaximizeDisabled;
            style.MaximizeButton.ActiveState.Image = Resources.MaximizePressed;
            style.MaximizeButton.HoverState.Image = Resources.MaximizeHot;

            style.MinimizeButton.Size = Resources.Minimize.Size;
            style.MinimizeButton.Margin = new Padding(1, 5, 1, 0);
            style.MinimizeButton.NormalState.Image = Resources.Minimize;
            style.MinimizeButton.DisabledState.Image = Resources.MinimizeDisabled;
            style.MinimizeButton.ActiveState.Image = Resources.MinimizePressed;
            style.MinimizeButton.HoverState.Image = Resources.MinimizeHot;

            style.RestoreButton.Size = Resources.Restore.Size;
            style.RestoreButton.Margin = new Padding(1, 5, 1, 0);
            style.RestoreButton.NormalState.Image = Resources.Restore;
            style.RestoreButton.DisabledState.Image = Resources.RestoreDisabled;
            style.RestoreButton.ActiveState.Image = Resources.RestorePressed;
            style.RestoreButton.HoverState.Image = Resources.RestoreHot;

            style.TitleColor = ThemedColors.ForeColor;
            style.TitleShadowColor = ThemedColors.BackColor;
            style.TitleFont = new Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);

            style.ClientAreaPadding = new Padding(3, 23, 3, 3);
            style.IconPadding = new Padding(3, 3, 0, 0);

            return style;
        }

        public FormStyle CreateFormHeader(bool minimizeButtonEnabled = true, bool maximizeButtonEnabled = true, bool closeButtonEnabled = true)
        {
            closeButton = new CaptionButton();
            maximizeButton = new CaptionButton();
            minimizeButton = new CaptionButton();
            helpButton = new CaptionButton();

            MinimizeButton = minimizeButtonEnabled;
            MaximizeButton = maximizeButtonEnabled;
            CloseButton = closeButtonEnabled;

            closeButton.Key = "close";
            closeButton.Visible = true;
            closeButton.HitTestCode = (int)NativeMethods.NCHITTEST.HTCLOSE;
            _captionButtons.Add(closeButton);


            maximizeButton.Key = "maximize";
            maximizeButton.Enabled = MaximizeButton;
            maximizeButton.HitTestCode = (int)NativeMethods.NCHITTEST.HTMAXBUTTON;
            _captionButtons.Add(maximizeButton);


            minimizeButton.Key = "minimize";
            minimizeButton.Enabled = MinimizeBox;
            minimizeButton.HitTestCode = (int)NativeMethods.NCHITTEST.HTMINBUTTON;
            _captionButtons.Add(minimizeButton);

            this.minimizeButton.Visible = this.maximizeButton.Visible
                = this.maximizeButton.Enabled | this.minimizeButton.Enabled;


            helpButton.Key = "help";
            helpButton.Visible = this.HelpButton;
            helpButton.HitTestCode = (int)NativeMethods.NCHITTEST.HTHELP;
            _captionButtons.Add(helpButton);

            this.NonClientAreaDoubleBuffering = false;
            this.EnableNonClientAreaPaint = true;
            this.Icon = Resources.Profile11_a;

            return CreateFormStyle();
        }
        public FormWithNonClientArea()
        {
            //UpdateActiveFormStyle();

            FormStyleManager.StyleChanged += new EventHandler(FormStyleManager_StyleChanged);
        }

        void FormStyleManager_StyleChanged(object sender, EventArgs args)
        {
            UpdateActiveFormStyle();
        }

        private FormStyle _formStyle;

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public FormStyle FormStyle
        {
            get { return _formStyle; }
            set
            {
                if (_formStyle != value)
                {
                    _formStyle = value;
                    UpdateActiveFormStyle();
                }
            }
        }


        private bool _useFormStyleManager;

        [Category("Appearance")]
        [DefaultValue(false)]
        public bool UseFormStyleManager
        {
            get { return _useFormStyleManager; }
            set
            {
                if (_useFormStyleManager != value)
                {
                    _useFormStyleManager = value;
                    UpdateActiveFormStyle();
                }
            }
        }

        private string _formStyleName;

        [Category("Appearance")]
        [DefaultValue(null)]
        public string FormStyleName
        {
            get { return _formStyleName; }
            set
            {
                if (_formStyleName != value)
                {
                    _formStyleName = value;
                    UpdateActiveFormStyle();
                }
            }
        }

        public void UpdateActiveFormStyle()
        {
            FormStyle style = null;
            if (UseFormStyleManager)
            {
                // try to load specified style
                if (!String.IsNullOrEmpty(FormStyleName))
                    style = FormStyleManager.GetStyle(FormStyleName);

                // if it wasn't found try to load default style
                if (style == null)
                    style = FormStyleManager.GetDefaultStyle();
            }
            else
            {
                style = FormStyle;
            }
            ActiveFormStyle = style;
        }

        private FormStyle _activeformStyle;

        public FormStyle ActiveFormStyle
        {
            get { return _activeformStyle; }
            set
            {
                if (_activeformStyle != value)
                {
                    if (_activeformStyle != null)
                        _activeformStyle.ChildPropertyChanged -= new ChildPropertyChangedEventHandler(FormStyle_ChildPropertyChanged);

                    _activeformStyle = value;

                    if (_activeformStyle != null)
                        _activeformStyle.ChildPropertyChanged += new ChildPropertyChangedEventHandler(FormStyle_ChildPropertyChanged);

                    UpdateButtonAppearance();
                    InvalidateWindow();
                }
            }
        }

        void FormStyle_ChildPropertyChanged(object sender, ChildPropertyChangedEventArgs args)
        {
            InvalidateWindow();
        }

        private void UpdateButtonAppearance()
        {
            if (ActiveFormStyle != null)
            {
                closeButton.Appearance = ActiveFormStyle.CloseButton;
                minimizeButton.Appearance = ActiveFormStyle.MinimizeButton;
                maximizeButton.Appearance = (this.WindowState == FormWindowState.Maximized) ? ActiveFormStyle.RestoreButton : ActiveFormStyle.MaximizeButton;
                helpButton.Appearance = ActiveFormStyle.HelpButton;
            }
            else
            {
                closeButton.Appearance = null;
                minimizeButton.Appearance = null;
                maximizeButton.Appearance = null;
                helpButton.Appearance = null;
            }
        }

        private new bool MaximizeBox = true;
        public bool MaximizeButton
        {
            get
            {
                return this.MaximizeBox;
            }
            set
            {
                this.maximizeButton.Enabled = value;
                this.minimizeButton.Visible = this.maximizeButton.Visible
                    = this.maximizeButton.Enabled | this.minimizeButton.Enabled;
                this.MaximizeBox = value;
            }
        }

        private new bool MinimizeBox = true;
        public bool MinimizeButton
        {
            get { return this.MinimizeBox; }
            set
            {
                this.minimizeButton.Enabled = value;
                this.minimizeButton.Visible = this.maximizeButton.Visible
                    = this.maximizeButton.Enabled | this.minimizeButton.Enabled;
                this.MinimizeBox = value;
            }
        }

        private new bool ControlBox = true;
        public bool CloseButton
        {
            get { return this.ControlBox; }
            set
            {
                this.closeButton.Enabled = value;
                this.ControlBox = value;
            }
        }

        private new bool HelpButton = true;
        public bool ButtonHelp
        {
            get { return this.HelpButton; }
            set
            {
                this.helpButton.Visible = value;
                this.HelpButton = value;
            }
        }

        protected virtual void OnNonClientAreaCalcSize(ref Rectangle bounds, bool update)
        {
            if (ActiveFormStyle == null)
                return;

            if (update)
                UpdateCaptionButtonBounds(bounds);

            Padding clientPadding = ActiveFormStyle.ClientAreaPadding;
            bounds = new Rectangle(bounds.Left + clientPadding.Left, bounds.Top + clientPadding.Top,
                bounds.Width - clientPadding.Horizontal, bounds.Height - clientPadding.Vertical);
        }

        private void UpdateCaptionButtonBounds(Rectangle windowRect)
        {
            // start from top-right corner
            int x = windowRect.Width;

            foreach (CaptionButton button in this.CaptionButtons)
            {
                if (button.Visible)
                {
                    int y = button.Appearance.Margin.Top;
                    x -= (button.Appearance.Size.Width + button.Appearance.Margin.Right);
                    button.Bounds = new Rectangle(x, y,
                        button.Appearance.Size.Width, button.Appearance.Size.Height);
                    x -= button.Appearance.Margin.Left;
                }

                // background will be recreated on next redraw
                button.BackgroundImage = null;
            }

            // Should I move this where this actually changes WM_GETMINMAXINFO ??
            //maximizeButton.Appearance = (this.WindowState == FormWindowState.Maximized) ?
            //    _borderAppearance.RestoreButton : _borderAppearance.MaximizeButton;
        }

        protected virtual void OnNonClientMouseLeave(EventArgs args)
        {
            if (!trakingMouse)
                return;

            foreach (CaptionButton button in this.CaptionButtons)
            {
                if (button.State != CaptionButtonState.Normal)
                {
                    button.State = CaptionButtonState.Normal;
                    DrawButton(button);
                    UnhookMouseEvent();
                }
            }
        }

        protected virtual void OnNonClientMouseDown(NonClientMouseEventArgs args)
        {
            if (args.Button != MouseButtons.Left)
                return;

            switch (args.HitTest)
            {
                case (int)NativeMethods.NCHITTEST.HTCLOSE:
                    {
                        //NativeMethods.POINT pt = NativeMethods.POINT.FromPoint(args.Location);
                        if (DepressButton(closeButton))
                            NativeMethods.SendMessage(this.Handle,
                                (int)NativeMethods.WindowMessages.WM_SYSCOMMAND,
                                (IntPtr)NativeMethods.SystemCommands.SC_CLOSE, IntPtr.Zero);
                        args.Handled = true;
                        break;
                    }
                case (int)NativeMethods.NCHITTEST.HTMINBUTTON:
                    {
                        //		NativeMethods.POINT pt = NativeMethods.POINT.FromPoint(args.Location);
                        if (DepressButton(minimizeButton))
                            NativeMethods.SendMessage(this.Handle,
                                (int)NativeMethods.WindowMessages.WM_SYSCOMMAND,
                                (IntPtr)NativeMethods.SystemCommands.SC_MINIMIZE, IntPtr.Zero);
                        args.Handled = true;
                        break;
                    }
                case (int)NativeMethods.NCHITTEST.HTMAXBUTTON:
                    {
                        if (DepressButton(maximizeButton))
                        {
                            int sc = (WindowState == FormWindowState.Maximized) ?
                                (int)NativeMethods.SystemCommands.SC_RESTORE : (int)NativeMethods.SystemCommands.SC_MAXIMIZE;

                            NativeMethods.SendMessage(this.Handle,
                                (int)NativeMethods.WindowMessages.WM_SYSCOMMAND,
                                (IntPtr)sc, IntPtr.Zero);
                        }
                        args.Handled = true;
                        break;
                    }
            }

            //TODO: handle other buttons if exist
        }

        protected virtual void UpdateWindowState()
        {
            if (ActiveFormStyle == null)
                return;

            FormButtonStyle newAppearance;
            if (this.WindowState == FormWindowState.Maximized)
                newAppearance = this.ActiveFormStyle.RestoreButton;
            else
                newAppearance = this.ActiveFormStyle.MaximizeButton;

            if (newAppearance != maximizeButton.Appearance)
            {
                maximizeButton.Appearance = newAppearance;
                DrawButton(maximizeButton);
            }
        }

        private void DrawButton(CaptionButton button)
        {
            if (IsHandleCreated)
            {
                // MSDN states that only WINDOW and INTERSECTRGN are needed,
                // but other sources confirm that CACHE is required on Win9x
                // and you need CLIPSIBLINGS to prevent painting on overlapping windows.

                IntPtr windowDC = NativeMethods.GetDCEx(this.Handle, IntPtr.Zero, (int)(NativeMethods.DCX.DCX_CACHE | NativeMethods.DCX.DCX_WINDOW | NativeMethods.DCX.DCX_CLIPSIBLINGS | NativeMethods.DCX.DCX_PARENTCLIP | NativeMethods.DCX.DCX_LOCKWINDOWUPDATE));

                if (windowDC != IntPtr.Zero)
                {
                    using (Graphics g = Graphics.FromHdc(windowDC))
                    {
                        button.DrawButton(g, false);
                    }
                }

                NativeMethods.ReleaseDC(this.Handle, windowDC);
            }
        }

        private bool DepressButton(CaptionButton currentButton)
        {
            bool result = false;

            if (!currentButton.Enabled)
                return false;

            try
            {

                // callect all mouse events until WL_BUTTONUP
                this.Capture = true;        // hopefully doeas the same as SetCapture(handle)

                // draw button in pressed mode
                currentButton.State = CaptionButtonState.Pressed;
                DrawButton(currentButton);

                // loop untill button is released
                bool done = false;
                while (!done)
                {
                    // NOTE: This struct needs to be here. We had strange error (starting from Beta 2).
                    // when this was defined at begining of this method. also check if types are correct (no overlap). 
                    Message m = new Message();
                    if (NativeMethods.PeekMessage(ref m, IntPtr.Zero,
                        (int)NativeMethods.WindowMessages.WM_MOUSEFIRST, (int)NativeMethods.WindowMessages.WM_MOUSELAST,
                        (int)NativeMethods.PeekMessageOptions.PM_REMOVE))
                    {
                        Log(MethodInfo.GetCurrentMethod(), "Message = {0}, Button = {1}", (NativeMethods.WindowMessages)m.Msg, currentButton);
                        switch (m.Msg)
                        {
                            case (int)NativeMethods.WindowMessages.WM_LBUTTONUP:
                                {
                                    if (currentButton.State == CaptionButtonState.Pressed)
                                    {
                                        currentButton.State = CaptionButtonState.Normal;
                                        DrawButton(currentButton);
                                    }
                                    Point pt = PointToWindow(PointToScreen(new Point(m.LParam.ToInt32())));
                                    Log(MethodInfo.GetCurrentMethod(), "### Point = ({0}, {1})", pt.X, pt.Y);
                                    result = currentButton.Bounds.Contains(pt);
                                    done = true;
                                    break;
                                }
                            case (int)NativeMethods.WindowMessages.WM_MOUSEMOVE:
                                {
                                    Point clientPoint = PointToWindow(PointToScreen(new Point(m.LParam.ToInt32())));
                                    if (currentButton.Bounds.Contains(clientPoint))
                                    {
                                        if (currentButton.State == CaptionButtonState.Normal)
                                        {
                                            currentButton.State = CaptionButtonState.Pressed;
                                            DrawButton(currentButton);
                                        }
                                    }
                                    else
                                    {
                                        if (currentButton.State == CaptionButtonState.Pressed)
                                        {
                                            currentButton.State = CaptionButtonState.Normal;
                                            DrawButton(currentButton);
                                        }
                                    }
                                    break;
                                }
                        }
                    }
                }
            }
            finally
            {
                this.Capture = false;
            }

            return result;
        }

        protected virtual int OnNonClientAreaHitTest(Point p)
        {
            if (ActiveFormStyle == null)
                return (int)NativeMethods.NCHITTEST.HTCLIENT;

            foreach (CaptionButton button in this.CaptionButtons)
            {
                if (button.Visible && button.Bounds.Contains(p) && button.HitTestCode > 0)
                    return button.HitTestCode;
            }

            if (FormBorderStyle != FormBorderStyle.FixedToolWindow &&
                FormBorderStyle != FormBorderStyle.SizableToolWindow)
            {
                if (GetIconRectangle().Contains(p))
                    return (int)NativeMethods.NCHITTEST.HTSYSMENU;
            }

            if (this.FormBorderStyle == FormBorderStyle.Sizable
                || this.FormBorderStyle == FormBorderStyle.SizableToolWindow)
            {
                #region Handle sizable window borders
                if (p.X <= ActiveFormStyle.SizingBorderWidth) // left border
                {
                    if (p.Y <= ActiveFormStyle.SizingCornerOffset)
                        return (int)NativeMethods.NCHITTEST.HTTOPLEFT;
                    else if (p.Y >= this.Height - ActiveFormStyle.SizingCornerOffset)
                        return (int)NativeMethods.NCHITTEST.HTBOTTOMLEFT;
                    else
                        return (int)NativeMethods.NCHITTEST.HTLEFT;
                }
                else if (p.X >= this.Width - ActiveFormStyle.SizingBorderWidth) // right border
                {
                    if (p.Y <= ActiveFormStyle.SizingCornerOffset)
                        return (int)NativeMethods.NCHITTEST.HTTOPRIGHT;
                    else if (p.Y >= this.Height - ActiveFormStyle.SizingCornerOffset)
                        return (int)NativeMethods.NCHITTEST.HTBOTTOMRIGHT;
                    else
                        return (int)NativeMethods.NCHITTEST.HTRIGHT;
                }
                else if (p.Y <= ActiveFormStyle.SizingBorderWidth) // top border
                {
                    if (p.X <= ActiveFormStyle.SizingCornerOffset)
                        return (int)NativeMethods.NCHITTEST.HTTOPLEFT;
                    if (p.X >= this.Width - ActiveFormStyle.SizingCornerOffset)
                        return (int)NativeMethods.NCHITTEST.HTTOPRIGHT;
                    else
                        return (int)NativeMethods.NCHITTEST.HTTOP;
                }
                else if (p.Y >= this.Height - ActiveFormStyle.SizingBorderWidth) // bottom border
                {
                    if (p.X <= ActiveFormStyle.SizingCornerOffset)
                        return (int)NativeMethods.NCHITTEST.HTBOTTOMLEFT;
                    if (p.X >= this.Width - ActiveFormStyle.SizingCornerOffset)
                        return (int)NativeMethods.NCHITTEST.HTBOTTOMRIGHT;
                    else
                        return (int)NativeMethods.NCHITTEST.HTBOTTOM;
                }
                #endregion
            }

            // title bar
            if (p.Y <= ActiveFormStyle.ClientAreaPadding.Top)
                return (int)NativeMethods.NCHITTEST.HTCAPTION;

            // rest of non client area
            if (p.X <= this.ActiveFormStyle.ClientAreaPadding.Left || p.X >= this.ActiveFormStyle.ClientAreaPadding.Right
                || p.Y >= this.ActiveFormStyle.ClientAreaPadding.Bottom)
                return (int)NativeMethods.NCHITTEST.HTBORDER;

            return (int)NativeMethods.NCHITTEST.HTCLIENT;
        }

        protected virtual void OnNonClientMouseMove(MouseEventArgs mouseEventArgs)
        {
            foreach (CaptionButton button in this.CaptionButtons)
            {
                if (button.Visible && button.Bounds.Contains(mouseEventArgs.X, mouseEventArgs.Y) && button.HitTestCode > 0)
                {
                    if (button.State != CaptionButtonState.Over)
                    {

                        button.State = CaptionButtonState.Over;
                        DrawButton(button);
                        HookMouseEvent();
                    }
                }
                else
                {
                    if (button.State != CaptionButtonState.Normal)
                    {
                        button.State = CaptionButtonState.Normal;
                        DrawButton(button);
                        UnhookMouseEvent();
                    }
                }
            }
        }

        NativeMethods.TRACKMOUSEEVENT trackMouseEvent;
        bool trakingMouse = false;

        private void HookMouseEvent()
        {
            if (!trakingMouse)
            {
                trakingMouse = true;
                if (this.trackMouseEvent == null)
                {
                    this.trackMouseEvent = new NativeMethods.TRACKMOUSEEVENT();
                    this.trackMouseEvent.dwFlags =
                        (int)(NativeMethods.TrackMouseEventFalgs.TME_HOVER |
                              NativeMethods.TrackMouseEventFalgs.TME_LEAVE |
                              NativeMethods.TrackMouseEventFalgs.TME_NONCLIENT);

                    this.trackMouseEvent.hwndTrack = this.Handle;
                }

                if (NativeMethods.TrackMouseEvent(this.trackMouseEvent) == false)
                    // use getlasterror to see whats wrong
                    Log(MethodInfo.GetCurrentMethod(), "Failed enabling TrackMouseEvent: error {0}",
                        NativeMethods.GetLastError());
            }
        }

        private void UnhookMouseEvent()
        {
            trakingMouse = false;
        }

        private Rectangle GetIconRectangle()
        {
            return new Rectangle(ActiveFormStyle.IconPadding.Left, ActiveFormStyle.IconPadding.Top, 16, 16);
        }

        Icon smallIcon;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (smallIcon != null)
                    smallIcon.Dispose();

                FormStyleManager.StyleChanged -= new EventHandler(FormStyleManager_StyleChanged);
                ActiveFormStyle = null;
            }
        }

        public new Icon Icon
        {
            get { return base.Icon; }
            set
            {
                if (value != Icon)
                {
                    if (this.smallIcon != null)
                    {
                        smallIcon.Dispose();
                        smallIcon = null;
                    }
                    try
                    {
                        smallIcon = new Icon(value, SystemInformation.SmallIconSize);
                    }
                    catch
                    { }
                }
                base.Icon = value;
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            if (ActiveFormStyle == null || ActiveFormStyle.NormalState.Image == null)
                return;

            Rectangle srcRect =
                DrawUtil.ExcludePadding(new Rectangle(Point.Empty, ActiveFormStyle.NormalState.Image.Size),
                    ActiveFormStyle.ClientAreaPadding);

            Padding margins =
                DrawUtil.SubstractPadding(ActiveFormStyle.NormalState.StretchMargins, ActiveFormStyle.ClientAreaPadding);

            DrawUtil.DrawImage(e.Graphics, ActiveFormStyle.NormalState.Image, srcRect, ClientRectangle, null, margins);
        }

        protected virtual void OnNonClientAreaPaint(NonClientPaintEventArgs e)
        {
            if (ActiveFormStyle == null)
                return;

            // assign clip region to exclude client area
            Region clipRegion = new Region(e.Bounds);
            clipRegion.Exclude(DrawUtil.ExcludePadding(e.Bounds, ActiveFormStyle.ClientAreaPadding));
            e.Graphics.Clip = clipRegion;

            // paint borders
            ActiveFormStyle.NormalState.DrawImage(e.Graphics, e.Bounds);

            int textOffset = 0;

            // paint icon
            //if (ShowIcon &&
            //    FormBorderStyle != FormBorderStyle.FixedToolWindow &&
            //    FormBorderStyle != FormBorderStyle.SizableToolWindow)
            //{
            //    Rectangle iconRect = GetIconRectangle();
            //    textOffset += iconRect.Right;

            //    if (smallIcon != null)
            //        e.Graphics.DrawIconUnstretched(smallIcon, iconRect);
            //    else
            //        e.Graphics.DrawIcon(Icon, iconRect);
            //}


            // paint caption
            string text = this.Text;
            if (!String.IsNullOrEmpty(text))
            {
                // disable text wrapping and request elipsis characters on overflow
                using (StringFormat sf = new StringFormat())
                {
                    sf.Trimming = StringTrimming.EllipsisCharacter;
                    sf.FormatFlags = StringFormatFlags.NoWrap;
                    sf.LineAlignment = StringAlignment.Center;

                    // find position of the first button from left
                    int firstButton = e.Bounds.Width;
                    foreach (CaptionButton button in this.CaptionButtons)
                        if (button.Visible)
                            firstButton = Math.Min(firstButton, button.Bounds.X);

                    Padding padding = ActiveFormStyle.TitlePadding;
                    Rectangle textRect = new Rectangle(textOffset + padding.Left,
                        padding.Top, firstButton - textOffset - padding.Horizontal,
                        ActiveFormStyle.ClientAreaPadding.Top - padding.Vertical);

                    Font textFont = this.Font;
                    if (ActiveFormStyle.TitleFont != null)
                        textFont = ActiveFormStyle.TitleFont;

                    if (!ActiveFormStyle.TitleShadowColor.IsEmpty)
                    {
                        Rectangle shadowRect = textRect;
                        shadowRect.Offset(1, 1);

                        // draw drop shadow
                        using (Brush b = new SolidBrush(ActiveFormStyle.TitleShadowColor))
                        {
                            e.Graphics.DrawString(text, textFont, b, shadowRect, sf);
                        }
                    }

                    if (!ActiveFormStyle.TitleColor.IsEmpty)
                    {
                        // draw text
                        using (Brush b = new SolidBrush(ActiveFormStyle.TitleColor))
                        {
                            e.Graphics.DrawString(text, textFont, b, textRect, sf);
                        }
                    }
                }
            }

            // paint buttons
            foreach (CaptionButton button in this.CaptionButtons)
                button.DrawButton(e.Graphics, false);
        }

        public enum CaptionButtonState
        {
            Normal, Pressed, Over
        }

        #region class CaptionButton

#if !DEBUGFORM
        [DebuggerStepThrough]
#endif
        private class CaptionButton
        {
            private Rectangle _bounds;
            private CaptionButtonState _state;
            private FormButtonStyle _appearance;
            private string _key;
            private bool _visible = true;
            private int _hitTestCode = -1;
            private bool _enabled = true;

            public override string ToString()
            {
                return this.Key;
            }

            public CaptionButtonState State
            {
                get { return _state; }
                set { _state = value; }
            }

            public Rectangle Bounds
            {
                get
                {
                    //_bounds = new Rectangle(_bounds.Left, _bounds.Top, _bounds.Width, _bounds.Height);
                    return _bounds;
                }
                set { _bounds = value; }
            }

            public FormButtonStyle Appearance
            {
                get { return _appearance; }
                set { _appearance = value; }
            }

            public string Key
            {
                get { return _key; }
                set { _key = value; }
            }

            public int HitTestCode
            {
                get { return _hitTestCode; }
                set { _hitTestCode = value; }
            }

            public bool Visible
            {
                get { return _visible; }
                set { _visible = value; }
            }

            public bool Enabled
            {
                get { return _enabled; }
                set { _enabled = value; }
            }

            private Image _backgroundImage;

            public Image BackgroundImage
            {
                get { return _backgroundImage; }
                set { _backgroundImage = value; }
            }

            public void DrawButton(Graphics g, bool paintBackground)
            {
                if (!Visible)
                    return;

                if (paintBackground && BackgroundImage != null)
                    g.DrawImage(BackgroundImage, Bounds);

                if (this.Enabled)
                {
                    switch (this.State)
                    {
                        case CaptionButtonState.Normal:
                            if (Appearance.NormalState != null)
                                Appearance.NormalState.DrawImage(g, Bounds);
                            break;
                        case CaptionButtonState.Pressed:
                            if (Appearance.ActiveState != null)
                                Appearance.ActiveState.DrawImage(g, Bounds);
                            break;
                        case CaptionButtonState.Over:
                            if (Appearance.HoverState != null)
                                Appearance.HoverState.DrawImage(g, Bounds);
                            break;
                    }
                }
                else
                {
                    Appearance.DisabledState.DrawImage(g, Bounds);
                }
            }
        }

        #endregion // END CaptionButton

        #region class CaptionButtonCollection
#if !DEBUGFORM
        [DebuggerStepThrough]
#endif
        private class CaptionButtonCollection : CollectionBase
        {
            public void Add(CaptionButton button)
            {
                this.List.Add(button);
            }
        }

        #endregion

        // END CustomBorderForm










#if !DEBUGFORM
        [DebuggerStepThrough]
#endif
        public class NonClientMouseEventArgs : MouseEventArgs
        {
            private int _hitTest;
            private bool _handled;

            public NonClientMouseEventArgs(MouseButtons button, int clicks, int x, int y, int delta, int hitTest)
                : base(button, clicks, x, y, delta)
            {
                _hitTest = hitTest;
            }

            public int HitTest
            {
                get { return _hitTest; }
                set { _hitTest = value; }
            }

            public bool Handled
            {
                get { return _handled; }
                set { _handled = value; }
            }

        }

#if !DEBUGFORM
        [DebuggerStepThrough]
#endif
        public class NonClientPaintEventArgs : EventArgs
        {
            public NonClientPaintEventArgs(Graphics g, Rectangle bounds, Region clipRegion)
            {
                _graphics = g;
                _bounds = bounds;
                _clipRegion = clipRegion;
            }

            private Rectangle _bounds;
            public Rectangle Bounds
            {
                get { return _bounds; }
            }

            private Region _clipRegion;
            public Region ClipRegion
            {
                get { return _clipRegion; }
            }

            private Graphics _graphics;
            public Graphics Graphics
            {
                get { return _graphics; }
            }
        }

    }

}