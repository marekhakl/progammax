﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ProGammaX
{
    class trackBar : TrackBar
    {
        private Bitmap Vienna_thumb = new Bitmap(1, 1);
        private Bitmap Vienna_Track = new Bitmap(1, 1);
        private Dictionary<IntPtr, cTrackBar> _oScrollbarSkin;

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            try
            {
                Start();
            }
            catch
            {

            }
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);

            try
            {
                if (this.Visible && this.FindForm().Visible)
                {
                    Start();
                }
            }
            catch
            {
            }
        }

        private void Start()
        {
            if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
            {
                return;
            }

            AddScrollBar();
        }

        private void LoadImages()
        {
            Vienna_thumb.Dispose();
            Vienna_Track.Dispose();

            Vienna_thumb = new Bitmap(MainForm.mainFormRef.vienna_thumb);
            Vienna_Track = new Bitmap(MainForm.mainFormRef.vienna_Track);
        }

        /// <summary>
        /// Remove a specific control from the skin engine
        /// </summary>
        /// <param name="handle">Control handle</param>
        public void RemoveScrollBar(IntPtr handle)
        {
            if (handle != IntPtr.Zero)
            {
                Control ctl = Control.FromHandle(handle);

                if (ctl != null)
                {
                    _oScrollbarSkin[ctl.Handle].Dispose();
                    _oScrollbarSkin.Remove(ctl.Handle);
                }
            }
        }

        /// <summary>
        /// Add method for the ScrollBar control
        /// </summary>
        /// <param name="ct">Control type [ScrollBar]</param>
        /// <param name="track">Track image</param>
        /// <param name="arrow">Arrow image</param>
        /// <param name="thumb">Thumb image</param>
        /// <param name="orientation">ScrollbBar orientation</param>
        public void AddScrollBar()
        {
            if (_oScrollbarSkin == null)
                _oScrollbarSkin = new Dictionary<IntPtr, cTrackBar>();

            var control = Control.FromHandle(this.Handle);
            if (control is trackBar)
            {
                if (_oScrollbarSkin.ContainsKey(control.Handle))
                {
                    RemoveScrollBar(control.Handle);
                }

                if (control.Visible)
                {
                    LoadImages();
                    _oScrollbarSkin.Add(control.Handle, new cTrackBar(control.Handle, Vienna_thumb, Vienna_Track));
                }

                control.Refresh();
            }
        }
    }
}
