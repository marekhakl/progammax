﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using XCoolForm;

namespace ProGammaX
{
    public class myForm : XCoolForm.XCoolForm
    {
        private bool MinimizeButtonEnabled { get; set; }
        private bool MaximizeButtonEnabled { get; set; }
        private bool CloseButtonEnabled { get; set; }

        public myForm(bool minimizeButtonEnabled = true, bool maximizeButtonEnabled = true, bool closeButtonEnabled = true)
        {
            MinimizeButtonEnabled = minimizeButtonEnabled;
            MaximizeButtonEnabled = maximizeButtonEnabled;
            CloseButtonEnabled = closeButtonEnabled;

            TitleBar.TitleBarMixColors.Add(Color.FromArgb(245, 245, 245));
            TitleBar.TitleBarMixColors.Add(Color.FromArgb(93, 93, 93));
            TitleBar.TitleBarMixColors.Add(Color.FromArgb(45, 45, 45));
            TitleBar.TitleBarMixColors.Add(Color.FromArgb(30, 30, 30));
            TitleBar.TitleBarMixColors.Add(Color.FromArgb(52, 52, 52));
            
            // initialize titlebar buttons:
            TitleBar.TitleBarButtons.Add(new XTitleBarButton(XTitleBarButton.XTitleBarButtonType.Close, Color.FromArgb(124, 13, 2), Color.FromArgb(251, 164, 48)));
            TitleBar.TitleBarButtons.Add(new XTitleBarButton(XTitleBarButton.XTitleBarButtonType.Maximize, Color.FromArgb(3, 63, 126), Color.FromArgb(119, 217, 246)));
            TitleBar.TitleBarButtons.Add(new XTitleBarButton(XTitleBarButton.XTitleBarButtonType.Minimize));
        }

        public void ApplyTheme(myForm form)
        {
            //form.FormBorderStyle = FormBorderStyle.None;
            // initialize mix:
            form.TitleBar.ButtonBoxMixColors[0] = ThemedColors.GridColor;
            form.TitleBar.ButtonBoxMixColors[1] = ThemedColors.MenuColor;
            form.TitleBar.ButtonBoxMixColors[2] = ThemedColors.BackColor;
            form.TitleBar.ButtonBoxMixColors[3] = ThemedColors.WindowFrameColor;
            form.TitleBar.ButtonBoxMixColors[4] = ThemedColors.MenuColor;
            form.TitleBar.ButtonBoxInnerBorder = ThemedColors.GridColor;
            form.TitleBar.ButtonBoxOuterBorder = ThemedColors.WindowFrameColor;

            form.MenuIcon = Properties.Resources.Profile11;
            form.TitleBar.TitleBarFill = XTitleBar.XTitleBarFill.AdvancedRendering;

            form.TitleBar.TitleBarMixColors[0] = ThemedColors.MenuColor;
            form.TitleBar.TitleBarMixColors[1] = ThemedColors.GridColor;
            form.TitleBar.TitleBarMixColors[2] = ThemedColors.WindowFrameColor;
            form.TitleBar.TitleBarMixColors[3] = ThemedColors.WindowFrameColor;
            form.TitleBar.TitleBarMixColors[4] = ThemedColors.BackColor;
            form.TitleBar.TitleBarType = XTitleBar.XTitleBarType.Default;

            form.TitleBar.TitleBarButtons[2].Enabled = form.MinimizeButtonEnabled;
            form.TitleBar.TitleBarButtons[1].Enabled = form.MaximizeButtonEnabled;
            form.TitleBar.TitleBarButtons[0].Enabled = form.CloseButtonEnabled;

            // initialize titlebar buttons:
            form.TitleBar.TitleBarButtons[2].ButtonFillMode = XCoolForm.XTitleBarButton.XButtonFillMode.FullFill;
            form.TitleBar.TitleBarButtons[1].ButtonFillMode = XCoolForm.XTitleBarButton.XButtonFillMode.FullFill;
            form.TitleBar.TitleBarButtons[0].ButtonFillMode = XCoolForm.XTitleBarButton.XButtonFillMode.FullFill;
            form.Padding = new Padding(8, 35, 8, 8);

            form.Border.OuterBorderColors[0] = ThemedColors.WindowFrameColor;
            form.Border.OuterBorderColors[1] = ThemedColors.WindowFrameColor;
            form.Border.InnerBorderColors[0] = ThemedColors.BackColor;
            form.Border.InnerBorderColors[1] = ThemedColors.WindowFrameColor;
            form.Border.InnerBorderColors[2] = ThemedColors.WindowFrameColor;
            form.Border.InnerBorderColors[3] = ThemedColors.BackColor;

            form.XFormBackColor = ThemedColors.BackColor;
            form.StatusBar.StatusStartColor = ThemedColors.BackColor;
            form.StatusBar.StatusEndColor = ThemedColors.BackColor;

            form.TitleBar.TitleBarCaptionColor = ThemedColors.MenuTextColor;
            form.TitleBar.TitleBarCaption = form.Text;
            form.TitleBar.InnerTitleBarColor = Color.Transparent;
            form.TitleBar.OuterTitleBarColor = Color.Transparent;
        }
    }
}
