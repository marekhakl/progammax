﻿using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Reflection;
using System.Drawing.Imaging;

namespace ProGammaX
{
    public class ListViewColumnSorter : IComparer
    {
        /// <summary>
        /// Specifies the column to be sorted
        /// </summary>
        private int ColumnToSort;
        /// <summary>
        /// Specifies the order in which to sort (i.e. 'Ascending').
        /// </summary>
        private SortOrder OrderOfSort;
        /// <summary>
        /// Case insensitive comparer object
        /// </summary>
        private CaseInsensitiveComparer ObjectCompare;

        /// <summary>
        /// Class constructor.  Initializes various elements
        /// </summary>
        public ListViewColumnSorter()
        {
            // Initialize the column to '0'
            ColumnToSort = 0;

            // Initialize the sort order to 'none'
            OrderOfSort = SortOrder.None;

            // Initialize the CaseInsensitiveComparer object
            ObjectCompare = new CaseInsensitiveComparer();
        }

        /// <summary>
        /// This method is inherited from the IComparer interface.  It compares the two objects passed using a case insensitive comparison.
        /// </summary>
        /// <param name="x">First object to be compared</param>
        /// <param name="y">Second object to be compared</param>
        /// <returns>The result of the comparison. "0" if equal, negative if 'x' is less than 'y' and positive if 'x' is greater than 'y'</returns>
        public int Compare(object x, object y)
        {
            int compareResult;
            ListViewItem listviewX, listviewY;

            // Cast the objects to be compared to ListViewItem objects
            listviewX = (ListViewItem)x;
            listviewY = (ListViewItem)y;

            // Compare the two items
            compareResult = ObjectCompare.Compare(listviewX.SubItems[ColumnToSort].Text, listviewY.SubItems[ColumnToSort].Text);

            // Calculate correct return value based on object comparison
            if (OrderOfSort == SortOrder.Ascending)
            {
                // Ascending sort is selected, return normal result of compare operation
                return compareResult;
            }
            else if (OrderOfSort == SortOrder.Descending)
            {
                // Descending sort is selected, return negative result of compare operation
                return (-compareResult);
            }
            else
            {
                // Return '0' to indicate they are equal
                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the number of the column to which to apply the sorting operation (Defaults to '0').
        /// </summary>
        public int SortColumn
        {
            set
            {
                ColumnToSort = value;
            }
            get
            {
                return ColumnToSort;
            }
        }

        /// <summary>
        /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
        /// </summary>
        public SortOrder Order
        {
            set
            {
                OrderOfSort = value;
            }
            get
            {
                return OrderOfSort;
            }
        }
    }

    class listView : ListView
    {
        private Bitmap Vienna_ScrollHorzThumb = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollHorzShaft = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollHorzArrow = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollVertThumb = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollVertShaft = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollVertArrow = new Bitmap(1, 1);
        private Bitmap Header = new Bitmap(1, 1);
        private Dictionary<IntPtr, cListView> _oScrollbarSkin;

        public listView()
        {
            this.OwnerDraw = true;
            this.HideSelection = false;
            this.DoubleBuffered = true;
            FullRowSelect = true;
        }

        protected override void DefWndProc(ref Message m)
        {
            //Don't turn selected item's backcolor to gray when losing focus:
            if (m.Msg != 8)
            {
                base.DefWndProc(ref m);
            }
        }

        //protected override void OnItemSelectionChanged(ListViewItemSelectionChangedEventArgs e)
        //{
        //    if (e.Item.ForeColor == System.Drawing.Color.Gray)
        //    {
        //        e.Item.Selected = false;
        //        return;
        //    }
        //    base.OnItemSelectionChanged(e);
        //}

        //protected override void WndProc(ref Message m)
        //{    
        //    // Suppress mouse messages that are OUTSIDE of the items area
        //    if (m.Msg >= 0x201 && m.Msg <= 0x209)
        //    {
        //        Point pos = new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16);
        //        var hit = this.HitTest(pos);
        //        switch (hit.Location)
        //        {
        //            case ListViewHitTestLocations.AboveClientArea:
        //            case ListViewHitTestLocations.BelowClientArea:
        //            case ListViewHitTestLocations.LeftOfClientArea:
        //            case ListViewHitTestLocations.RightOfClientArea:
        //            case ListViewHitTestLocations.None:
        //                return;
        //        }
        //    }

        //    base.WndProc(ref m);
        //}

        protected override void OnDrawColumnHeader(DrawListViewColumnHeaderEventArgs e)
        {
            base.OnDrawColumnHeader(e);
            using (var backBrush = new SolidBrush(ThemedColors.MenuColor))
            {
                e.Graphics.FillRectangle(backBrush, e.Bounds);
            }

            TextRenderer.DrawText(e.Graphics, e.Header.Text, e.Font, e.Bounds, ThemedColors.MenuTextColor, Color.Transparent, TextFormatFlags.VerticalCenter);
            //e.Graphics.DrawString(e.Header.Text, e.Font, foreBrush, e.Bounds);
        }
        protected override void OnDrawItem(DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;

            base.OnDrawItem(e);
            //e.DrawBackground();
            //e.DrawFocusRectangle();
            if (e.Item.Focused && !e.Item.Selected)
            {
                e.Item.Selected = true;
                //using (var backBrush = new SolidBrush(ThemedColors.SelectionColor))
                //{
                //    e.Graphics.FillRectangle(backBrush, e.Bounds);
                //}
            }
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);

            try
            {
                Update();
            }
            catch
            {

            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            try
            {
                Start();
            }
            catch
            {

            }
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);

            try
            {
                if (this.Visible && this.FindForm().Visible)
                {
                    Start();
                }
            }
            catch
            {
            }
        }

        private void Start()
        {
            if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
            {
                RemoveScrollBar(this.Handle);
                DisposeImages();

                return;
            }

            AddScrollBar();
        }

        private void DisposeImages()
        {
            Vienna_ScrollHorzThumb.Dispose();
            Vienna_ScrollHorzShaft.Dispose();
            Vienna_ScrollHorzArrow.Dispose();
            Vienna_ScrollVertThumb.Dispose();
            Vienna_ScrollVertShaft.Dispose();
            Vienna_ScrollVertArrow.Dispose();
            Header.Dispose();
        }

        private void LoadImages()
        {
            DisposeImages();

            Vienna_ScrollHorzThumb = MainForm.mainFormRef.vienna_ScrollHorzThumb.Clone(new Rectangle(0,0,MainForm.mainFormRef.vienna_ScrollHorzThumb.Width, MainForm.mainFormRef.vienna_ScrollHorzThumb.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollHorzShaft = MainForm.mainFormRef.vienna_ScrollHorzShaft.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollHorzShaft.Width, MainForm.mainFormRef.vienna_ScrollHorzShaft.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollHorzArrow = MainForm.mainFormRef.vienna_ScrollHorzArrow.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollHorzArrow.Width, MainForm.mainFormRef.vienna_ScrollHorzArrow.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollVertThumb = MainForm.mainFormRef.vienna_ScrollVertThumb.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollVertThumb.Width, MainForm.mainFormRef.vienna_ScrollVertThumb.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollVertShaft = MainForm.mainFormRef.vienna_ScrollVertShaft.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollVertShaft.Width, MainForm.mainFormRef.vienna_ScrollVertShaft.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollVertArrow = MainForm.mainFormRef.vienna_ScrollVertArrow.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollVertArrow.Width, MainForm.mainFormRef.vienna_ScrollVertArrow.Height), PixelFormat.Format16bppRgb555);
            Header = new Bitmap(MainForm.mainFormRef.vienna_header);
        }

        /// <summary>
        /// Remove a specific control from the skin engine
        /// </summary>
        /// <param name="handle">Control handle</param>
        public void RemoveScrollBar(IntPtr handle)
        {
            if (handle != IntPtr.Zero)
            {
                Control ctl = Control.FromHandle(handle);

                if (ctl != null)
                {
                    _oScrollbarSkin[ctl.Handle].Dispose();
                    _oScrollbarSkin.Remove(ctl.Handle);
                }
            }
        }

        /// <summary>
        /// Add method for the ScrollBar control
        /// </summary>
        /// <param name="ct">Control type [ScrollBar]</param>
        /// <param name="track">Track image</param>
        /// <param name="arrow">Arrow image</param>
        /// <param name="thumb">Thumb image</param>
        /// <param name="orientation">ScrollbBar orientation</param>
        public void AddScrollBar()
        {
            if (_oScrollbarSkin == null)
                _oScrollbarSkin = new Dictionary<IntPtr, cListView>();

            var control = Control.FromHandle(this.Handle);
            if (control is listView)
            {
                if (_oScrollbarSkin.ContainsKey(control.Handle))
                {
                    RemoveScrollBar(control.Handle);
                }

                if (control.Visible)
                {
                    LoadImages();
                    _oScrollbarSkin.Add(control.Handle, new cListView(control.Handle, Header, Vienna_ScrollHorzShaft, Vienna_ScrollHorzArrow, Vienna_ScrollHorzThumb, Vienna_ScrollVertShaft, Vienna_ScrollVertArrow, Vienna_ScrollVertThumb, null));
                }

                control.Refresh();
            }
        }
    }
}
