﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ProGammaX
{
    public class treeView : TreeView
    {
        private Bitmap Vienna_ScrollHorzThumb = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollHorzShaft = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollHorzArrow = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollVertThumb = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollVertShaft = new Bitmap(1, 1);
        private Bitmap Vienna_ScrollVertArrow = new Bitmap(1, 1);
        private Bitmap Fader = new Bitmap(1, 1);
        private Dictionary<IntPtr, cTreeView> _oScrollbarSkin;

        public treeView()
        {
            this.DrawMode = TreeViewDrawMode.OwnerDrawText;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        // Override the drawMode of TreeView.
        protected override void OnDrawNode(DrawTreeNodeEventArgs e)
        {
            base.OnDrawNode(e);
            TreeNodeStates treeState = e.State;
            Font treeFont = e.Node.NodeFont ?? e.Node.TreeView.Font;

            // Colors.
            Color foreColor = e.Node.ForeColor;
            //string strDeselectedColor = @"#6B6E77", strSelectedColor = @"#94C7FC"; //@"#1ABEE8"; //@"#2FC0EE"; //@"#3A8FEA";
            Color selectedColor = ThemedColors.SelectionColor; //System.Drawing.ColorTranslator.FromHtml(strSelectedColor);
            Color deselectedColor = ThemedColors.WindowColor; //System.Drawing.ColorTranslator.FromHtml(strDeselectedColor);

            // New brush.
            SolidBrush selectedTreeBrush = new SolidBrush(selectedColor);
            SolidBrush deselectedTreeBrush = new SolidBrush(deselectedColor);

            // Set default font color.
            if (foreColor == Color.Empty)
                foreColor = e.Node.TreeView.ForeColor;



            //// Do your own logic to determine what overlay image you want to use

            //if (e.Node.ImageIndex != null)
            //{
            //    Image overlayImage = e.Node.TreeView.ImageList.Images[e.Node.ImageIndex];
            //        // you have to move the X value left a bit, 
            //        // otherwise it will draw over your node text
            //        // I'm also adjusting to move the overlay down a bit
            //        e.Graphics.DrawImage(overlayImage,
            //         e.Node.Bounds.X - 15, e.Node.Bounds.Y + 4);
            //}




            // Draw bounding box and fill.
            if (e.Node == e.Node.TreeView.SelectedNode)
            {
                // Use appropriate brush depending on if the tree has focus.
                if (this.Focused)
                {
                    foreColor = ThemedColors.SelectionForeColor;
                    e.Graphics.FillRectangle(selectedTreeBrush /*SystemBrushes.Highlight*/, e.Bounds);
                    ControlPaint.DrawFocusRectangle(e.Graphics, e.Bounds, foreColor, ThemedColors.WindowColor);
                    TextRenderer.DrawText(e.Graphics, e.Node.Text, treeFont, e.Bounds, foreColor, TextFormatFlags.GlyphOverhangPadding);
                }
                else
                {
                    foreColor = ThemedColors.ForeColor;
                    e.Graphics.FillRectangle(deselectedTreeBrush /*SystemBrushes.Highlight*/, e.Bounds);
                    ControlPaint.DrawFocusRectangle(e.Graphics, e.Bounds, foreColor, ThemedColors.WindowColor);
                    TextRenderer.DrawText(e.Graphics, e.Node.Text, treeFont, e.Bounds, foreColor, TextFormatFlags.GlyphOverhangPadding);
                }
            }
            else
            {
                // This is firing but is being over written, perhaps by the above?
                if ((e.State & TreeNodeStates.Hot) == TreeNodeStates.Hot)
                {
                    e.DrawDefault = true;
                    e.Graphics.FillRectangle(new SolidBrush(ThemedColors.WindowColor), e.Bounds);
                    TextRenderer.DrawText(e.Graphics, e.Node.Text, treeFont, e.Bounds, System.Drawing.Color.Blue, TextFormatFlags.GlyphOverhangPadding);
                }
                else
                {
                    e.Graphics.FillRectangle(new SolidBrush(ThemedColors.WindowColor), e.Bounds);
                    TextRenderer.DrawText(e.Graphics, e.Node.Text, treeFont, e.Bounds, foreColor, TextFormatFlags.GlyphOverhangPadding);
                }
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            try
            {
                Start();
            }
            catch
            {

            }
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);

            try
            {
                if (this.Visible && this.FindForm().Visible)
                {
                    Start();
                }
            }
            catch
            {
            }
        }

        private void Start()
        {
            if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
            {
                RemoveScrollBar(this.Handle);
                DisposeImages();

                return;
            }

            AddScrollBar();
        }

        private void DisposeImages()
        {
            Vienna_ScrollHorzThumb.Dispose();
            Vienna_ScrollHorzShaft.Dispose();
            Vienna_ScrollHorzArrow.Dispose();
            Vienna_ScrollVertThumb.Dispose();
            Vienna_ScrollVertShaft.Dispose();
            Vienna_ScrollVertArrow.Dispose();
        }

        private void LoadImages()
        {
            DisposeImages();

            Vienna_ScrollHorzThumb = MainForm.mainFormRef.vienna_ScrollHorzThumb.Clone(new Rectangle(0,0,MainForm.mainFormRef.vienna_ScrollHorzThumb.Width, MainForm.mainFormRef.vienna_ScrollHorzThumb.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollHorzShaft = MainForm.mainFormRef.vienna_ScrollHorzShaft.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollHorzShaft.Width, MainForm.mainFormRef.vienna_ScrollHorzShaft.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollHorzArrow = MainForm.mainFormRef.vienna_ScrollHorzArrow.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollHorzArrow.Width, MainForm.mainFormRef.vienna_ScrollHorzArrow.Height), PixelFormat.Format16bppRgb555);
           Vienna_ScrollVertThumb = MainForm.mainFormRef.vienna_ScrollVertThumb.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollVertThumb.Width, MainForm.mainFormRef.vienna_ScrollVertThumb.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollVertShaft = MainForm.mainFormRef.vienna_ScrollVertShaft.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollVertShaft.Width, MainForm.mainFormRef.vienna_ScrollVertShaft.Height), PixelFormat.Format16bppRgb555);
            Vienna_ScrollVertArrow = MainForm.mainFormRef.vienna_ScrollVertArrow.Clone(new Rectangle(0, 0, MainForm.mainFormRef.vienna_ScrollVertArrow.Width, MainForm.mainFormRef.vienna_ScrollVertArrow.Height), PixelFormat.Format16bppRgb555);
            Fader = null;
        }

        /// <summary>
        /// Remove a specific control from the skin engine
        /// </summary>
        /// <param name="handle">Control handle</param>
        public void RemoveScrollBar(IntPtr handle)
        {
            if (handle != IntPtr.Zero)
            {
                Control ctl = Control.FromHandle(handle);

                if (ctl != null)
                {
                    _oScrollbarSkin[ctl.Handle].Dispose();
                    _oScrollbarSkin.Remove(ctl.Handle);
                }
            }
        }

        /// <summary>
        /// Add method for the ScrollBar control
        /// </summary>
        /// <param name="ct">Control type [ScrollBar]</param>
        /// <param name="track">Track image</param>
        /// <param name="arrow">Arrow image</param>
        /// <param name="thumb">Thumb image</param>
        /// <param name="orientation">ScrollbBar orientation</param>
        public void AddScrollBar()
        {
            if (_oScrollbarSkin == null)
                _oScrollbarSkin = new Dictionary<IntPtr, cTreeView>();

            var control = Control.FromHandle(this.Handle);
            if (control is treeView)
            {
                if (_oScrollbarSkin.ContainsKey(control.Handle))
                {
                    RemoveScrollBar(control.Handle);
                }

                if (control.Visible)
                {
                    LoadImages();
                    _oScrollbarSkin.Add(control.Handle, new cTreeView(control.Handle, Vienna_ScrollHorzShaft, Vienna_ScrollHorzArrow, Vienna_ScrollHorzThumb, Vienna_ScrollVertShaft, Vienna_ScrollVertArrow, Vienna_ScrollVertThumb, Fader));
                }

                control.Refresh();
            }
        }
    }
}
