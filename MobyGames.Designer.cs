﻿namespace ProGammaX
{
    partial class MobyGames : myForm
    {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlContainer = new myPanel();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.OK_Button = new ProGammaX.myButton();
            this.Cancel_Button = new ProGammaX.myButton();
            this.TextBox_GetInfoFromMobyGames_Searchbox = new ProGammaX.textBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Button_SearchMobyGames = new ProGammaX.myButton();
            this.Label3 = new System.Windows.Forms.Label();
            this.TextBox_GetInfoFromMobyGames_Genre = new ProGammaX.textBox();
            this.TextBox_GetInfoFromMobyGames_Developer = new ProGammaX.textBox();
            this.TextBox_GetInfoFromMobyGames_Publisher = new ProGammaX.textBox();
            this.TextBox_GetInfoFromMobyGames_www = new ProGammaX.textBox();
            this.TextBox_GetInfoFromMobyGames_Comment = new ProGammaX.textBox();
            this.TextBox_GetInfoFromMobyGames_Year = new ProGammaX.textBox();
            this.CheckBox_GetInfoFromMobyGames_Genre = new ProGammaX.myCheckBox();
            this.CheckBox_GetInfoFromMobyGames_Developer = new ProGammaX.myCheckBox();
            this.CheckBox_GetInfoFromMobyGames_Publisher = new ProGammaX.myCheckBox();
            this.CheckBox_GetInfoFromMobyGames_www = new ProGammaX.myCheckBox();
            this.CheckBox_GetInfoFromMobyGames_Year = new ProGammaX.myCheckBox();
            this.CheckBox_GetInfoFromMobyGames_Comment = new ProGammaX.myCheckBox();
            this.CheckBox_GetInfoFromMobyGames_GameName = new ProGammaX.myCheckBox();
            this.ListView_GetInfoFromMobyGames_Results = new ProGammaX.listView();
            this.ColumnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Button_GetInfoFromMobyGames = new ProGammaX.myButton();
            this.TextBox_GetInfoFromMobyGames_GameName = new ProGammaX.textBox();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ComboBox_GetInfoFromMobyGames_Filter = new ProGammaX.myComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.TableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TableLayoutPanel1.ColumnCount = 2;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Controls.Add(this.OK_Button, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.Cancel_Button, 1, 0);
            this.TableLayoutPanel1.Location = new System.Drawing.Point(240, 360);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 1;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(146, 29);
            this.TableLayoutPanel1.TabIndex = 0;
            // 
            // OK_Button
            // 
            this.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OK_Button.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OK_Button.Location = new System.Drawing.Point(3, 3);
            this.OK_Button.Name = "OK_Button";
            this.OK_Button.Size = new System.Drawing.Size(67, 23);
            this.OK_Button.TabIndex = 2;
            this.OK_Button.Text = "Save";
            this.OK_Button.UseVisualStyleBackColor = true;
            this.OK_Button.Click += new System.EventHandler(this.OK_Button_Click);
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel_Button.Location = new System.Drawing.Point(76, 3);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(67, 23);
            this.Cancel_Button.TabIndex = 1;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.UseVisualStyleBackColor = true;
            this.Cancel_Button.Click += new System.EventHandler(this.Cancel_Button_Click);
            // 
            // TextBox_GetInfoFromMobyGames_Searchbox
            // 
            this.TextBox_GetInfoFromMobyGames_Searchbox.Location = new System.Drawing.Point(9, 36);
            this.TextBox_GetInfoFromMobyGames_Searchbox.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_GetInfoFromMobyGames_Searchbox.Name = "TextBox_GetInfoFromMobyGames_Searchbox";
            this.TextBox_GetInfoFromMobyGames_Searchbox.Size = new System.Drawing.Size(244, 20);
            this.TextBox_GetInfoFromMobyGames_Searchbox.TabIndex = 6;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(7, 20);
            this.Label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(97, 13);
            this.Label2.TabIndex = 5;
            this.Label2.Text = "Search for a game:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(7, 74);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(51, 13);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "Result(s):";
            // 
            // Button_SearchMobyGames
            // 
            this.Button_SearchMobyGames.Location = new System.Drawing.Point(257, 36);
            this.Button_SearchMobyGames.Margin = new System.Windows.Forms.Padding(2);
            this.Button_SearchMobyGames.Name = "Button_SearchMobyGames";
            this.Button_SearchMobyGames.Size = new System.Drawing.Size(59, 19);
            this.Button_SearchMobyGames.TabIndex = 0;
            this.Button_SearchMobyGames.Text = "Search";
            this.Button_SearchMobyGames.UseVisualStyleBackColor = true;
            this.Button_SearchMobyGames.Click += new System.EventHandler(this.Button_SearchMobyGames_Click);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(348, 22);
            this.Label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(129, 13);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "Downloaded informations:";
            // 
            // TextBox_GetInfoFromMobyGames_Genre
            // 
            this.TextBox_GetInfoFromMobyGames_Genre.Location = new System.Drawing.Point(365, 106);
            this.TextBox_GetInfoFromMobyGames_Genre.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_GetInfoFromMobyGames_Genre.Name = "TextBox_GetInfoFromMobyGames_Genre";
            this.TextBox_GetInfoFromMobyGames_Genre.Size = new System.Drawing.Size(294, 20);
            this.TextBox_GetInfoFromMobyGames_Genre.TabIndex = 9;
            // 
            // TextBox_GetInfoFromMobyGames_Developer
            // 
            this.TextBox_GetInfoFromMobyGames_Developer.Location = new System.Drawing.Point(365, 151);
            this.TextBox_GetInfoFromMobyGames_Developer.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_GetInfoFromMobyGames_Developer.Name = "TextBox_GetInfoFromMobyGames_Developer";
            this.TextBox_GetInfoFromMobyGames_Developer.Size = new System.Drawing.Size(294, 20);
            this.TextBox_GetInfoFromMobyGames_Developer.TabIndex = 9;
            // 
            // TextBox_GetInfoFromMobyGames_Publisher
            // 
            this.TextBox_GetInfoFromMobyGames_Publisher.Location = new System.Drawing.Point(365, 196);
            this.TextBox_GetInfoFromMobyGames_Publisher.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_GetInfoFromMobyGames_Publisher.Name = "TextBox_GetInfoFromMobyGames_Publisher";
            this.TextBox_GetInfoFromMobyGames_Publisher.Size = new System.Drawing.Size(294, 20);
            this.TextBox_GetInfoFromMobyGames_Publisher.TabIndex = 9;
            // 
            // TextBox_GetInfoFromMobyGames_www
            // 
            this.TextBox_GetInfoFromMobyGames_www.Location = new System.Drawing.Point(365, 241);
            this.TextBox_GetInfoFromMobyGames_www.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_GetInfoFromMobyGames_www.Name = "TextBox_GetInfoFromMobyGames_www";
            this.TextBox_GetInfoFromMobyGames_www.Size = new System.Drawing.Size(294, 20);
            this.TextBox_GetInfoFromMobyGames_www.TabIndex = 9;
            // 
            // TextBox_GetInfoFromMobyGames_Comment
            // 
            this.TextBox_GetInfoFromMobyGames_Comment.Location = new System.Drawing.Point(365, 331);
            this.TextBox_GetInfoFromMobyGames_Comment.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_GetInfoFromMobyGames_Comment.Multiline = true;
            this.TextBox_GetInfoFromMobyGames_Comment.Name = "TextBox_GetInfoFromMobyGames_Comment";
            this.TextBox_GetInfoFromMobyGames_Comment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBox_GetInfoFromMobyGames_Comment.Size = new System.Drawing.Size(294, 56);
            this.TextBox_GetInfoFromMobyGames_Comment.TabIndex = 9;
            // 
            // TextBox_GetInfoFromMobyGames_Year
            // 
            this.TextBox_GetInfoFromMobyGames_Year.Location = new System.Drawing.Point(365, 286);
            this.TextBox_GetInfoFromMobyGames_Year.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_GetInfoFromMobyGames_Year.Name = "TextBox_GetInfoFromMobyGames_Year";
            this.TextBox_GetInfoFromMobyGames_Year.Size = new System.Drawing.Size(294, 20);
            this.TextBox_GetInfoFromMobyGames_Year.TabIndex = 11;
            // 
            // CheckBox_GetInfoFromMobyGames_Genre
            // 
            this.CheckBox_GetInfoFromMobyGames_Genre.AutoSize = true;
            this.CheckBox_GetInfoFromMobyGames_Genre.Checked = true;
            this.CheckBox_GetInfoFromMobyGames_Genre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_GetInfoFromMobyGames_Genre.Location = new System.Drawing.Point(350, 85);
            this.CheckBox_GetInfoFromMobyGames_Genre.Margin = new System.Windows.Forms.Padding(2);
            this.CheckBox_GetInfoFromMobyGames_Genre.Name = "CheckBox_GetInfoFromMobyGames_Genre";
            this.CheckBox_GetInfoFromMobyGames_Genre.Size = new System.Drawing.Size(58, 17);
            this.CheckBox_GetInfoFromMobyGames_Genre.TabIndex = 12;
            this.CheckBox_GetInfoFromMobyGames_Genre.Text = "Genre:";
            this.CheckBox_GetInfoFromMobyGames_Genre.UseVisualStyleBackColor = true;
            // 
            // CheckBox_GetInfoFromMobyGames_Developer
            // 
            this.CheckBox_GetInfoFromMobyGames_Developer.AutoSize = true;
            this.CheckBox_GetInfoFromMobyGames_Developer.Checked = true;
            this.CheckBox_GetInfoFromMobyGames_Developer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_GetInfoFromMobyGames_Developer.Location = new System.Drawing.Point(350, 130);
            this.CheckBox_GetInfoFromMobyGames_Developer.Margin = new System.Windows.Forms.Padding(2);
            this.CheckBox_GetInfoFromMobyGames_Developer.Name = "CheckBox_GetInfoFromMobyGames_Developer";
            this.CheckBox_GetInfoFromMobyGames_Developer.Size = new System.Drawing.Size(78, 17);
            this.CheckBox_GetInfoFromMobyGames_Developer.TabIndex = 12;
            this.CheckBox_GetInfoFromMobyGames_Developer.Text = "Developer:";
            this.CheckBox_GetInfoFromMobyGames_Developer.UseVisualStyleBackColor = true;
            // 
            // CheckBox_GetInfoFromMobyGames_Publisher
            // 
            this.CheckBox_GetInfoFromMobyGames_Publisher.AutoSize = true;
            this.CheckBox_GetInfoFromMobyGames_Publisher.Checked = true;
            this.CheckBox_GetInfoFromMobyGames_Publisher.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_GetInfoFromMobyGames_Publisher.Location = new System.Drawing.Point(351, 175);
            this.CheckBox_GetInfoFromMobyGames_Publisher.Margin = new System.Windows.Forms.Padding(2);
            this.CheckBox_GetInfoFromMobyGames_Publisher.Name = "CheckBox_GetInfoFromMobyGames_Publisher";
            this.CheckBox_GetInfoFromMobyGames_Publisher.Size = new System.Drawing.Size(69, 17);
            this.CheckBox_GetInfoFromMobyGames_Publisher.TabIndex = 12;
            this.CheckBox_GetInfoFromMobyGames_Publisher.Text = "Publisher";
            this.CheckBox_GetInfoFromMobyGames_Publisher.UseVisualStyleBackColor = true;
            // 
            // CheckBox_GetInfoFromMobyGames_www
            // 
            this.CheckBox_GetInfoFromMobyGames_www.AutoSize = true;
            this.CheckBox_GetInfoFromMobyGames_www.Checked = true;
            this.CheckBox_GetInfoFromMobyGames_www.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_GetInfoFromMobyGames_www.Location = new System.Drawing.Point(350, 220);
            this.CheckBox_GetInfoFromMobyGames_www.Margin = new System.Windows.Forms.Padding(2);
            this.CheckBox_GetInfoFromMobyGames_www.Name = "CheckBox_GetInfoFromMobyGames_www";
            this.CheckBox_GetInfoFromMobyGames_www.Size = new System.Drawing.Size(53, 17);
            this.CheckBox_GetInfoFromMobyGames_www.TabIndex = 12;
            this.CheckBox_GetInfoFromMobyGames_www.Text = "www:";
            this.CheckBox_GetInfoFromMobyGames_www.UseVisualStyleBackColor = true;
            // 
            // CheckBox_GetInfoFromMobyGames_Year
            // 
            this.CheckBox_GetInfoFromMobyGames_Year.AutoSize = true;
            this.CheckBox_GetInfoFromMobyGames_Year.Checked = true;
            this.CheckBox_GetInfoFromMobyGames_Year.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_GetInfoFromMobyGames_Year.Location = new System.Drawing.Point(350, 265);
            this.CheckBox_GetInfoFromMobyGames_Year.Margin = new System.Windows.Forms.Padding(2);
            this.CheckBox_GetInfoFromMobyGames_Year.Name = "CheckBox_GetInfoFromMobyGames_Year";
            this.CheckBox_GetInfoFromMobyGames_Year.Size = new System.Drawing.Size(51, 17);
            this.CheckBox_GetInfoFromMobyGames_Year.TabIndex = 12;
            this.CheckBox_GetInfoFromMobyGames_Year.Text = "Year:";
            this.CheckBox_GetInfoFromMobyGames_Year.UseVisualStyleBackColor = true;
            // 
            // CheckBox_GetInfoFromMobyGames_Comment
            // 
            this.CheckBox_GetInfoFromMobyGames_Comment.AutoSize = true;
            this.CheckBox_GetInfoFromMobyGames_Comment.Checked = true;
            this.CheckBox_GetInfoFromMobyGames_Comment.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_GetInfoFromMobyGames_Comment.Location = new System.Drawing.Point(351, 310);
            this.CheckBox_GetInfoFromMobyGames_Comment.Margin = new System.Windows.Forms.Padding(2);
            this.CheckBox_GetInfoFromMobyGames_Comment.Name = "CheckBox_GetInfoFromMobyGames_Comment";
            this.CheckBox_GetInfoFromMobyGames_Comment.Size = new System.Drawing.Size(73, 17);
            this.CheckBox_GetInfoFromMobyGames_Comment.TabIndex = 12;
            this.CheckBox_GetInfoFromMobyGames_Comment.Text = "Comment:";
            this.CheckBox_GetInfoFromMobyGames_Comment.UseVisualStyleBackColor = true;
            // 
            // CheckBox_GetInfoFromMobyGames_GameName
            // 
            this.CheckBox_GetInfoFromMobyGames_GameName.AutoSize = true;
            this.CheckBox_GetInfoFromMobyGames_GameName.Checked = true;
            this.CheckBox_GetInfoFromMobyGames_GameName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_GetInfoFromMobyGames_GameName.Location = new System.Drawing.Point(350, 40);
            this.CheckBox_GetInfoFromMobyGames_GameName.Margin = new System.Windows.Forms.Padding(2);
            this.CheckBox_GetInfoFromMobyGames_GameName.Name = "CheckBox_GetInfoFromMobyGames_GameName";
            this.CheckBox_GetInfoFromMobyGames_GameName.Size = new System.Drawing.Size(86, 17);
            this.CheckBox_GetInfoFromMobyGames_GameName.TabIndex = 12;
            this.CheckBox_GetInfoFromMobyGames_GameName.Text = "Game name:";
            this.CheckBox_GetInfoFromMobyGames_GameName.UseVisualStyleBackColor = true;
            // 
            // ListView_GetInfoFromMobyGames_Results
            // 
            this.ListView_GetInfoFromMobyGames_Results.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader1});
            this.ListView_GetInfoFromMobyGames_Results.HideSelection = false;
            this.ListView_GetInfoFromMobyGames_Results.Location = new System.Drawing.Point(9, 90);
            this.ListView_GetInfoFromMobyGames_Results.Margin = new System.Windows.Forms.Padding(2);
            this.ListView_GetInfoFromMobyGames_Results.MultiSelect = false;
            this.ListView_GetInfoFromMobyGames_Results.Name = "ListView_GetInfoFromMobyGames_Results";
            this.ListView_GetInfoFromMobyGames_Results.OwnerDraw = true;
            this.ListView_GetInfoFromMobyGames_Results.ShowItemToolTips = true;
            this.ListView_GetInfoFromMobyGames_Results.Size = new System.Drawing.Size(307, 238);
            this.ListView_GetInfoFromMobyGames_Results.TabIndex = 14;
            this.ListView_GetInfoFromMobyGames_Results.UseCompatibleStateImageBehavior = false;
            this.ListView_GetInfoFromMobyGames_Results.View = System.Windows.Forms.View.Details;
            // 
            // ColumnHeader1
            // 
            this.ColumnHeader1.Text = "Game name";
            this.ColumnHeader1.Width = 257;
            // 
            // Button_GetInfoFromMobyGames
            // 
            this.Button_GetInfoFromMobyGames.Enabled = false;
            this.Button_GetInfoFromMobyGames.Location = new System.Drawing.Point(9, 332);
            this.Button_GetInfoFromMobyGames.Margin = new System.Windows.Forms.Padding(2);
            this.Button_GetInfoFromMobyGames.Name = "Button_GetInfoFromMobyGames";
            this.Button_GetInfoFromMobyGames.Size = new System.Drawing.Size(307, 55);
            this.Button_GetInfoFromMobyGames.TabIndex = 15;
            this.Button_GetInfoFromMobyGames.Text = "Get info";
            this.Button_GetInfoFromMobyGames.UseVisualStyleBackColor = true;
            this.Button_GetInfoFromMobyGames.Click += new System.EventHandler(this.Button_GetInfoFromMobyGames_Click);
            // 
            // TextBox_GetInfoFromMobyGames_GameName
            // 
            this.TextBox_GetInfoFromMobyGames_GameName.Location = new System.Drawing.Point(365, 61);
            this.TextBox_GetInfoFromMobyGames_GameName.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_GetInfoFromMobyGames_GameName.Name = "TextBox_GetInfoFromMobyGames_GameName";
            this.TextBox_GetInfoFromMobyGames_GameName.Size = new System.Drawing.Size(294, 20);
            this.TextBox_GetInfoFromMobyGames_GameName.TabIndex = 9;
            // 
            // ToolTip1
            // 
            this.ToolTip1.AutomaticDelay = 1500;
            this.ToolTip1.AutoPopDelay = 5000;
            this.ToolTip1.InitialDelay = 1500;
            this.ToolTip1.ReshowDelay = 300;
            this.ToolTip1.ToolTipTitle = "URL:";
            // 
            // ComboBox_GetInfoFromMobyGames_Filter
            // 
            this.ComboBox_GetInfoFromMobyGames_Filter.BackColor = System.Drawing.Color.Snow;
            this.ComboBox_GetInfoFromMobyGames_Filter.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ComboBox_GetInfoFromMobyGames_Filter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_GetInfoFromMobyGames_Filter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(44)))));
            this.ComboBox_GetInfoFromMobyGames_Filter.FormattingEnabled = true;
            this.ComboBox_GetInfoFromMobyGames_Filter.Items.AddRange(new object[] {
            "All Games",
            "DOS",
            "WINDOWS"});
            this.ComboBox_GetInfoFromMobyGames_Filter.Location = new System.Drawing.Point(169, 63);
            this.ComboBox_GetInfoFromMobyGames_Filter.Margin = new System.Windows.Forms.Padding(2);
            this.ComboBox_GetInfoFromMobyGames_Filter.Name = "ComboBox_GetInfoFromMobyGames_Filter";
            this.ComboBox_GetInfoFromMobyGames_Filter.Size = new System.Drawing.Size(147, 21);
            this.ComboBox_GetInfoFromMobyGames_Filter.TabIndex = 16;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(133, 66);
            this.Label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(32, 13);
            this.Label4.TabIndex = 4;
            this.Label4.Text = "Filter:";
            // 
            // plnContainer
            // 
            this.pnlContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(400, 400);
            this.pnlContainer.TabIndex = 11;
            this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer.Controls.Add(this.ComboBox_GetInfoFromMobyGames_Filter);
            this.pnlContainer.Controls.Add(this.Button_GetInfoFromMobyGames);
            this.pnlContainer.Controls.Add(this.ListView_GetInfoFromMobyGames_Results);
            this.pnlContainer.Controls.Add(this.CheckBox_GetInfoFromMobyGames_Comment);
            this.pnlContainer.Controls.Add(this.CheckBox_GetInfoFromMobyGames_Year);
            this.pnlContainer.Controls.Add(this.CheckBox_GetInfoFromMobyGames_www);
            this.pnlContainer.Controls.Add(this.CheckBox_GetInfoFromMobyGames_Publisher);
            this.pnlContainer.Controls.Add(this.CheckBox_GetInfoFromMobyGames_Developer);
            this.pnlContainer.Controls.Add(this.CheckBox_GetInfoFromMobyGames_GameName);
            this.pnlContainer.Controls.Add(this.CheckBox_GetInfoFromMobyGames_Genre);
            this.pnlContainer.Controls.Add(this.TextBox_GetInfoFromMobyGames_Year);
            this.pnlContainer.Controls.Add(this.TextBox_GetInfoFromMobyGames_Comment);
            this.pnlContainer.Controls.Add(this.TextBox_GetInfoFromMobyGames_www);
            this.pnlContainer.Controls.Add(this.TextBox_GetInfoFromMobyGames_Publisher);
            this.pnlContainer.Controls.Add(this.TextBox_GetInfoFromMobyGames_Developer);
            this.pnlContainer.Controls.Add(this.TextBox_GetInfoFromMobyGames_GameName);
            this.pnlContainer.Controls.Add(this.TextBox_GetInfoFromMobyGames_Genre);
            this.pnlContainer.Controls.Add(this.Label3);
            this.pnlContainer.Controls.Add(this.Button_SearchMobyGames);
            this.pnlContainer.Controls.Add(this.TextBox_GetInfoFromMobyGames_Searchbox);
            this.pnlContainer.Controls.Add(this.Label2);
            this.pnlContainer.Controls.Add(this.Label4);
            this.pnlContainer.Controls.Add(this.Label1);
            this.pnlContainer.Controls.Add(this.TableLayoutPanel1);
            // 
            // MobyGames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel_Button;
            this.ClientSize = new System.Drawing.Size(695, 484);
            this.Controls.Add(this.pnlContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MobyGames";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Get info from MobyGames.com";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal myButton Cancel_Button;
        internal textBox TextBox_GetInfoFromMobyGames_Searchbox;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal myButton Button_SearchMobyGames;
        internal System.Windows.Forms.Label Label3;
        internal textBox TextBox_GetInfoFromMobyGames_Genre;
        internal textBox TextBox_GetInfoFromMobyGames_Developer;
        internal textBox TextBox_GetInfoFromMobyGames_Publisher;
        internal textBox TextBox_GetInfoFromMobyGames_www;
        internal textBox TextBox_GetInfoFromMobyGames_Comment;
        internal textBox TextBox_GetInfoFromMobyGames_Year;
        internal myCheckBox CheckBox_GetInfoFromMobyGames_Genre;
        internal myCheckBox CheckBox_GetInfoFromMobyGames_Developer;
        internal myCheckBox CheckBox_GetInfoFromMobyGames_Publisher;
        internal myCheckBox CheckBox_GetInfoFromMobyGames_www;
        internal myCheckBox CheckBox_GetInfoFromMobyGames_Year;
        internal myCheckBox CheckBox_GetInfoFromMobyGames_Comment;
        internal myCheckBox CheckBox_GetInfoFromMobyGames_GameName;
        internal listView ListView_GetInfoFromMobyGames_Results;
        internal System.Windows.Forms.ColumnHeader ColumnHeader1;
        internal myButton Button_GetInfoFromMobyGames;
        internal textBox TextBox_GetInfoFromMobyGames_GameName;
        internal myButton OK_Button;
        internal System.Windows.Forms.ToolTip ToolTip1;
        internal myComboBox ComboBox_GetInfoFromMobyGames_Filter;
        internal myPanel pnlContainer;
        internal System.Windows.Forms.Label Label4;
    }
}