﻿namespace ProGammaX
{
    partial class Dialog_CreateShortcut : myForm
    {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            //this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.OK_Button = new myButton();
            this.Cancel_Button = new myButton();
            this.CheckBox1 = new myCheckBox();
            this.CheckBox2 = new myCheckBox();
            this.TextBox2 = new textBox();
            this.pnlContainer = new myPanel();
            this.Label2 = new System.Windows.Forms.Label();
            //this.TableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // plnContainer
            // 
            this.pnlContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(580, 140);
            this.pnlContainer.TabIndex = 11;
            this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer.Controls.Add(this.Label2);
            this.pnlContainer.Controls.Add(this.TextBox2);
            this.pnlContainer.Controls.Add(this.CheckBox2);
            this.pnlContainer.Controls.Add(this.CheckBox1);
            this.pnlContainer.Controls.Add(this.OK_Button);
            this.pnlContainer.Controls.Add(this.Cancel_Button);
            this.pnlContainer.Controls.SetChildIndex(this.Label2, 0);
            this.pnlContainer.Controls.SetChildIndex(this.TextBox2, 1);
            this.pnlContainer.Controls.SetChildIndex(this.CheckBox2, 2);
            this.pnlContainer.Controls.SetChildIndex(this.CheckBox1, 3);
            this.pnlContainer.Controls.SetChildIndex(this.OK_Button, 4);
            this.pnlContainer.Controls.SetChildIndex(this.Cancel_Button, 5);
            //// 
            //// TableLayoutPanel1
            //// 
            //this.TableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            //this.TableLayoutPanel1.ColumnCount = 2;
            //this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            //this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            //this.TableLayoutPanel1.Controls.Add(this.OK_Button, 0, 0);
            //this.TableLayoutPanel1.Controls.Add(this.Cancel_Button, 1, 0);
            //this.TableLayoutPanel1.Location = new System.Drawing.Point(369, 181);
            //this.TableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            //this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            //this.TableLayoutPanel1.RowCount = 1;
            //this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            //this.TableLayoutPanel1.Size = new System.Drawing.Size(195, 36);
            //this.TableLayoutPanel1.TabIndex = 0;
            // 
            // OK_Button
            // 
            this.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OK_Button.Location = new System.Drawing.Point(366, 140);
            this.OK_Button.Margin = new System.Windows.Forms.Padding(4);
            this.OK_Button.Name = "OK_Button";
            this.OK_Button.Size = new System.Drawing.Size(89, 28);
            this.OK_Button.TabIndex = 0;
            this.OK_Button.Text = "OK";
            this.OK_Button.Click += new System.EventHandler(this.OK_Button_Click);
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel_Button.Location = new System.Drawing.Point(460, 140);
            this.Cancel_Button.Margin = new System.Windows.Forms.Padding(4);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(89, 28);
            this.Cancel_Button.TabIndex = 1;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.Click += new System.EventHandler(this.Cancel_Button_Click);
            // 
            // CheckBox1
            // 
            this.CheckBox1.AutoSize = true;
            this.CheckBox1.Checked = true;
            this.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox1.Location = new System.Drawing.Point(75, 39);
            this.CheckBox1.Margin = new System.Windows.Forms.Padding(4);
            this.CheckBox1.Name = "CheckBox1";
            this.CheckBox1.Size = new System.Drawing.Size(400, 21);
            this.CheckBox1.TabIndex = 1;
            this.CheckBox1.Text = "Create on the desktop";
            this.CheckBox1.UseVisualStyleBackColor = true;
            // 
            // CheckBox2
            // 
            this.CheckBox2.AutoSize = true;
            this.CheckBox2.Location = new System.Drawing.Point(75, 69);
            this.CheckBox2.Margin = new System.Windows.Forms.Padding(4);
            this.CheckBox2.Name = "CheckBox2";
            this.CheckBox2.Size = new System.Drawing.Size(400, 21);
            this.CheckBox2.TabIndex = 1;
            this.CheckBox2.Text = "Create in start menu";
            this.CheckBox2.UseVisualStyleBackColor = true;
            this.CheckBox2.CheckedChanged += new System.EventHandler(this.CheckBox2_CheckedChanged);
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(75, 128);
            this.TextBox2.Margin = new System.Windows.Forms.Padding(4);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Size = new System.Drawing.Size(425, 22);
            this.TextBox2.TabIndex = 2;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(75, 108);
            this.Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(113, 17);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "Programs folder:";
            // 
            // Dialog6_CreateShortcut
            // 
            this.AcceptButton = this.OK_Button;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel_Button;
            this.ClientSize = new System.Drawing.Size(580, 280);
            this.Controls.Add(pnlContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Dialog6_CreateShortcut";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Create shortcut";
            //this.TableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal myButton OK_Button;
        internal myButton Cancel_Button;
        internal myCheckBox CheckBox1;
        internal myCheckBox CheckBox2;
        internal textBox TextBox2;
        internal myPanel pnlContainer;
        internal System.Windows.Forms.Label Label2;
    }
}