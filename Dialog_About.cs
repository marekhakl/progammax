﻿namespace ProGammaX
{
    using System;
    using System.Windows.Forms;

    public partial class Dialog_About
    {


        private static Dialog_About Dialog_AboutRef;
        public static Dialog_About dialog_AboutRef
        {
        get
            {
            return Dialog_AboutRef;
            }
        }
    public Dialog_About() : base (true, false, true)
        {

            InitializeComponent();

            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);

            Dialog_AboutRef = this;


            LinkLabel1.LinkColor = ThemedColors.LabelColor;
            LinkLabel2.LinkColor = ThemedColors.LabelColor;

           
            this.OK_Button.Click += OK_Button_Click;
            this.LinkLabel1.Click += LinkLabel1_Click;
            this.LinkLabel2.Click += LinkLabel2_Click;
            this.Load += Dialog_About_Load;
        }

        #region Methods

        // ERROR: Handles clauses are not supported in C#
        //private void Cancel_Button_Click(System.Object sender, System.EventArgs e)
        //{
        //    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        //    this.Close();
        //}

        // ERROR: Handles clauses are not supported in C#
        private void Dialog_About_Load(System.Object sender, System.EventArgs e)
        {


            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(asm.Location);
            Label_Version.Text = System.String.Format(MainForm.StringToBeTranslated[170], fvi.FileMajorPart, fvi.FileMinorPart, fvi.FileBuildPart);
            

            //System.String.Format("{0}.{1}", fvi.ProductMajorPart, fvi.ProductMinorPart);

            //Label_Version.Text = Module_Translation.stringToBeTranslated[170];
            // "Version {0}.{1:00}.{2}"
            //Label_Version.Text = "Version {0}.{1:00}"
             
            //Label_Version.Text = System.String.Format(Label_Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)
        }

        //Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        //    Select Case Me.PictureBox1.Size.Width
        //        Case 72
        //            Me.PictureBox1.Image = Nothing
        //            Me.PictureBox1.Size = New Size(80, 80)
        //            Me.PictureBox1.BackgroundImage = My.Resources.mak7b
        //        Case 80
        //            Me.PictureBox1.Image = My.Resources.Profile11
        //            Me.PictureBox1.Size = New Size(72, 80)
        //            Me.PictureBox1.BackgroundImage = Nothing
        //    End Select
        //End Sub
        // ERROR: Handles clauses are not supported in C#
    private void LinkLabel1_Click(System.Object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(this.LinkLabel1.Text);
            }
            catch
            {
            }
        }

        // ERROR: Handles clauses are not supported in C#
    private void LinkLabel2_Click(System.Object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("mailto:" + this.LinkLabel2.Text);
            }
            catch
            {
            }
        }

        // ERROR: Handles clauses are not supported in C#
        private void OK_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        #endregion Methods
    }
}