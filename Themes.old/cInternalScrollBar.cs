﻿namespace ProGammaX
{
    #region Directives
    using System;
    using System.Windows.Forms;
    using System.Runtime.InteropServices;
    using System.Drawing;
    using System.Text;
    using System.Reflection;
    using System.Threading;
    #endregion

    public class cInternalScrollBar : NativeWindow, IDisposable
    {
        #region Constants
        // style
        private const int GWL_STYLE = (-16);
        private const int GWL_EXSTYLE = (-20);
        private const int WS_EX_TOPMOST = 0x8;
        private const int WS_EX_TOOLWINDOW = 0x80;
        private const int WS_CHILD = 0x40000000;
        private const int WS_OVERLAPPED = 0x0;
        private const int WS_CLIPSIBLINGS = 0x4000000;
        private const int WS_VISIBLE = 0x10000000;
        private const int WS_HSCROLL = 0x100000;
        private const int WS_VSCROLL = 0x200000;
        private const int SS_OWNERDRAW = 0xD;
        // showwindow
        private const int SW_HIDE = 0x0;
        private const int SW_NORMAL = 0x1;
        // size/move
        private const uint SWP_NOSIZE = 0x0001;
        private const uint SWP_NOMOVE = 0x0002;
        private const uint SWP_NOZORDER = 0x0004;
        private const uint SWP_NOREDRAW = 0x0008;
        private const uint SWP_NOACTIVATE = 0x0010;
        private const uint SWP_FRAMECHANGED = 0x0020;
        private const uint SWP_SHOWWINDOW = 0x0040;
        private const uint SWP_HIDEWINDOW = 0x0080;
        private const uint SWP_NOCOPYBITS = 0x0100;
        private const uint SWP_NOOWNERZORDER = 0x0200;
        private const uint SWP_NOSENDCHANGING = 0x0400;
        // setwindowpos
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        // scroll messages
        private const int WM_HSCROLL = 0x114;
        private const int WM_VSCROLL = 0x115;
        private const int SB_LINEUP = 0;
        private const int SB_LINEDOWN = 1;
        private const int SB_LINELEFT = 0;
        private const int SB_LINERIGHT = 1;
        private const int SB_PAGEUP = 2;
        private const int SB_PAGEDOWN = 3;
        private const int SB_PAGELEFT = 2;
        private const int SB_PAGERIGHT = 3;
        // mouse buttons
        private const int VK_LBUTTON = 0x1;
        private const int VK_RBUTTON = 0x2;
        // redraw
        private const int RDW_INVALIDATE = 0x0001;
        private const int RDW_INTERNALPAINT = 0x0002;
        private const int RDW_ERASE = 0x0004;
        private const int RDW_VALIDATE = 0x0008;
        private const int RDW_NOINTERNALPAINT = 0x0010;
        private const int RDW_NOERASE = 0x0020;
        private const int RDW_NOCHILDREN = 0x0040;
        private const int RDW_ALLCHILDREN = 0x0080;
        private const int RDW_UPDATENOW = 0x0100;
        private const int RDW_ERASENOW = 0x0200;
        private const int RDW_FRAME = 0x0400;
        private const int RDW_NOFRAME = 0x0800;
        // scroll bar messages
        private const int SB_HORZ = 0x0;
        private const int SB_VERT = 0x1;
        private const int SBM_SETPOS = 0x00E0;
        private const int SBM_GETPOS = 0x00E1;
        private const int SBM_SETRANGE = 0x00E2;
        private const int SBM_SETRANGEREDRAW = 0x00E6;
        private const int SBM_GETRANGE = 0x00E3;
        private const int SBM_ENABLE_ARROWS = 0x00E4;
        private const int SBM_SETSCROLLINFO = 0x00E9;
        private const int SBM_GETSCROLLINFO = 0x00EA;
        private const int SBM_GETSCROLLBARINFO = 0x00EB;
        private const int SIF_RANGE = 0x0001;
        private const int SIF_PAGE = 0x0002;
        private const int SIF_POS = 0x0004;
        private const int SIF_DISABLENOSCROLL = 0x0008;
        private const int SIF_TRACKPOS = 0x0010;
        private const int SIF_ALL = (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS);
        // scrollbar states
        private const int STATE_SYSTEM_INVISIBLE = 0x00008000;
        private const int STATE_SYSTEM_OFFSCREEN = 0x00010000;
        private const int STATE_SYSTEM_PRESSED = 0x00000008;
        private const int STATE_SYSTEM_UNAVAILABLE = 0x00000001;
        private const uint OBJID_HSCROLL = 0xFFFFFFFA;
        private const uint OBJID_VSCROLL = 0xFFFFFFFB;
        private const uint OBJID_CLIENT = 0xFFFFFFFC;
        // window messages
        private const int WM_PAINT = 0xF;
        private const int WM_NCPAINT = 0x85;
        private const int WM_NCMOUSEMOVE = 0xA0;
        private const int WM_MOUSEMOVE = 0x200;
        private const int WM_MOUSELEAVE = 0x2A3;
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_LBUTTONUP = 0x202;
        private const int WM_LBUTTONDBLCLK = 0x203;
        private const int WM_RBUTTONDOWN = 0x204;
        private const int WM_RBUTTONUP = 0x205;
        private const int WM_RBUTTONDBLCLK = 0x206;
        private const int WM_MBUTTONDOWN = 0x207;
        private const int WM_MBUTTONUP = 0x208;
        private const int WM_MBUTTONDBLCLK = 0x209;
        private const int WM_MOUSEWHEEL = 0x20A;
        private const int WM_MOUSEHWHEEL = 0x020E;
        private const int WM_STYLECHANGED = 0x7D;
        private const int WM_SIZE = 0x5;
        private const int WM_MOVE = 0x3;
        // message handler
        private static IntPtr MSG_HANDLED = new IntPtr(1);
        #endregion

        #region Enums
        private enum SB_HITEST : int
        {
            offControl = 0,
            topArrow,
            bottomArrow,
            leftArrow,
            rightArrow,
            button,
            track
        }
        public enum TernaryRasterOperations : uint
        {
            SRCCOPY = 0x00CC0020,
            SRCPAINT = 0x00EE0086,
            SRCAND = 0x008800C6,
            SRCINVERT = 0x00660046,
            SRCERASE = 0x00440328,
            NOTSRCCOPY = 0x00330008,
            NOTSRCERASE = 0x001100A6,
            MERGECOPY = 0x00C000CA,
            MERGEPAINT = 0x00BB0226,
            PATCOPY = 0x00F00021,
            PATPAINT = 0x00FB0A09,
            PATINVERT = 0x005A0049,
            DSTINVERT = 0x00550009,
            BLACKNESS = 0x00000042,
            WHITENESS = 0x00FF0062,
            CAPTUREBLT = 0x40000000 //only if WinVer >= 5.0.0 (see wingdi.h)
        }
        private enum SYSTEM_METRICS : int
        {
            SM_CXSCREEN = 0,
            SM_CYSCREEN = 1,
            SM_CXVSCROLL = 2,
            SM_CYHSCROLL = 3,
            SM_CYCAPTION = 4,
            SM_CXBORDER = 5,
            SM_CYBORDER = 6,
            SM_CYVTHUMB = 9,
            SM_CXHTHUMB = 10,
            SM_CXICON = 11,
            SM_CYICON = 12,
            SM_CXCURSOR = 13,
            SM_CYCURSOR = 14,
            SM_CYMENU = 15,
            SM_CXFULLSCREEN = 16,
            SM_CYFULLSCREEN = 17,
            SM_CYKANJIWINDOW = 18,
            SM_MOUSEPRESENT = 19,
            SM_CYVSCROLL = 20,
            SM_CXHSCROLL = 21,
            SM_SWAPBUTTON = 23,
            SM_CXMIN = 28,
            SM_CYMIN = 29,
            SM_CXSIZE = 30,
            SM_CYSIZE = 31,
            SM_CXFRAME = 32,
            SM_CYFRAME = 33,
            SM_CXMINTRACK = 34,
            SM_CYMINTRACK = 35,
            SM_CYSMCAPTION = 51,
            SM_CXMINIMIZED = 57,
            SM_CYMINIMIZED = 58,
            SM_CXMAXTRACK = 59,
            SM_CYMAXTRACK = 60,
            SM_CXMAXIMIZED = 61,
            SM_CYMAXIMIZED = 62
        }
        #endregion

        #region Structs
        [StructLayout(LayoutKind.Sequential)]
        private struct PAINTSTRUCT
        {
            internal IntPtr hdc;
            internal int fErase;
            internal RECT rcPaint;
            internal int fRestore;
            internal int fIncUpdate;
            internal int Reserved1;
            internal int Reserved2;
            internal int Reserved3;
            internal int Reserved4;
            internal int Reserved5;
            internal int Reserved6;
            internal int Reserved7;
            internal int Reserved8;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            internal RECT(int X, int Y, int Width, int Height)
            {
                this.Left = X;
                this.Top = Y;
                this.Right = Width;
                this.Bottom = Height;
            }
            internal int Left;
            internal int Top;
            internal int Right;
            internal int Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SCROLLINFO
        {
            internal uint cbSize;
            internal uint fMask;
            internal int nMin;
            internal int nMax;
            internal uint nPage;
            internal int nPos;
            internal int nTrackPos;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SCROLLBARINFO
        {
            internal int cbSize;
            internal RECT rcScrollBar;
            internal int dxyLineButton;
            internal int xyThumbTop;
            internal int xyThumbBottom;
            internal int reserved;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            internal int[] rgstate;
        }



        //        [StructLayout(LayoutKind.Sequential)]
        //        public struct RECT
        //        {
        //            public Int32 Left;
        //            public Int32 Top;
        //            public Int32 Right;
        //            public Int32 Bottom;
        //        }

        //        [StructLayout(LayoutKind.Sequential)]
        //        private struct WINDOWINFO
        //        {
        //            public uint cbSize;
        //            public RECT rcWindow;
        //            public RECT rcClient;
        //            public uint dwStyle;
        //            public uint dwExStyle;
        //            public uint dwWindowStatus;
        //            public uint cxWindowBorders;
        //            public uint cyWindowBorders;
        //            public ushort atomWindowType;
        //            public ushort wCreatorVersion;

        //            public WINDOWINFO(Boolean? filler)
        //                : this()   // Allows automatic initialization of "cbSize" with "new WINDOWINFO(null/true/false)".
        //            {
        //                cbSize = (UInt32)(Marshal.SizeOf(typeof(WINDOWINFO)));
        //            }

        //        }






        #endregion

        #region API
        [DllImport("user32.dll")]
        private static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);

        public static long SRCINVERT = 0x00660046;
        [DllImport("gdi32.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool BitBlt(IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, long dwRop);
        [DllImport("gdi32.dll", EntryPoint = "SelectObject", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr h);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool StretchBlt(IntPtr hDest, int X, int Y, int nWidth, int nHeight, IntPtr hdcSrc,
        int sX, int sY, int nWidthSrc, int nHeightSrc, int dwRop);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr handle);

        [DllImport("user32.dll")]
        private static extern int ReleaseDC(IntPtr handle, IntPtr hdc);

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref SCROLLBARINFO lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private extern static int OffsetRect(ref RECT lpRect, int x, int y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ValidateRect(IntPtr hWnd, ref RECT lpRect);

        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(SYSTEM_METRICS smIndex);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool PtInRect([In] ref RECT lprc, Point pt);

        [DllImport("user32.dll")]
        private static extern int ScreenToClient(IntPtr hwnd, ref Point lpPoint);

        [DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);

        [DllImport("user32.dll")]
        private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("user32.dll")]
        private static extern int GetScrollPos(IntPtr hWnd, int nBar);

        [DllImport("user32.dll")]
        private static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr CreateWindowEx(int exstyle, string lpClassName, string lpWindowName, int dwStyle,
            int x, int y, int nWidth, int nHeight, IntPtr hwndParent, IntPtr Menu, IntPtr hInstance, IntPtr lpParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DestroyWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndAfter, int x, int y, int cx, int cy, uint flags);


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EqualRect([In] ref RECT lprc1, [In] ref RECT lprc2);

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern int GetScrollBarInfo(IntPtr hWnd, uint idObject, ref SCROLLBARINFO psbi);

        [DllImport("gdi32.dll")]
        public static extern bool DeleteDC(IntPtr hDC);

        [DllImport("Gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteObject(
        IntPtr hObject // handle to graphic object
        );

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll")]
        public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest, int nWidth,
           int nHeight, IntPtr hObjSource, int nXSrc, int nYSrc, TernaryRasterOperations dwRop);

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        private static extern int SetWindowTheme(IntPtr hWnd, String pszSubAppName, String pszSubIdList);

        [DllImport("uxtheme.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private extern static bool IsAppThemed();


        #endregion

        #region Fields
        public static cInternalScrollBar cInternalScrollBarRef;
        private bool _bTrackingMouse = false;
        private int _iArrowCx = 0;
        private int _iArrowCy = 0;
        private IntPtr _hVerticalMaskWnd = IntPtr.Zero;
        private IntPtr _hHorizontalMaskWnd = IntPtr.Zero;
        private IntPtr _hSizerMaskWnd = IntPtr.Zero;
        private IntPtr _hControlWnd = IntPtr.Zero;
        private cStoreDc _makHorizontalArrowDc = new cStoreDc();
        private cStoreDc _makHorizontalThumbDc = new cStoreDc();
        private cStoreDc _makHorizontalTrackDc = new cStoreDc();
        private cStoreDc _makVerticalArrowDc = new cStoreDc();
        private cStoreDc _makVerticalThumbDc = new cStoreDc();
        private cStoreDc _makVerticalTrackDc = new cStoreDc();

        private cStoreDc _cHorizontalArrowDc = new cStoreDc();
        private cStoreDc _cHorizontalThumbDc = new cStoreDc();
        private cStoreDc _cHorizontalTrackDc = new cStoreDc();
        private cStoreDc _cVerticalArrowDc = new cStoreDc();
        private cStoreDc _cVerticalThumbDc = new cStoreDc();
        private cStoreDc _cVerticalTrackDc = new cStoreDc();
        private Bitmap _oHorizontalArrowBitmap;
        private Bitmap _oHorizontalThumbBitmap;
        private Bitmap _oHorizontalTrackBitmap;
        private Bitmap _oVerticalArrowBitmap;
        private Bitmap _oVerticalThumbBitmap;
        private Bitmap _oVerticalTrackBitmap;
        private Bitmap _oMask;
        private bool forceRepaint = false;
        private Object thisLock = new Object();

        #endregion

        #region Constructor


        public IntPtr selectObject(IntPtr Hdc, IntPtr HBmp)
        {


            return SelectObject(Hdc, HBmp);

        }
        public void selectObject(Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb)
        {




            SelectObject(_cHorizontalArrowDc.Hdc, hzarrow.GetHbitmap());
            SelectObject(_cHorizontalThumbDc.Hdc, hzthumb.GetHbitmap());
            SelectObject(_cHorizontalTrackDc.Hdc, hztrack.GetHbitmap());
            SelectObject(_cVerticalArrowDc.Hdc, vtarrow.GetHbitmap());
            SelectObject(_cVerticalThumbDc.Hdc, vtthumb.GetHbitmap());
            SelectObject(_cVerticalTrackDc.Hdc, vttrack.GetHbitmap());

            _makHorizontalArrowDc = _cHorizontalArrowDc;
            _makHorizontalThumbDc = _cHorizontalThumbDc;
            _makHorizontalTrackDc = _cHorizontalTrackDc;
            _makVerticalArrowDc = _cVerticalArrowDc;
            _makVerticalThumbDc = _cVerticalThumbDc;
            _makVerticalTrackDc = _cVerticalTrackDc;

        }




        public cInternalScrollBar(IntPtr hWnd, Bitmap hztrack = null, Bitmap hzarrow = null, Bitmap hzthumb = null, Bitmap vttrack = null, Bitmap vtarrow = null, Bitmap vtthumb = null, Bitmap fader = null, bool forcerepaint = true)
        {
            if (hWnd == IntPtr.Zero)
                throw new Exception("The control handle is invalid.");
            if (hztrack == null && _oHorizontalTrackBitmap != null)
            {
                hztrack = _oHorizontalTrackBitmap; // throw new Exception("The Horizontal Track image is invalid.");

            }
            else
            {

                return;
            }
            if (hzarrow == null && _oHorizontalArrowBitmap != null)
            { 
                hzarrow = _oHorizontalArrowBitmap; // throw new Exception("The Horizontal Arrow image is invalid.");

            }
            else
            {

                return;
            }
            if (hzthumb == null && _oHorizontalThumbBitmap != null)
            { 
                hzthumb = _oHorizontalThumbBitmap; // throw new Exception("The Horizontal Thumb image is invalid.");

            }
            else
            {

                return;
            }
            if (vttrack == null && _oVerticalTrackBitmap != null)
            {
                vttrack = _oVerticalTrackBitmap; // throw new Exception("The Vertical Track image is invalid.");
            }
            else
            {

                return;
            }
            if (vtarrow == null && _oVerticalArrowBitmap != null)
            {
                vtarrow = _oVerticalArrowBitmap; // throw new Exception("The Vertical Arrow image is invalid.");
            }
            else
            {

                return;
            }
            if (vtthumb == null && _oVerticalThumbBitmap != null)
            {

                vtthumb = _oVerticalThumbBitmap; // throw new Exception("The Vertical Thumb image is invalid.");

            }
            else
            {

                return;

            }




            _oHorizontalArrowBitmap = hzarrow;
            _oHorizontalThumbBitmap = hzthumb;
            _oHorizontalTrackBitmap = hztrack;
            _oVerticalArrowBitmap = vtarrow;
            _oVerticalThumbBitmap = vtthumb;
            _oVerticalTrackBitmap = vttrack;








   


            
            forceRepaint = true;

            if (forceRepaint == false)
            {

                _cHorizontalArrowDc = _makHorizontalArrowDc;
                _cHorizontalThumbDc = _makHorizontalThumbDc;
                _cHorizontalTrackDc = _makHorizontalTrackDc;
                _cVerticalArrowDc = _makVerticalArrowDc;
                _cVerticalThumbDc = _makVerticalThumbDc;
                _cVerticalTrackDc = _makVerticalTrackDc;



                if (fader != null)
                    TransitionGraphic = fader;
                scrollbarMetrics();
                _hControlWnd = hWnd;
                createScrollBarMask();
                this.AssignHandle(_hControlWnd);
               
                return;
            }

            

            //    _makHorizontalArrowDc = _cHorizontalArrowDc;
            //    _makHorizontalThumbDc = _cHorizontalThumbDc;
            //    _makHorizontalTrackDc = _cHorizontalTrackDc;
            //    _makVerticalArrowDc = _cVerticalArrowDc;
            //    _makVerticalThumbDc = _cVerticalThumbDc;
            //    _makVerticalTrackDc = _cVerticalTrackDc;



            //}
            //else
            //{




                //HorizontalArrowGraphic = hzarrow;
                //HorizontalThumbGraphic = hzthumb;
                //HorizontalTrackGraphic = hztrack;
                //VerticalArrowGraphic = vtarrow;
                //VerticalThumbGraphic = vtthumb;
                //VerticalTrackGraphic = vttrack;
            //thisLock = new Object();
            //lock (thisLock)
            //{

                //if (forceRepaint)
                //{
            _cHorizontalArrowDc.Dispose();
            _cHorizontalThumbDc.Dispose();
            _cHorizontalTrackDc.Dispose();
            _cVerticalArrowDc.Dispose();
            _cVerticalThumbDc.Dispose();
            _cVerticalTrackDc.Dispose();

            _cHorizontalArrowDc = new cStoreDc();
            _cHorizontalThumbDc = new cStoreDc();
            _cHorizontalTrackDc = new cStoreDc();
            _cVerticalArrowDc = new cStoreDc();
            _cVerticalThumbDc = new cStoreDc();
            _cVerticalTrackDc = new cStoreDc();
                //}
                //else
                //{

                //    //_cHorizontalArrowDc = _makHorizontalArrowDc;
                //    //_cHorizontalThumbDc = _makHorizontalThumbDc;
                //    //_cHorizontalTrackDc = _makHorizontalTrackDc;
                //    //_cVerticalArrowDc = _makVerticalArrowDc;
                //    //_cVerticalThumbDc = _makVerticalThumbDc;
                //    //_cVerticalTrackDc = _makVerticalTrackDc;

                //    //drawScrollBar();
                //    //return;

                //}





                //if (forceRepaint)
                //{
                    _cHorizontalArrowDc.Width = hzarrow.Width;
                    _cHorizontalArrowDc.Height = hzarrow.Height;
                    _cHorizontalThumbDc.Width = hzthumb.Width;
                    _cHorizontalThumbDc.Height = hzthumb.Height;
                    _cHorizontalTrackDc.Width = hztrack.Width;
                    _cHorizontalTrackDc.Height = hztrack.Height;
                    _cVerticalArrowDc.Width = vtarrow.Width;
                    _cVerticalArrowDc.Height = vtarrow.Height;
                    _cVerticalThumbDc.Width = vtthumb.Width;
                    _cVerticalThumbDc.Height = vtthumb.Height;
                    _cVerticalTrackDc.Width = vttrack.Width;
                    _cVerticalTrackDc.Height = vttrack.Height;
                //    try
                //    {
                //        Thread selectBmp1 = new Thread(() => selectObject(_cHorizontalArrowDc.Hdc, hzarrow.GetHbitmap()));
                        
                        
                //        Thread selectBmp2 = new Thread(() => selectObject(_cHorizontalThumbDc.Hdc, hzthumb.GetHbitmap()));
                        
                        
                //        Thread selectBmp3 = new Thread(() => selectObject(_cHorizontalTrackDc.Hdc, hztrack.GetHbitmap()));
                        
                        
                //        Thread selectBmp4 = new Thread(() => selectObject(_cVerticalArrowDc.Hdc, vtarrow.GetHbitmap()));
                        
                        
                //        Thread selectBmp5 = new Thread(() => selectObject(_cVerticalThumbDc.Hdc, vtthumb.GetHbitmap()));
                        
                        
                //        Thread selectBmp6 = new Thread(() => selectObject(_cVerticalTrackDc.Hdc, vttrack.GetHbitmap()));

                        
                //        selectBmp1.Start();
                //        selectBmp1.Join();
                //        selectBmp2.Start();
                //        selectBmp2.Join();
                //        selectBmp3.Start();
                //        selectBmp3.Join();
                //        selectBmp4.Start();
                //        selectBmp4.Join();
                //        selectBmp5.Start();
                //        selectBmp5.Join();
                //        selectBmp6.Start();
                //        selectBmp6.Join();
   
                        



                //    }
                //catch
                //    {

                //    }
                    //SelectObject(_cHorizontalArrowDc.Hdc, hzarrow.GetHbitmap());
                    //SelectObject(_cHorizontalThumbDc.Hdc, hzthumb.GetHbitmap());
                    //SelectObject(_cHorizontalTrackDc.Hdc, hztrack.GetHbitmap());
                    //SelectObject(_cVerticalArrowDc.Hdc, vtarrow.GetHbitmap());
                    //SelectObject(_cVerticalThumbDc.Hdc, vtthumb.GetHbitmap());
                    //SelectObject(_cVerticalTrackDc.Hdc, vttrack.GetHbitmap());
                    //}
                    //_makHorizontalArrowDc = _cHorizontalArrowDc;
                    //_makHorizontalThumbDc = _cHorizontalThumbDc;
                    //_makHorizontalTrackDc = _cHorizontalTrackDc;
                    //_makVerticalArrowDc = _cVerticalArrowDc;
                    //_makVerticalThumbDc = _cVerticalThumbDc;
                    //_makVerticalTrackDc = _cVerticalTrackDc;
                    
                //}
            
                //createScrollBarMask();
                ////checkBarState();
                //drawScrollBar();


                    selectObject(hztrack, hzarrow, hzthumb, vttrack, vtarrow, vtthumb);

                    scrollbarMetrics();
                    _hControlWnd = hWnd;
                    if (Environment.OSVersion.Version.Major > 5)
                    {
                        if (IsAppThemed())
                            SetWindowTheme(_hControlWnd, "", "");
                    }
                    createScrollBarMask();

            // the fader for this class would require
            // some additional code, with the fader inclass
                    if (fader != null)
                        TransitionGraphic = fader;
                    scrollbarMetrics();
                    _hControlWnd = hWnd;
                    createScrollBarMask();
                    this.AssignHandle(_hControlWnd);

           
            //if (forceRepaint)
            //{
            //    ProGammaX.cRCM _cRcm;
            //    _cRcm = new ProGammaX.cRCM(this.Handle);
            //    // skin controls in containers [tabcontrol]
            //    _cRcm.SkinChildControls = true;
            //    // fade graphic
            //    _cRcm.TransitionGraphic = ProGammaX.Properties.Resources.fader;
            //    // use custom tooltips
            //    _cRcm.UseCustomTips = true;
            //    _cRcm.Start();
            //    //checkBarState();
            //    //drawScrollBar();


            //}




        }
        #endregion

        #region Properties
        private Bitmap HorizontalArrowGraphic
        {
            get { return _oHorizontalArrowBitmap; }
            set
            {
                try
                {

                    //_oHorizontalArrowBitmap = new Bitmap(HorizontalArrowGraphic);
                    if (forceRepaint)
                    {
                        _oHorizontalArrowBitmap = value;
                        _cHorizontalArrowDc.Dispose();
                        _cHorizontalArrowDc = new cStoreDc();
                        _cHorizontalArrowDc.Width = _oHorizontalArrowBitmap.Width;
                        _cHorizontalArrowDc.Height = _oHorizontalArrowBitmap.Height;
                        SelectObject(_cHorizontalArrowDc.Hdc, _oHorizontalArrowBitmap.GetHbitmap());


                    }
                    else
                    {
                        ////Here we select the compatible bitmap in memeory device context and keeps the refrence to Old bitmap.
                        //_makPreviousBrushUsed = SelectObject(_cHorizontalArrowDc.Hdc, _oHorizontalArrowBitmap.GetHbitmap());
                        ////IntPtr hOld = (IntPtr)SelectObject(hMemDC, m_HBitmap);
                        //_cHorizontalArrowDc.Width = _oHorizontalArrowBitmap.Width;
                        //_cHorizontalArrowDc.Height = _oHorizontalArrowBitmap.Height;
                        ////We copy the Bitmap to the memory device context.
                        //IntPtr compatDC = CreateCompatibleDC(this.Handle);

                        //BitBlt(_cHorizontalArrowDc.Hdc, 0, 0, _cHorizontalArrowDc.Width, _cHorizontalArrowDc.Height, compatDC, 0, 0, TernaryRasterOperations.SRCCOPY);

                        ////We select the old bitmap back to the memory device context.
                        //SelectObject(_cHorizontalArrowDc.Hdc, _makPreviousBrushUsed);

                        ////We delete the memory device context.
                        //DeleteDC(_cHorizontalArrowDc.Hdc);

                        ////We release the screen device context.
                        ////ReleaseDC(PlatformInvokeUSER32.GetDesktopWindow(), hDC);

                        ////Image is created by Image bitmap handle and assigned to Bitmap variable.
                        //value = System.Drawing.Image.FromHbitmap(_oHorizontalArrowBitmap.GetHbitmap());

                        ////Delete the compatible bitmap object.
                        //DeleteObject(_oHorizontalArrowBitmap.GetHbitmap());
                        ////_cHorizontalArrowDc.Update(this.Handle);
                        _oHorizontalArrowBitmap = value;
                        return;


                    }


                    //if (_oHorizontalArrowBitmap == HorizontalArrowGraphic || _cHorizontalArrowDc.Hdc == _oHorizontalArrowBitmap.GetHbitmap())
                    //{

                    //    //_oHorizontalArrowBitmap = null;
                    //    //_cHorizontalArrowDc.Dispose();
                    //   //if (!forceRepaint)
                    //    return;

                    //}
                    //else
                    //{


                        
                    //    //_cHorizontalArrowDc.Dispose();


                    //}






                  

                }
                catch
                {


                }


                

            }
        }

        private Bitmap HorizontalThumbGraphic
        {
            get { return _oHorizontalThumbBitmap; }
            set
            {
                try
                {
                    if (forceRepaint)
                    {
                        _oHorizontalThumbBitmap = value;
                        _cHorizontalThumbDc.Dispose();
                        _cHorizontalThumbDc = new cStoreDc();
                        _cHorizontalThumbDc.Width = _oHorizontalThumbBitmap.Width;
                        _cHorizontalThumbDc.Height = _oHorizontalThumbBitmap.Height;
                        SelectObject(_cHorizontalThumbDc.Hdc, _oHorizontalThumbBitmap.GetHbitmap());


                    }
                    else
                    {
                        ////Here we select the compatible bitmap in memeory device context and keeps the refrence to Old bitmap.
                        //_makPreviousBrushUsed = SelectObject(_cHorizontalThumbDc.Hdc, _oHorizontalThumbBitmap.GetHbitmap());
                        ////IntPtr hOld = (IntPtr)SelectObject(hMemDC, m_HBitmap);
                        //_cHorizontalThumbDc.Width = _oHorizontalThumbBitmap.Width;
                        //_cHorizontalThumbDc.Height = _oHorizontalThumbBitmap.Height;
                        ////We copy the Bitmap to the memory device context.
                        //IntPtr compatDC = CreateCompatibleDC(this.Handle);

                        //BitBlt(_cHorizontalThumbDc.Hdc, 0, 0, _cHorizontalThumbDc.Width, _cHorizontalThumbDc.Height, compatDC, 0, 0, TernaryRasterOperations.SRCCOPY);

                        ////We select the old bitmap back to the memory device context.
                        //SelectObject(_cHorizontalThumbDc.Hdc, _makPreviousBrushUsed);

                        ////We delete the memory device context.
                        //DeleteDC(_cHorizontalThumbDc.Hdc);

                        ////We release the screen device context.
                        ////ReleaseDC(PlatformInvokeUSER32.GetDesktopWindow(), hDC);

                        ////Image is created by Image bitmap handle and assigned to Bitmap variable.
                        //value = System.Drawing.Image.FromHbitmap(_oHorizontalThumbBitmap.GetHbitmap());

                        ////Delete the compatible bitmap object.
                        //DeleteObject(_oHorizontalThumbBitmap.GetHbitmap());
                        ////_cHorizontalThumbDc.Update(this.Handle);
                        _oHorizontalThumbBitmap = value;
                        return;


                    }

                    //if (_oHorizontalThumbBitmap == HorizontalThumbGraphic || _cHorizontalThumbDc.Hdc == _oHorizontalThumbBitmap.GetHbitmap())
                    //{
                    //    //_oHorizontalThumbBitmap = null;
                    //    //_cHorizontalThumbDc.Dispose();
                    //    //if (!forceRepaint)
                    //        return;
                    //}
                    //else
                    //{



                        
                    //    //_cHorizontalThumbDc.Dispose();


                    //}






                }
                catch
                {


                }


               
            }
        }

        private Bitmap HorizontalTrackGraphic
        {
            get { return _oHorizontalTrackBitmap; }
            set
            {
                try
                {
                    if (forceRepaint)
                    {
                        _oHorizontalTrackBitmap = value;
                        _cHorizontalTrackDc.Dispose();
                        _cHorizontalTrackDc = new cStoreDc();
                        _cHorizontalTrackDc.Width = _oHorizontalTrackBitmap.Width;
                        _cHorizontalTrackDc.Height = _oHorizontalTrackBitmap.Height;
                        SelectObject(_cHorizontalTrackDc.Hdc, _oHorizontalTrackBitmap.GetHbitmap());


                    }
                    else
                    {
                        ////Here we select the compatible bitmap in memeory device context and keeps the refrence to Old bitmap.
                        //_makPreviousBrushUsed = SelectObject(_cHorizontalTrackDc.Hdc, _oHorizontalTrackBitmap.GetHbitmap());
                        ////IntPtr hOld = (IntPtr)SelectObject(hMemDC, m_HBitmap);
                        //_cHorizontalTrackDc.Width = _oHorizontalTrackBitmap.Width;
                        //_cHorizontalTrackDc.Height = _oHorizontalTrackBitmap.Height;
                        ////We copy the Bitmap to the memory device context.
                        //IntPtr compatDC = CreateCompatibleDC(this.Handle);

                        //BitBlt(_cHorizontalTrackDc.Hdc, 0, 0, _cHorizontalTrackDc.Width, _cHorizontalTrackDc.Height, compatDC, 0, 0, TernaryRasterOperations.SRCCOPY);

                        ////We select the old bitmap back to the memory device context.
                        //SelectObject(_cHorizontalTrackDc.Hdc, _makPreviousBrushUsed);

                        ////We delete the memory device context.
                        //DeleteDC(_cHorizontalTrackDc.Hdc);

                        ////We release the screen device context.
                        ////ReleaseDC(PlatformInvokeUSER32.GetDesktopWindow(), hDC);

                        ////Image is created by Image bitmap handle and assigned to Bitmap variable.
                        //value = System.Drawing.Image.FromHbitmap(_oHorizontalTrackBitmap.GetHbitmap());

                        ////Delete the compatible bitmap object.
                        //DeleteObject(_oHorizontalTrackBitmap.GetHbitmap());
                        ////_cHorizontalTrackDc.Update(this.Handle);
                        _oHorizontalTrackBitmap = value;
                        return;




                    }
                    //if (_oHorizontalTrackBitmap == HorizontalTrackGraphic || _cHorizontalTrackDc.Hdc == _oHorizontalTrackBitmap.GetHbitmap())
                    //{
                    //    //_oHorizontalTrackBitmap = null;
                    //    //_cHorizontalTrackDc.Dispose();
                    //    //if (!forceRepaint)
                    //        return;

                    //}
                    //else
                    //{



                        
                    //    //_cHorizontalTrackDc.Dispose();

                    //}







                }
                catch
                {


                }


                
            }
        }

        private Bitmap VerticalArrowGraphic
        {
            get { return _oVerticalArrowBitmap; }
            set
            {
                try
                {

                    if (forceRepaint)
                    {
                        _oVerticalArrowBitmap = value;
                        _cVerticalArrowDc.Dispose();
                        _cVerticalArrowDc = new cStoreDc();
                        _cVerticalArrowDc.Width = _oVerticalArrowBitmap.Width;
                        _cVerticalArrowDc.Height = _oVerticalArrowBitmap.Height;
                        SelectObject(_cVerticalArrowDc.Hdc, _oVerticalArrowBitmap.GetHbitmap());


                    }
                    else
                    {
                        ////Here we select the compatible bitmap in memeory device context and keeps the refrence to Old bitmap.
                        //_makPreviousBrushUsed = SelectObject(_cVerticalArrowDc.Hdc, _oVerticalArrowBitmap.GetHbitmap());
                        ////IntPtr hOld = (IntPtr)SelectObject(hMemDC, m_HBitmap);
                        //_cVerticalArrowDc.Width = _oVerticalArrowBitmap.Width;
                        //_cVerticalArrowDc.Height = _oVerticalArrowBitmap.Height;
                        ////We copy the Bitmap to the memory device context.
                        //IntPtr compatDC = CreateCompatibleDC(this.Handle);

                        //BitBlt(_cVerticalArrowDc.Hdc, 0, 0, _cVerticalArrowDc.Width, _cVerticalArrowDc.Height, compatDC, 0, 0, TernaryRasterOperations.SRCCOPY);

                        ////We select the old bitmap back to the memory device context.
                        //SelectObject(_cVerticalArrowDc.Hdc, _makPreviousBrushUsed);

                        ////We delete the memory device context.
                        //DeleteDC(_cVerticalArrowDc.Hdc);

                        ////We release the screen device context.
                        ////ReleaseDC(PlatformInvokeUSER32.GetDesktopWindow(), hDC);

                        ////Image is created by Image bitmap handle and assigned to Bitmap variable.
                        //value = System.Drawing.Image.FromHbitmap(_oVerticalArrowBitmap.GetHbitmap());

                        ////Delete the compatible bitmap object.
                        //DeleteObject(_oVerticalArrowBitmap.GetHbitmap());
                        ////_cVerticalArrowDc.Update(this.Handle);
                        _oVerticalArrowBitmap = value;
                        return;



                    }

                    //if (_oVerticalArrowBitmap == VerticalArrowGraphic || _cVerticalArrowDc.Hdc == _oVerticalArrowBitmap.GetHbitmap())
                    //{
                    //    //_oVerticalArrowBitmap = null;
                    //    //_cVerticalArrowDc.Dispose();
                    //    //if (!forceRepaint)
                    //        return;

                    //}
                    //else
                    //{


                        
                    //    //_cVerticalArrowDc.Dispose();


                    //}


      

                }
                catch
                {


                }


                
            }
        }

        private Bitmap VerticalThumbGraphic
        {
            get { return _oVerticalThumbBitmap; }
            set
            {
                try
                {



                    if (forceRepaint)
                    {
                        _oVerticalThumbBitmap = value;
                        _cVerticalThumbDc.Dispose();
                        _cVerticalThumbDc = new cStoreDc();
                        _cVerticalThumbDc.Width = _oVerticalThumbBitmap.Width;
                        _cVerticalThumbDc.Height = _oVerticalThumbBitmap.Height;
                        SelectObject(_cVerticalThumbDc.Hdc, _oVerticalThumbBitmap.GetHbitmap());


                    }
                    else
                    {
                        
                        ////Here we select the compatible bitmap in memeory device context and keeps the refrence to Old bitmap.
                        //_makPreviousBrushUsed = SelectObject(_cVerticalThumbDc.Hdc, _oVerticalThumbBitmap.GetHbitmap());
                        ////IntPtr hOld = (IntPtr)SelectObject(hMemDC, m_HBitmap);
                        //_cVerticalThumbDc.Width = _oVerticalThumbBitmap.Width;
                        //_cVerticalThumbDc.Height = _oVerticalThumbBitmap.Height;
                        ////We copy the Bitmap to the memory device context.
                        //IntPtr compatDC = CreateCompatibleDC(this.Handle);

                        //BitBlt(_cVerticalThumbDc.Hdc, 0, 0, _cVerticalThumbDc.Width, _cVerticalThumbDc.Height, compatDC, 0, 0, TernaryRasterOperations.SRCCOPY);

                        ////We select the old bitmap back to the memory device context.
                        //SelectObject(_cVerticalThumbDc.Hdc, _makPreviousBrushUsed);

                        ////We delete the memory device context.
                        //DeleteDC(_cVerticalThumbDc.Hdc);

                        ////We release the screen device context.
                        ////ReleaseDC(PlatformInvokeUSER32.GetDesktopWindow(), hDC);

                        ////Image is created by Image bitmap handle and assigned to Bitmap variable.
                        //value = System.Drawing.Image.FromHbitmap(_oVerticalThumbBitmap.GetHbitmap());

                        ////Delete the compatible bitmap object.
                        //DeleteObject(_oVerticalThumbBitmap.GetHbitmap());
                        ////_cVerticalThumbDc.Update(this.Handle);
                        _oVerticalThumbBitmap = value;
                        return;


                    }


                    //if (_oVerticalThumbBitmap == VerticalThumbGraphic || _cVerticalThumbDc.Hdc == _oVerticalThumbBitmap.GetHbitmap())
                    //{
                    //    //_oVerticalThumbBitmap = null;
                    //    //_cVerticalThumbDc.Dispose();
                    //    //if (!forceRepaint)
                    //        return;


                    //}
                    //else
                    //{

                        
                    //    //_oVerticalThumbBitmap = null;
                    //    //_cVerticalThumbDc.Dispose();


                    //}






                }
                catch
                {


                }



                
            }
        }

        private Bitmap VerticalTrackGraphic
        {
            get { return _oVerticalTrackBitmap; }
            set
            {

                try
                {



                    if (forceRepaint)
                    {
                        _oVerticalTrackBitmap = value;
                        _cVerticalTrackDc.Dispose();
                        _cVerticalTrackDc = new cStoreDc();
                        _cVerticalTrackDc.Width = _oVerticalTrackBitmap.Width;
                        _cVerticalTrackDc.Height = _oVerticalTrackBitmap.Height;
                        SelectObject(_cVerticalTrackDc.Hdc, _oVerticalTrackBitmap.GetHbitmap());
 

                    }
                    else
                    {
                        ////Here we select the compatible bitmap in memeory device context and keeps the refrence to Old bitmap.
                        //_makPreviousBrushUsed = SelectObject(_cVerticalTrackDc.Hdc, _oVerticalTrackBitmap.GetHbitmap());
                        ////IntPtr hOld = (IntPtr)SelectObject(hMemDC, m_HBitmap);
                        //_cVerticalTrackDc.Width = _oVerticalTrackBitmap.Width;
                        //_cVerticalTrackDc.Height = _oVerticalTrackBitmap.Height;
                        ////We copy the Bitmap to the memory device context.
                        //IntPtr compatDC = CreateCompatibleDC(this.Handle);

                        //BitBlt(_cVerticalTrackDc.Hdc, 0, 0, _cVerticalTrackDc.Width, _cVerticalTrackDc.Height, compatDC, 0, 0, TernaryRasterOperations.SRCCOPY);

                        ////We select the old bitmap back to the memory device context.
                        //SelectObject(_cVerticalTrackDc.Hdc, _makPreviousBrushUsed);

                        ////We delete the memory device context.
                        //DeleteDC(_cVerticalTrackDc.Hdc);

                        ////We release the screen device context.
                        ////ReleaseDC(PlatformInvokeUSER32.GetDesktopWindow(), hDC);

                        ////Image is created by Image bitmap handle and assigned to Bitmap variable.
                        //value = System.Drawing.Image.FromHbitmap(_oVerticalTrackBitmap.GetHbitmap());

                        ////Delete the compatible bitmap object.
                        //DeleteObject(_oVerticalTrackBitmap.GetHbitmap());

                        ////_cVerticalTrackDc.Update(this.Handle);

                        _oVerticalTrackBitmap = value;
                        return;



                    }

                    //if (_oVerticalTrackBitmap == VerticalTrackGraphic || _cVerticalTrackDc.Hdc == _oVerticalTrackBitmap.GetHbitmap())
                    //{
                    //    //_oVerticalTrackBitmap = null;
                    //    //_cVerticalTrackDc.Dispose();
                    //    //if (!forceRepaint)
                    //        return;


                    //}
                    //else
                    //{

                        
                    //    //_cVerticalTrackDc.Dispose();


                    //}



                            //}
                         


                }
                catch
                {


                }



               
            }
        }



        private Bitmap TransitionGraphic
        {
            get { return _oMask; }
            set { _oMask = value; }
        }

        private int HScrollPos
        {
            get { return GetScrollPos((IntPtr)this.Handle, SB_HORZ); }
            set { SetScrollPos((IntPtr)this.Handle, SB_HORZ, value, true); }
        }

        private int VScrollPos
        {
            get { return GetScrollPos((IntPtr)this.Handle, SB_VERT); }
            set { SetScrollPos((IntPtr)this.Handle, SB_VERT, value, true); }
        }
        #endregion

        #region Methods
        public void checkBarState()
        {


            if ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_VISIBLE) == WS_VISIBLE)
            {


                if (hasHorizontal())
                    ShowWindow(_hHorizontalMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hHorizontalMaskWnd, SW_HIDE);

                if (hasVertical())
                    ShowWindow(_hVerticalMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hVerticalMaskWnd, SW_HIDE);

                if (hasSizer())
                    ShowWindow(_hSizerMaskWnd, SW_NORMAL);
                else
                    ShowWindow(_hSizerMaskWnd, SW_HIDE);

                
            }
            else
            {
                ShowWindow(_hHorizontalMaskWnd, SW_HIDE);
                ShowWindow(_hVerticalMaskWnd, SW_HIDE);
                ShowWindow(_hSizerMaskWnd, SW_HIDE);
            }
        }

        public void createScrollBarMask()
        {
            Type t = typeof(cScrollBar);
            Module m = t.Module;
            IntPtr hInstance = Marshal.GetHINSTANCE(m);
            IntPtr hParent = GetParent(_hControlWnd);
            RECT tr = new RECT();
            Point pt = new Point();
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);

            // vertical scrollbar
            // get the size and position
            GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
            tr = sb.rcScrollBar;
            pt.X = tr.Left;
            pt.Y = tr.Top;
            ScreenToClient(hParent, ref pt);

            // create the window
            _hVerticalMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X, pt.Y,
                (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            // set z-order
            SetWindowPos(_hVerticalMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

            // horizontal scrollbar
            GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
            tr = sb.rcScrollBar;
            pt.X = tr.Left;
            pt.Y = tr.Top;
            ScreenToClient(hParent, ref pt);

            _hHorizontalMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X, pt.Y,
                (tr.Right - tr.Left), (tr.Bottom - tr.Top),
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            SetWindowPos(_hHorizontalMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);

            // sizer
            _hSizerMaskWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                "STATIC", "",
                SS_OWNERDRAW | WS_CHILD | WS_CLIPSIBLINGS | WS_OVERLAPPED | WS_VISIBLE,
                pt.X + (tr.Right - tr.Left), pt.Y,
                _iArrowCx, _iArrowCy,
                hParent,
                IntPtr.Zero, hInstance, IntPtr.Zero);

            SetWindowPos(_hSizerMaskWnd, HWND_TOP,
                0, 0,
                0, 0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
            reSizeMask();
        }

        public void drawScrollBar()
        {
            RECT tr = new RECT();
            Point pst = new Point();
            cStoreDc tempDc = new cStoreDc();
            IntPtr hdc = IntPtr.Zero;
            int offset = 0;
            int width = 0;
            int section = 0;
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);

            if (hasHorizontal())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                OffsetRect(ref tr, -tr.Left, -tr.Top);
                tempDc.Width = tr.Right;
                tempDc.Height = tr.Bottom;
                SB_HITEST hitTest = scrollbarHitTest(Orientation.Horizontal);

                // draw the track
                using (StretchImage si = new StretchImage(_cHorizontalTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cHorizontalTrackDc.Width, _cHorizontalTrackDc.Height), new Rectangle(_iArrowCx, 0, tr.Right - (2 * _iArrowCx), tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                // draw the arrows
                section = 7;
                width = _cHorizontalArrowDc.Width / section;
                // left arrow
                if (hitTest == SB_HITEST.leftArrow)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }
                using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(0, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // right arrow
                if (hitTest == SB_HITEST.rightArrow)
                {
                    if (leftKeyPressed())
                        offset = 5;
                    else
                        offset = 4;

                }
                else
                {
                    offset = 3;
                }
                using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(tr.Right - _iArrowCx, 0, _iArrowCx, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // draw the thumb
                section = 3;
                width = _cHorizontalThumbDc.Width / section;
                if (hitTest == SB_HITEST.button)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }
                pst.X = sb.xyThumbTop;
                pst.Y = sb.xyThumbBottom;







                using (StretchImage si = new StretchImage(_cHorizontalThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalThumbDc.Height), new Rectangle(pst.X, 2, pst.Y - pst.X, tr.Bottom), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                hdc = GetDC(_hHorizontalMaskWnd);
                BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hHorizontalMaskWnd, hdc);
            }

            if (hasSizer())
            {
                tempDc.Width = _iArrowCx;
                tempDc.Height = _iArrowCy;
                offset = 6;
                section = 7;
                width = _cHorizontalArrowDc.Width / section;

                using (StretchImage si = new StretchImage(_cHorizontalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cHorizontalArrowDc.Height), new Rectangle(0, 0, _iArrowCx, _iArrowCy), 0, StretchModeEnum.STRETCH_HALFTONE)) { }
                hdc = GetDC(_hSizerMaskWnd);
                BitBlt(hdc, 0, 0, _iArrowCx, _iArrowCy, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hSizerMaskWnd, hdc);
            }

            if (hasVertical())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                OffsetRect(ref tr, -tr.Left, -tr.Top);
                tempDc.Width = tr.Right;
                tempDc.Height = tr.Bottom;
                SB_HITEST hitTest = scrollbarHitTest(Orientation.Vertical);

                // draw the track
                using (StretchImage si = new StretchImage(_cVerticalTrackDc.Hdc, tempDc.Hdc, new Rectangle(0, 0, _cVerticalTrackDc.Width, _cVerticalTrackDc.Height), new Rectangle(0, _iArrowCy, tr.Right, tr.Bottom - (2 * _iArrowCy)), 2, StretchModeEnum.STRETCH_HALFTONE)) { }
                section = 6;
                width = _cVerticalArrowDc.Width / section;

                // top arrow
                if (hitTest == SB_HITEST.topArrow)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }
                using (StretchImage si = new StretchImage(_cVerticalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalArrowDc.Height), new Rectangle(0, 0, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // bottom arrow
                if (hitTest == SB_HITEST.bottomArrow)
                {
                    if (leftKeyPressed())
                        offset = 5;
                    else
                        offset = 4;

                }
                else
                {
                    offset = 3;
                }
                using (StretchImage si = new StretchImage(_cVerticalArrowDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalArrowDc.Height), new Rectangle(0, tr.Bottom - _iArrowCy, tr.Right, _iArrowCy), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                // draw the thumb
                section = 3;
                width = _cVerticalThumbDc.Width / section;
                if (hitTest == SB_HITEST.button)
                {
                    if (leftKeyPressed())
                        offset = 2;
                    else
                        offset = 1;

                }
                else
                {
                    offset = 0;
                }

                pst.X = sb.xyThumbTop;
                pst.Y = sb.xyThumbBottom;
                using (StretchImage si = new StretchImage(_cVerticalThumbDc.Hdc, tempDc.Hdc, new Rectangle(offset * width, 0, width, _cVerticalThumbDc.Height), new Rectangle(0, pst.X, _iArrowCx, pst.Y - pst.X), 2, StretchModeEnum.STRETCH_HALFTONE)) { }

                hdc = GetDC(_hVerticalMaskWnd);
                BitBlt(hdc, 0, 0, tr.Right, tr.Bottom, tempDc.Hdc, 0, 0, 0xCC0020);
                ReleaseDC(_hVerticalMaskWnd, hdc);
            }
            tempDc.Dispose();
 
        }

        private void scrollFader()
        {
            if (TransitionGraphic != null)
            {
                SB_HITEST hitTest;
                SCROLLBARINFO sb = new SCROLLBARINFO();
                sb.cbSize = Marshal.SizeOf(sb);
                if (hasHorizontal())
                {
                    hitTest = scrollbarHitTest(Orientation.Horizontal);

                    if ((hitTest == SB_HITEST.button) && (!leftKeyPressed()))
                    {
                        GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                        // start the transition routines here
                        // size of mask - new Rectangle(sb.xyThumbTop, 2, sb.xyThumbBottom - sb.xyThumbTop, sb.rcScrollBar.Bottom)
                    }
                }
                if (hasVertical())
                {
                    hitTest = scrollbarHitTest(Orientation.Vertical);

                    if ((hitTest == SB_HITEST.button) && (!leftKeyPressed()))
                    {
                        GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                        // start the transition routines here
                        // size of mask - new Rectangle(sb.xyThumbTop, 2, sb.xyThumbBottom - sb.xyThumbTop, sb.rcScrollBar.Bottom)
                    }
                }
            }


        }

        private bool hasHorizontal()
        {
            return ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_HSCROLL) == WS_HSCROLL);
        }

        private bool hasSizer()
        {
            return (hasHorizontal() && hasVertical());
        }

        private bool hasVertical()
        {
            return ((GetWindowLong(_hControlWnd, GWL_STYLE) & WS_VSCROLL) == WS_VSCROLL);
        }

        private void invalidateWindow(bool messaged)
        {
            if (messaged)
                RedrawWindow(_hControlWnd, IntPtr.Zero, IntPtr.Zero, RDW_INTERNALPAINT);
            else
                RedrawWindow(_hControlWnd, IntPtr.Zero, IntPtr.Zero, RDW_INVALIDATE | RDW_UPDATENOW);
        }

        private bool leftKeyPressed()
        {
            if (mouseButtonsSwitched())
                return (GetKeyState(VK_RBUTTON) < 0);
            else
                return (GetKeyState(VK_LBUTTON) < 0);
        }

        private bool mouseButtonsSwitched()
        {
            return (GetSystemMetrics(SYSTEM_METRICS.SM_SWAPBUTTON) != 0);
        }

        private SB_HITEST scrollbarHitTest(Orientation orient)
        {
            Point pt = new Point();
            RECT tr = new RECT();
            RECT tp = new RECT();
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);

            GetCursorPos(ref pt);

            if (orient == Orientation.Horizontal)
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                //OffsetRect(ref tr, -tr.Left, -tr.Top);
                tp = tr;
                if (PtInRect(ref tr, pt))
                {
                    // left arrow
                    tp.Right = tp.Left + _iArrowCx;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.leftArrow;
                    // right arrow
                    tp.Left = tr.Right - _iArrowCx;
                    tp.Right = tr.Right;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.rightArrow;
                    // button
                    tp.Left = tr.Left + sb.xyThumbTop;
                    tp.Right = tr.Left + sb.xyThumbBottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.button;
                    // track
                    return SB_HITEST.track;
                }
            }
            else
            {
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                tp = tr;

                if (PtInRect(ref tr, pt))
                {
                    // top arrow
                    tp.Bottom = tr.Top + _iArrowCy;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.topArrow;
                    // bottom arrow
                    tp.Top = tr.Bottom - _iArrowCy;
                    tp.Bottom = tr.Bottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.bottomArrow;
                    // button
                    tp.Top = tr.Top + sb.xyThumbTop;
                    tp.Bottom = tr.Bottom + sb.xyThumbBottom;
                    if (PtInRect(ref tp, pt))
                        return SB_HITEST.button;
                    // track
                    return SB_HITEST.track;
                }
            }
            return SB_HITEST.offControl;
        }

        private void reSizeMask()
        {
            RECT tr = new RECT();
            SCROLLBARINFO sb = new SCROLLBARINFO();
            sb.cbSize = Marshal.SizeOf(sb);
            IntPtr hParent = GetParent(_hControlWnd);
            Point pt = new Point();

            if (hasVertical())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_VSCROLL, ref sb);
                tr = sb.rcScrollBar;
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);
                SetWindowPos(_hVerticalMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
            if (hasHorizontal())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = sb.rcScrollBar;
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);
                SetWindowPos(_hHorizontalMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
            if (hasSizer())
            {
                GetScrollBarInfo(_hControlWnd, OBJID_HSCROLL, ref sb);
                tr = new RECT(sb.rcScrollBar.Right, sb.rcScrollBar.Top, sb.rcScrollBar.Right + _iArrowCx, sb.rcScrollBar.Bottom);
                pt.X = tr.Left;
                pt.Y = tr.Top;
                ScreenToClient(hParent, ref pt);
                SetWindowPos(_hSizerMaskWnd, IntPtr.Zero, pt.X, pt.Y, tr.Right - tr.Left, tr.Bottom - tr.Top, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
            }
        }

        public void scrollbarMetrics()
        {
            _iArrowCx = GetSystemMetrics(SYSTEM_METRICS.SM_CXVSCROLL);
            _iArrowCy = GetSystemMetrics(SYSTEM_METRICS.SM_CYVSCROLL);
        }

        private void scrollHorizontal(bool Right)
        {
            if (Right)
                SendMessage(_hControlWnd, WM_HSCROLL, SB_LINERIGHT, 0);
            else
                SendMessage(_hControlWnd, WM_HSCROLL, SB_LINELEFT, 0);

        }

        private void scrollVertical(bool Down)
        {
            if (Down)
                SendMessage(_hControlWnd, WM_VSCROLL, SB_LINEDOWN, 0);
            else
                SendMessage(_hControlWnd, WM_VSCROLL, SB_LINEUP, 0);
        }

        public void Dispose()
        {
            try
            {
                this.ReleaseHandle();
                if (_oVerticalArrowBitmap != null) _oVerticalArrowBitmap.Dispose();
                if (_cVerticalArrowDc != null) _cVerticalArrowDc.Dispose();
                if (_oVerticalThumbBitmap != null) _oVerticalThumbBitmap.Dispose();
                if (_cVerticalThumbDc != null) _cVerticalThumbDc.Dispose();
                if (_oVerticalTrackBitmap != null) _oVerticalTrackBitmap.Dispose();
                if (_cVerticalTrackDc != null) _cVerticalTrackDc.Dispose();
                if (_oHorizontalArrowBitmap != null) _oHorizontalArrowBitmap.Dispose();
                if (_cHorizontalArrowDc != null) _cHorizontalArrowDc.Dispose();
                if (_oHorizontalThumbBitmap != null) _oHorizontalThumbBitmap.Dispose();
                if (_cHorizontalThumbDc != null) _cHorizontalThumbDc.Dispose();
                if (_oHorizontalTrackBitmap != null) _oHorizontalTrackBitmap.Dispose();
                if (_cHorizontalTrackDc != null) _cHorizontalTrackDc.Dispose();
                if (_hVerticalMaskWnd != IntPtr.Zero) DestroyWindow(_hVerticalMaskWnd);
                if (_hHorizontalMaskWnd != IntPtr.Zero) DestroyWindow(_hHorizontalMaskWnd);
                if (_hSizerMaskWnd != IntPtr.Zero) DestroyWindow(_hSizerMaskWnd);
            }
            catch { }
            GC.SuppressFinalize(this);
        }
        #endregion


        ////  Scrollbar direction
        ////  All these constents can be found in WinUser.h
        //// 
        //private const int SBS_HORZ = 0;
        //private const int SBS_VERT = 1;
        ////  Windows Messages
        ////  All these constents can be found in WinUser.h
        //// 
        //private const int WM_VSCROLL = 277;
        //private const int WM_HSCROLL = 276;
        //private const int SB_THUMBPOSITION = 4;
        //public enum eScrollAction
        //{

        //    Jump = 0,

        //    Relative = 1,
        //}
        //public enum eScrollDirection
        //{

        //    Vertical = 0,

        //    Horizontal = 1,
        //}


        ////  API Function: GetScrollPos
        ////  Returns an integer of the position of the scrollbar
        //// 
        //[DllImport("user32.dll")]
        //private static extern int GetScrollPos(IntPtr hWnd, int nBar);

        ////  API Function: SetScrollPos
        ////  Sets ONLY the scrollbar DOES NOT change the control object
        //// 
        //[DllImport("user32.dll")]
        //private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        //  API Function: PostMessageA
        //  Sends a message to a control (We are going to tell it to synch
        //  with the scrollbar)
        // 
        //[DllImport("user32.dll")]
        //private static extern bool PostMessageA(IntPtr hwnd, int wMsg, int wParam, int lParam);

        ////      Sub: scrollControl
        ////  Purpose: All functions to control the scroll are in here
        //// 
        //private void scrollControl(IntPtr hWnd, eScrollDirection Direction, eScrollAction Action, int Amount)
        //{
        //    int position;
        //    //  What direction are we going
        //    if ((Direction == eScrollDirection.Horizontal))
        //    {
        //        //  What action are we taking (Jumping or Relative)
        //        if ((Action == eScrollAction.Relative))
        //        {
        //            position = (GetScrollPos(hWnd, SBS_HORZ) + Amount);
        //        }
        //        else
        //        {
        //            position = Amount;
        //        }
        //        //  Make it so
        //        if ((SetScrollPos(hWnd, SBS_HORZ, position, true) != -1))
        //        {
        //            PostMessageA(hWnd, WM_HSCROLL, (SB_THUMBPOSITION + (65536 * position)), 0);
        //        }
        //        else
        //        {
        //            //MsgBox(("Can\'t set info (Err: "
        //            //                + (GetLastWin32Error() + ")")));
        //        }
        //    }
        //    else
        //    {
        //        //  What action are we taking (Jumping or Relative)
        //        if ((Action == eScrollAction.Relative))
        //        {
        //            position = (GetScrollPos(hWnd, SBS_VERT) + Amount);
        //        }
        //        else
        //        {
        //            position = Amount;
        //        }
        //        //  Make it so
        //        if ((SetScrollPos(hWnd, SBS_VERT, position, true) != -1))
        //        {
        //            PostMessageA(hWnd, WM_VSCROLL, (SB_THUMBPOSITION + (65536 * position)), 0);
        //        }
        //        else
        //        {
        //            //MsgBox(("Can\'t set info (Err: "
        //            //                + (GetLastWin32Error() + ")")));
        //        }
        //    }
        //}




        //#region WndProc
        //protected override void WndProc(ref Message m)
        //{

        //    switch (m.Msg)
        //    {

        //        case WM_VSCROLL:
        //            this.WndProc(ref m);

                    

        //            break;
        //        case WM_HSCROLL:

        //            this.WndProc(ref m);
                    


        //            break;
        //        default:
        //            this.WndProc(ref m);
        //            break;
        //    }
        //}
        //#endregion


        //public bool PreFilterMessage(ref Message m)
        //{
        //    switch (m.Msg)
        //    {
        //        case WM_MOUSEWHEEL:   // 0x020A
        //        case WM_MOUSEHWHEEL:  // 0x020E
        //            IntPtr hControlUnderMouse = WindowFromPoint(new Point((int)m.LParam));
        //            if (hControlUnderMouse == m.HWnd)
        //                return false; // already headed for the right control
        //            else
        //            {
        //                // redirect the message to the control under the mouse
        //                SendMessage(hControlUnderMouse, m.Msg, m.WParam, m.LParam);
        //                return true;
        //            }
        //        default:
        //            return false;
        //    }
        //}








        //protected override void OnResize(EventArgs e)
        //{
        //    this.OnResize(e);
        //    VisibleScrollbars = GetVisibleScrollbars(this);
        //}




        #region WndProc
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {

                //case 0x000F: //NCCALCSIZE = 0x0083, WM_CREATE = 0x0001, DRAWITEM = 0x002B, MEASUREITEM = 0x002C, CTLCOLOREDIT  = 0x0133, WM_PAINT = 0x000F
                //    //checkBarState();
                //    drawScrollBar();
                //    //reSizeMask();
                //    //reSizeMask();
                //    base.WndProc(ref m);
                //    break;
                
                case WM_NCPAINT:
                    drawScrollBar();
                    base.WndProc(ref m);
                    break;
                
                case WM_MOUSEHWHEEL:  // 0x020E
                case WM_HSCROLL:
                    //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, 5);
                    _bTrackingMouse = true;
                    drawScrollBar();
                    scrollFader();

                    base.WndProc(ref m);
                    break;
                case WM_MOUSEWHEEL:   // 0x020A
                case WM_VSCROLL:
                    //scrollControl(m.WParam, eScrollDirection.Vertical, eScrollAction.Relative, -5);

                    _bTrackingMouse = true;
                    drawScrollBar();
                    scrollFader();
                    base.WndProc(ref m);
                    break;
                case WM_NCMOUSEMOVE:
                    _bTrackingMouse = true;
                    drawScrollBar();
                    scrollFader();
                    base.WndProc(ref m);
                    break;

                case WM_MOUSEMOVE:
                    if (_bTrackingMouse)
                        _bTrackingMouse = false;
                    base.WndProc(ref m);
                    break;

                //case 0x0083: //mak
                case WM_SIZE:
                case WM_MOVE:
                    reSizeMask();
                    base.WndProc(ref m);
                    break;

                default:
                    checkBarState();
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion
    }
}