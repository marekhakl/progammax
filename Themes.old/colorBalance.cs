﻿using System.Drawing;
using System.Drawing.Imaging;

namespace ProGammaX
{
    public static class cColorBalance
    {




        public static Bitmap MakeGrayscale3(Bitmap original)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(
               new float[][] 
      {
         new float[] {.3f, .3f, .3f, 0, 0},
         new float[] {.59f, .59f, .59f, 0, 0},
         new float[] {.11f, .11f, .11f, 0, 0},
         new float[] {0, 0, 0, 1, 0},
         new float[] {0, 0, 0, 0, 1}
      });

            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }






        ///// <summary>
        ///// Creates a Color from alpha, hue, saturation and brightness.
        ///// </summary>
        ///// <param name="alpha">The alpha channel value.</param>
        ///// <param name="hue">The hue value.</param>
        ///// <param name="saturation">The saturation value.</param>
        ///// <param name="brightness">The brightness value.</param>
        ///// <returns>A Color with the given values.</returns>
        //public static Color fromAhsb(int alpha, float hue, float saturation, float brightness)
        //{

        //    if (0 > alpha || 255 < alpha)
        //    {
        //        throw new ArgumentOutOfRangeException("alpha", alpha,
        //          "Value must be within a range of 0 - 255.");
        //    }
        //    if (0f > hue || 360f < hue)
        //    {
        //        throw new ArgumentOutOfRangeException("hue", hue,
        //          "Value must be within a range of 0 - 360.");
        //    }
        //    if (0f > saturation || 1f < saturation)
        //    {
        //        throw new ArgumentOutOfRangeException("saturation", saturation,
        //          "Value must be within a range of 0 - 1.");
        //    }
        //    if (0f > brightness || 1f < brightness)
        //    {
        //        throw new ArgumentOutOfRangeException("brightness", brightness,
        //          "Value must be within a range of 0 - 1.");
        //    }

        //    if (0 == saturation)
        //    {
        //        return Color.FromArgb(alpha, Convert.ToInt32(brightness * 255),
        //          Convert.ToInt32(brightness * 255), Convert.ToInt32(brightness * 255));
        //    }

        //    float fMax, fMid, fMin;
        //    int iSextant, iMax, iMid, iMin;

        //    if (0.5 < brightness)
        //    {
        //        fMax = brightness - (brightness * saturation) + saturation;
        //        fMin = brightness + (brightness * saturation) - saturation;
        //    }
        //    else
        //    {
        //        fMax = brightness + (brightness * saturation);
        //        fMin = brightness - (brightness * saturation);
        //    }

        //    iSextant = (int)Math.Floor(hue / 60f);
        //    if (300f <= hue)
        //    {
        //        hue -= 360f;
        //    }
        //    hue /= 60f;
        //    hue -= 2f * (float)Math.Floor(((iSextant + 1f) % 6f) / 2f);
        //    if (0 == iSextant % 2)
        //    {
        //        fMid = hue * (fMax - fMin) + fMin;
        //    }
        //    else
        //    {
        //        fMid = fMin - hue * (fMax - fMin);
        //    }

        //    iMax = Convert.ToInt32(fMax * 255);
        //    iMid = Convert.ToInt32(fMid * 255);
        //    iMin = Convert.ToInt32(fMin * 255);

        //    switch (iSextant)
        //    {
        //        case 1:
        //            return Color.FromArgb(alpha, iMid, iMax, iMin);
        //        case 2:
        //            return Color.FromArgb(alpha, iMin, iMax, iMid);
        //        case 3:
        //            return Color.FromArgb(alpha, iMin, iMid, iMax);
        //        case 4:
        //            return Color.FromArgb(alpha, iMid, iMin, iMax);
        //        case 5:
        //            return Color.FromArgb(alpha, iMax, iMin, iMid);
        //        default:
        //            return Color.FromArgb(alpha, iMax, iMid, iMin);
        //    }
        //}











        public static Bitmap colorBalance(Bitmap tempBitmap, Color targetColor)
        {


            //Bitmap myBitmap = (Bitmap)tempBitmap.Clone();

            //Color targetColor = Color.Red;

            float alpha = (float)targetColor.A / 128f;
            float red = (float)targetColor.R / 128f;
            float green = (float)targetColor.G / 128f;
            float blue = (float)targetColor.B / 128f;





            //float alpha = 0;
            //float red = 0;
            //float green = 0;
            //float blue = 0;
            //if(targetColorHUE >= 0 && targetColorHUE <= 255)
            //{

            //    red = targetColorHUE / 255;

            //}
            //else if (targetColorHUE >= 256 && targetColorHUE <= 510)
            //{
            //    green = (targetColorHUE - 255) / 255;
            //}
            //else if (targetColorHUE >= 511 && targetColorHUE <= 765)
            //{

            //    blue = (targetColorHUE - 510) / 255;

            //}



            tempBitmap = MakeGrayscale3(tempBitmap);



            ////ColorMatrix layout:

            //// Red Result Green Result Blue Result Alpha Result

            ////Red Value .299 .299 .299 0 (Ignored)

            ////Green Value .587 .587 .587 0 (Ignored)

            ////Blue Value .114 .114 .114 0 (Ignored)

            ////Alpha Value 0 0 0 1

            ////This is basically saying that:

            //// Red should be converted to (R*.299)+(G*.587)+(B*.114)

            //// Green should be converted to (R*.299)+(G*.587)+(B*.114)

            //// Blue should be converted to (R*.299)+(G*.587)+(B*.114)

            //// Alpha should stay the same.

            ////Create the color matrix.

            //ColorMatrix matrix = new ColorMatrix(new float[][]{ 

            //    new float[] { red, 0, 0, 0, 0},

            //    new float[] { 0, green, 0, 0, 0},

            //    new float[] { 0, 0, blue, 0, 0},

            //    new float[] { 0, 0, 0, alpha, 0},

            //    new float[] { 0, 0, 0, 0, 1}

            //    });


            //int red = targetColor.R ;
            //int green = targetColor.G;
            //int blue = targetColor.B;





            ////Store the height/width of the image

            //int height = tempBitmap.Size.Height;

            //int width = tempBitmap.Size.Width;

            ////Loop through both the Y (vertical) and X (horizontal)

            //// coordinates of the image.

            //for (int yCoordinate = 0; yCoordinate < height; yCoordinate++)
            //{

            //    for (int xCoordinate = 0; xCoordinate < width; xCoordinate++)
            //    {

            //        //Get the pixel that's at our current coordinate.

            //        Color color = tempBitmap.GetPixel(xCoordinate, yCoordinate);

            //        //Calculate the gray to use for this pixel.

            //        int grayColor = (color.R + color.G + color.B) / 3;

            //        //Set the pixel to the new gray color.

            //        tempBitmap.SetPixel(xCoordinate, yCoordinate, Color.FromArgb(255, grayColor,

            //        grayColor,

            //        grayColor));

            //    }

            //}

            ////Loop through both the Y (vertical) and X (horizontal)

            //// coordinates of the image.

            //for (int yCoordinate = 0; yCoordinate < tempBitmap.Size.Height; yCoordinate++)
            //{

            //    for (int xCoordinate = 0; xCoordinate < tempBitmap.Size.Width; xCoordinate++)
            //    {


            //        //Calculate the gray to use for this pixel.

            //        int grayColor = (targetColor.R + targetColor.G + targetColor.B) / 3;

            //        //Set the pixel to the new gray color.

            //        tempBitmap.SetPixel(xCoordinate, yCoordinate, Color.FromArgb(grayColor,

            //        grayColor,

            //        grayColor));


            //    }

            //}








            //ImageAttributes IA = new ImageAttributes();

            //Graphics G = Graphics.FromImage(tempBitmap);

            //ColorMatrix CM = new ColorMatrix();

            //CM.Matrix00 = System.Convert.ToSingle(red);

            //CM.Matrix11 = System.Convert.ToSingle(green);

            //CM.Matrix22 = System.Convert.ToSingle(blue);

            //CM.Matrix33 = 1;

            //CM.Matrix44 = 1;

            //IA.SetColorMatrix(CM);

            //Rectangle R = new Rectangle(0, 0, tempBitmap.Width, tempBitmap.Height);

            //G.DrawImage(tempBitmap, R, R.X, R.Y, R.Width, R.Height, GraphicsUnit.Pixel, IA);

            //return new Bitmap(tempBitmap.Width, tempBitmap.Height, G);








            //    float degrees = targetColorHUE;
            //    double r = degrees * System.Math.PI / 180; // degrees to radians 

            //    float[][] colorMatrixElements = { 
            //new float[] {(float)System.Math.Cos(r),  (float)System.Math.Sin(r),  0,  0, 0},
            //new float[] {(float)-System.Math.Sin(r),  (float)-System.Math.Cos(r),  0,  0, 0},
            //new float[] {1,  1,  1,  .5f, 0},
            //new float[] {0,  0,  0,  1, 0},
            //new float[] {0, 0, 0, 0, 1}};

            float[][] colorMatrixElements = { 
   new float[] {red,  0,  0,  0, 0},
   new float[] {0,  green,  0,  0, 0},
   new float[] {0,  0,  blue,  0, 0},
   new float[] {0,  0,  0,  1, 0},
   new float[] {0, 0, 0, 0, 1}};

            ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);



            //Create our image to convert.

            //Image image = (Bitmap)tempBitmap.Clone();

            //Create the ImageAttributes object and apply the ColorMatrix

            ImageAttributes imageAttributes = new ImageAttributes();





            imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);





            //attributes.SetColorMatrix(colorMatrix);

            //Create a new Graphics object from the image.

            Graphics graphics = Graphics.FromImage(tempBitmap);

            //Draw the image using the ImageAttributes we created.

            graphics.DrawImage(tempBitmap, new Rectangle(0, 0, tempBitmap.Width, tempBitmap.Height), 0, 0, tempBitmap.Width, tempBitmap.Height, GraphicsUnit.Pixel, imageAttributes);

            //Dispose of the Graphics object.

            //Bitmap _bitmap = new Bitmap(tempBitmap.Width, tempBitmap.Height, graphics);


            graphics.Dispose();

            //if (AllOneColor(tempBitmap))
            //{
            //    return myBitmap;
            //}

            return tempBitmap;


        }





















    }





}
