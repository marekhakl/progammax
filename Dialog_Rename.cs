﻿namespace ProGammaX
{
    using System.Windows.Forms;

    public partial class Dialog_Rename
    {


        private static Dialog_Rename Dialog_RenameRef;
        public static Dialog_Rename dialog_RenameRef
        {
        get
            {
            return Dialog_RenameRef;
            }
        }
        #region Constructors

        public Dialog_Rename()
        {
            InitializeComponent();

            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);

            Dialog_RenameRef = this;
        }

        #endregion Constructors
        #region Methods

        // ERROR: Handles clauses are not supported in C#
        private void Cancel_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        // ERROR: Handles clauses are not supported in C#
        private void Dialog3_Load(System.Object sender, System.EventArgs e)
        {
        }

        // ERROR: Handles clauses are not supported in C#
        private void OK_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        // ERROR: Handles clauses are not supported in C#
        private void TextBox1_TextChanged(System.Object sender, System.EventArgs e)
        {
            //Me.TextBox1.Text = Form1.GetValidFileName(Me.TextBox1.Text, True)
        }

        #endregion Methods
    }
}